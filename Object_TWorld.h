#ifndef OBJECT_TWORLD_H_
#define OBJECT_TWORLD_H_

#include "Object_TGroup.h"
#include "TParameters.h"
#include "TLog.h"

//#include "stringFunctions.h"


class TWorld{
public:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	std::string dataFile;
	std::string outNameTag;
	bool outNameTagGiven;

	//vector of hierarchical parameters
	TPriorVector priorVec;

	//parameters
	int maxDistanceToConsider;
	int numTotPops;

	//storage
	TSites sites;
	TMC* tmc;
	TGroup* groups;
	double* P;
	double* P_old;
	double* meanP;
	double lambda; //parameter of exponential prior on A_max
	std::string* alpha_values;
	std::ofstream* alpha_file;
	std::string* p_values;
	std::ofstream* p_file;
	std::string A_values;
	std::ofstream A_file;
	std::string P_values;
	std::ofstream P_file;

	unsigned int numgroups;
	unsigned int g; //index variable across group
	int s_max;
	TNormalPriorWithExpVal log_a;
	TNormalPriorWithExpVal log_b;
	double lngamma_aplusb;
	double old_lngamma_aplusb;

//	double mu; //deviation from symmetric tmc matrix
//	double nu;
	bool storageInitialized;

	bool estimateS;
	bool estimateSg;
	bool estimateP;
	bool estimatep;
	bool CalcTotLL;

	bool posteriorP;
	bool restart;
	bool oneAlphaAllGroups;
	TMC* tmcgroups;

	TWorld(TParameters & params, TLog* Logfile, TRandomGenerator* RandomGenerator);

	~TWorld(){
		delete[] P;
		delete[] P_old;
		delete[] meanP;
		delete tmc;
		delete[] groups;
		delete[] alpha_values;
		delete[] alpha_file;
		delete[] p_values;
		delete[] p_file;
		delete tmcgroups;
};

	void readSitesGroupsAndPopulations(std::string & filename, TParameters & params);
	void createSitesGroupsAndPopulations(TParameters & params);
	void initializeFromPrior(TParameters & params);
	void initializeFromCommandLineArguments(TParameters & params, double rangeProp_alphaMax, double rangeProp_beta, double rangeProp_kappa, double rangeProp_mu, double rangeProp_nu);
	void estimateFrequenciesFromData(TParameters & params);
	void InitializeS(TParameters & params);

	void runMCMCInference(TParameters & params);
	void findGoodStartingValuesForMCMC(bool estimateS, double sigma2_prior);
	void findInitialValuesOfSAndKappa(const int & numSwitches, TLog* logfile, bool estimateS, double sigma2_prior_world);
	void findInitialValuesOfMuAndNu(const int & numSwitches, TLog* logfile);
	void findInitialValues(TPrior* param, const int & numSwitches, TLog* logfile);
	double calcSumlnLhdQ();
	void print_S(bool estimateS, int  iterations);
	void print_P(bool posteriorP, int  iterations);
	void update_S();
	void update_SoneAlphaAllGroups();
	void update_Amax();
	void update_P();
	void update_alphamax();
	void update_alphamaxAllGroups();
	void update_B(std::ostream & hbeta);
	void update_Kappa();
	void update_mu();
	void update_nu();
	void update_beta_parameter(bool hierarchy, TNormalPriorWithExpVal & log_par);
	void update_allStogether(std::ofstream& upAllS, int iterations);
	void update_allStogetherGroups(double & log_h, double & prop_LL, double & old_LL, bool & accepted, int & _min, int & _max, int & cost);
	double logHastings_a(bool hierarchy);
	double logHastings_b(bool hierarchy);
	void calcGammaFor_betapar();
	void switchGammaFor_betapar();
	void rejectGammaFor_betapar();

	void makeMCMCStep(bool hierarchy);
	void writeData(std::string filename);
	void filterData(std::string filename, TParameters & params);
	void filter_countslimit(TParameters & params, int allpop, std::ofstream& out);
	void filter_missedPop(TParameters & params, std::ofstream& out);
	void filter_minVar(TParameters & params, std::ofstream& out);
	void filter_minPop(TParameters & params, std::ofstream& out);
	void Restart(TParameters & params);

	void calculateLLsurfaces(TParameters & params);
	void calcLikelihoodSurfaceAmax(std::string filename, TParameters & params);
	void calcLikelihoodSurface_alphamax(std::string filename,TParameters & params);
	void calcLikelihoodSurfaceB(std::string filename, TParameters & params);
	void calcLikelihoodSurface_beta(std::string filename,TParameters & params);
	void calcLikelihoodSurface_P(std::string filename);
	void calcLikelihoodSurface_p(std::string filename, TParameters & params);
	void calcLikelihoodSurfacelnK(std::string filename, TParameters & params);
	void calcLikelihoodSurfacelnkappa(std::string filename, TParameters & params);
	void calcLikelihoodSurfaceSworld(std::string filename);
	void calcLikelihoodSurfaceSgroups(std::string filename);
	void calcLikelihoodSurface_a(std::string filename, TParameters & params);

	void writeInitialLL(std::ofstream& name);
	void writeParameters(std::ofstream& name, int i);
	double give_totLL();

	void readAndSetS(TParameters & params, TMC *tmc);
	void readAndSetSg(TParameters & params);
	void readAndSetP(TParameters & params);

	void readAndSetParameters(TParameters & params);
	void simulateData(TParameters & params);

	void calculate_Fst(TParameters & params);

};

#endif /* OBJECT_TWORLD_H_ */
