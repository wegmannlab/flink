#include "Object_TWorld.h"
#include <chrono>

//#define GPERFTOOLS_PROF

#ifdef GPERFTOOLS_PROF
	#include <gperftools/profiler.h>
#endif

#if defined(_OPENMP)
    #include <omp.h>
#endif

TWorld::TWorld(TParameters & params, TLog* Logfile, TRandomGenerator* RandomGenerator){
	logfile = Logfile;
	randomGenerator = RandomGenerator;
	if(params.parameterExists("outName")){
		outNameTagGiven = true;
		outNameTag = params.getParameterString("outName");
	} else {
		outNameTagGiven = false;
		outNameTag = "Flink";
	}

	double sigmaProp_a = params.getParameterDoubleWithDefault("sigmaProp_a", 0.04);

	//set parameters from user input
	s_max = params.getParameterIntWithDefault("s_max", 10);

	std::string a_beta = params.getParameterStringWithDefault("log_a", "0.0");
	std::string b_beta = params.getParameterStringWithDefault("log_b", "0.0");
	if((params.parameterExists("log_a") && !params.parameterExists("log_b")) || (!params.parameterExists("log_a") && params.parameterExists("log_b"))) throw "Both the parameters of the beta distribution have to be set!";
	log_a.initialize("log_a", a_beta, -20.0, 20.0, sigmaProp_a, randomGenerator);
	log_b.initialize("log_b", b_beta, -20.0, 20.0, sigmaProp_a, randomGenerator);

	//	mu = params.getParameterDoubleWithDefault("mu", 0.9);
	//	nu = params.getParameterDoubleWithDefault("nu", 0.01);
	maxDistanceToConsider = params.getParameterIntWithDefault("maxDist", 1000000);
	if(maxDistanceToConsider < 100) throw "Largest distance to consider when creating distance groups should be at least 100!";

	numTotPops=0;
	//storage
	P = NULL;
	P_old = NULL;
	meanP = NULL;
	lambda = 0.0;
	alpha_values = NULL;
	alpha_file = NULL;
	p_values = NULL;
	p_file = NULL;

	calcGammaFor_betapar();
	old_lngamma_aplusb=0;

	numgroups = 0;
	g = 0;
	groups = NULL;
	tmc = NULL;
	storageInitialized = false;

	estimateS = true;
	estimateSg = false;
	estimateP = true;
	estimatep = true;
	CalcTotLL = false;
	if(params.parameterExists("CalcTotLL"))
		CalcTotLL = true;

	posteriorP = false;
	restart = false;
	oneAlphaAllGroups = false;
	if(params.parameterExists("restart"))
		restart = true;

	tmcgroups = NULL;
	if(params.parameterExists("oneAlphaAllGroups")){
		oneAlphaAllGroups = true;
		estimateSg = true;
	}
}

void TWorld::readSitesGroupsAndPopulations(std::string & filename, TParameters & params){
	logfile->startIndent("Reading data and population structure from file '" + filename + "':");

	//set output name tag
	if(!outNameTagGiven){
		outNameTag = filename;
		outNameTag = extractBeforeLast(outNameTag, '.');
	}
	//open data file for reading
	logfile->listFlush("Opening file '" + filename + "' ...");
	std::ifstream input(filename.c_str());
	if(!input) throw "Data file '" + filename + "' could not be opened!";
	logfile->write(" done!");

	//read groups and populations from input file
	logfile->listFlush("Reading groups and populations ...");
	std::string line;
	long lineNum = 2;
	std::vector<std::string> groupNames;
	std::vector<std::string> popNames;
	fillVectorFromLineWhiteSpaceSkipEmpty(input, groupNames);
	fillVectorFromLineWhiteSpaceSkipEmpty(input, popNames);
	if(groupNames.size() != popNames.size()) throw "Problem reading data file '" + filename + "': number of group names does not match number of population names!";

	//remove dashes in first two columns of group and pop names, if present
	if(groupNames[0] == "-"){
		if(groupNames[1] != "-") throw "Expecting two, not a single dash at the beginning of group names in file '" + filename + "'!";
		if(popNames[0] != "-") throw "Expecting dashes at the beginning of population names in file '" + filename + "'!";
		if(popNames[1] != "-") throw "Expecting two, not a single dash at the beginning of population names in file '" + filename + "'!";
		groupNames.erase(groupNames.begin(), groupNames.begin()+2);
		popNames.erase(popNames.begin(), popNames.begin()+2);
	}

	//get unique group names
	std::vector<std::string> uniqueGroupNames;
	std::vector<std::string>::iterator it, it2;
	bool found;
	for(it=groupNames.begin(); it!=groupNames.end(); ++it){
		if(*it != "-"){
			//check if name exists
			found = false;
			for(it2=uniqueGroupNames.begin(); it2!=uniqueGroupNames.end(); ++it2){
				if(*it2 == *it){
					found = true;
					break;
				}
			}
			if(!found) uniqueGroupNames.push_back(*it);
		}
	}
	numgroups = uniqueGroupNames.size();
	logfile->write(" done!");
	logfile->conclude("Read " + toString(numgroups) + " groups");

	//read all sites and store them
	logfile->listFlush("Reading loci from file ...");
	std::vector<std::string> vec;
	int numCol = groupNames.size() + 2;
	while(input.good() && !input.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(input, vec);
		//skip empty lines
		if(vec.size() > 0){
			if(vec.size() != (unsigned)numCol) throw "Wrong number of columns (" + toString(vec.size()) + " instead of " + toString(numCol) + ") in file '" + filename + "' on line " + toString(lineNum) + "!";

			//add site
			sites.addSite(vec[0], stringToLong(vec[1]));
		}
	}
	input.close();
	sites.calcAndGroupDistances(maxDistanceToConsider);

	logfile->write(" done!");
	logfile->conclude("Read " + toString(sites.numSites) + " sites on " + toString(sites.numChromosomes) + " chromosomes");

	//initialize TMC
	tmc = new TMC(s_max, &sites, randomGenerator);

	if(oneAlphaAllGroups==true){
		tmcgroups = new TMC(s_max, &sites, randomGenerator);
	}

	//construct group and population objects
	groups = new TGroup[numgroups];

	//initialize groups
	logfile->startIndent("Creating the following groups and populations:");
	int* groupIndex = new int[numCol];
	int* popIndex = new int[numCol];
	int col;
	std::vector<std::string> popNamesPerGroup;
	for(g=0; g<numgroups; ++g){
		logfile->startIndent("Group '" + uniqueGroupNames[g] + "':");
		//initialize populations of this group
		popNamesPerGroup.clear();
		it = popNames.begin();
		it2 = groupNames.begin();
		col = 0;
		for(; it != popNames.end(); ++it, ++it2, ++col){
			if(*it2 == uniqueGroupNames[g]){
				popNamesPerGroup.push_back(*it);
				groupIndex[col+2] = g;
				popIndex[col+2] = popNamesPerGroup.size() - 1;
				logfile->list(*it);
			}
		}
		logfile->endIndent();
		groups[g].initialize(uniqueGroupNames[g], popNamesPerGroup, s_max, &sites, randomGenerator);
	}

	logfile->endIndent();

	//read file again to read data
	logfile->listFlush("Reading allele frequency data from file '" + filename + "' ...");
	input.open(filename.c_str());
	if(!input) throw "Data file '" + filename + "' could not be opened!";

	//skip two header lines
	getline(input, line);
	getline(input, line);

	//now parse through file
	lineNum = 2;
	int n, N;
	bool Nsetted = false;
	//read N
	if(params.parameterExists("N")){
		N = params.getParameterDouble("N");
		logfile->list("Will set N = " + toString(N) + " in each population.");
		Nsetted = true;
	}

	bool nocounts=false;
	if(params.parameterExists("nocounts")){
		nocounts=true;
		if(!params.parameterExists("N")) throw "Giving frequencies instead of counts in the input file needs to get N";
	}

	int locus = 0;
	std::size_t pos;
	while(input.good() && !input.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(input, vec);
		//skip empty lines
		if(vec.size() > 0){
			if(vec.size() != (unsigned)numCol) throw "Wrong number of columns in file '" + filename + "' on line " + toString(lineNum) + "!";

			//add data to populations
			for(col = 2 ; col < numCol; ++col){
				if(nocounts==false){
					pos = vec[col].find_first_of('/');
					if(pos == std::string::npos) throw "Character '/' missing on line " + toString(lineNum) + " in file '" + filename + "'!";
					n = stringToInt(vec[col].substr(0, pos));
					if(Nsetted==false){
						N = stringToInt(vec[col].substr(pos+1));
					}
					if(n > N) throw "n > N on line " + toString(lineNum) + " in file '" + filename + "'!";
					if(n < 0) throw "n < 0 on line " + toString(lineNum) + " in file '" + filename + "'!";

					groups[groupIndex[col]].addData(popIndex[col], locus, n, N);
				}else{
					double f = stringToDouble(vec[col]);
					n=int(f*N);
					groups[groupIndex[col]].addData(popIndex[col], locus, n, N);
				}
			}
			++locus;
			if(locus > sites.numSites) throw "Number of sites when reading data does not match number of sites read initially! Did the file change?";
		}
	}
	input.close();
	logfile->write(" done!");
	if(locus != sites.numSites) throw "Number of sites when reading data does not match number of sites read initially! Did the file change?";

	//initialize P
	P = new double[sites.numSites];
	P_old = new double[sites.numSites];
	meanP = new double[sites.numSites];
	storageInitialized = true;
	alpha_values = new std::string[numgroups];
	alpha_file = new std::ofstream[numgroups];
	p_values = new std::string[numgroups];
	p_file = new std::ofstream[numgroups];

	for(int l=0; l< sites.numSites; l++){
		meanP[l] = 0.0;
	}
	for(unsigned int g=0; g<numgroups; g++){
		for(int l=0; l<groups[g].numSites; l++){
			groups[g].mean_p[l] = 0.0;
		}
	}

	//clean up memory
	delete[] groupIndex;
	delete[] popIndex;
}

void TWorld::createSitesGroupsAndPopulations(TParameters & params){
	//function to create groups and populations from parameters
	logfile->startIndent("Creating sites and population structure from parameters:");

	//Create sites
	int numChr = params.getParameterIntWithDefault("numChr", 1);
	int numSitesPerChr = params.getParameterIntWithDefault("numSites", 100);
	int distanceBetweenSites = params.getParameterIntWithDefault("distBetweenSites", 1000);
	logfile->list("Will create " + toString(numChr) + " chromosomes with "+ toString(numSitesPerChr) + " sites at " + toString(distanceBetweenSites) + "bp from each other.");
	sites.createChromosomesAndSites(numChr, numSitesPerChr, distanceBetweenSites);
	sites.calcAndGroupDistances(maxDistanceToConsider);
	//create TMC object
	tmc = new TMC(s_max, &sites, randomGenerator);

	//create groups and populations
	std::string tmp = params.getParameterString("pops");
	std::vector<int> numpopspergroup_vec;
	fillVectorFromString(tmp, numpopspergroup_vec, ',');
	numgroups = numpopspergroup_vec.size();
	if(numgroups<1) throw "No group has been specified!";
	std::string outstring;
	for(std::vector<int>::iterator it = numpopspergroup_vec.begin(); it != numpopspergroup_vec.end(); ++it){
		if(*it < 1) throw "Can not create group with < 1 populations!";
		if(it == numpopspergroup_vec.begin()) outstring = toString(*it);
		else if(it == numpopspergroup_vec.end() - 1) outstring += " and " + toString(*it);
		else outstring += ", " + toString(*it);
	}
	logfile->list("Will create " + toString(numgroups) + " groups with " + outstring + " populations, respectively.");

	//read N
	int N = params.getParameterIntWithDefault("N", 100);
	logfile->list("Will set N = " + toString(N) + " in each population.");

	//initialize groups
	groups = new TGroup[numgroups];
	std::vector<std::string> popNamesPerGroup;
	std::string groupLetter;
	int n = 0;
	for(unsigned int g=0; g<numgroups; ++g){
		//create population names
		groupLetter = static_cast<char>('A' + g);
		popNamesPerGroup.clear();
		for(int j=0; j<numpopspergroup_vec[g]; ++j)
			popNamesPerGroup.push_back("Pop_" + groupLetter + toString(j));
		groups[g].initialize("Group_" + groupLetter, popNamesPerGroup, s_max, &sites, randomGenerator);
		for(int j=0; j<numpopspergroup_vec[g]; ++j){
			for(int l=0; l<sites.numSites; ++l){
				groups[g].addData(j, l, n, N);
			}
		}
	}
	logfile->endIndent();

	//initialize P
	P = new double[sites.numSites];
	P_old = new double[sites.numSites];
	storageInitialized = true;
}


void TWorld::initializeFromCommandLineArguments(TParameters & params, double rangeProp_alphaMax, double rangeProp_beta, double rangeProp_kappa, double rangeProp_mu, double rangeProp_nu){
	std::string tmp;
	std::vector<std::string> BVec;
	std::string task = params.getParameterString("task");
	if(numgroups>1){
		//NOTE: P has to be filled!
		//B
		if(task=="Fst"){
			tmp = "(-1.0,0.1)";
		}else{
			tmp = params.getParameterString("B");
		}
		splitParameterStringIntoVector(tmp, BVec, "B");
		if(BVec.size() == 1) repeatFirstEntry(BVec, numgroups);
		else if (BVec.size() != numgroups) throw "The number of B values provided does not match the number of groups!";

		//A_max
		std::string A_max;
		if(task=="Fst"){
			A_max = "1.0,0.1";
		}else{
			A_max = params.getParameterString("A_max");
		}
		//lnK
		std::string lnK = params.getParameterStringWithDefault("lnK", "-2.0");
		std::string lnMu = params.getParameterStringWithDefault("lnMu", "-2.0");
		std::string lnNu = params.getParameterStringWithDefault("lnNu", "-1.0");

		//initialize tmc
		tmc->initializeFromPrior("A_max", A_max, "lnK", lnK, "lnMu", lnMu, "lnNu", lnNu, rangeProp_alphaMax, rangeProp_kappa, rangeProp_mu, rangeProp_nu);

		if(oneAlphaAllGroups==true){
			std::string alpha_max = params.getParameterStringWithDefault("alpha_max", "3.0");
			std::string lnkappa = params.getParameterStringWithDefault("lnkappa", "-2.0");
			std::string lnMu_g = params.getParameterStringWithDefault("lnMu_g", "-2.0");
			std::string lnNu_g = params.getParameterStringWithDefault("lnNu_g", "-1.0");
			tmcgroups->initializeFromPrior("alpha_max", alpha_max, "lnkappa", lnkappa, "lnMu_g", lnMu_g, "lnNu_g", lnNu_g, rangeProp_alphaMax, rangeProp_kappa, rangeProp_mu, rangeProp_nu);
		}
	}

	//beta
	if(task=="Fst"){
		tmp = "[(-2.0,0.1)]";
	}else{
		tmp = params.getParameterString("beta");
	}
	std::vector<std::string> betaVec;
	splitParameterStringIntoVectorHierarchical(tmp, betaVec, "beta");
	if(betaVec.size() == 1) repeatFirstEntry(betaVec, numgroups);
	else if(betaVec.size() != numgroups) throw "The number of beta values provided does not match the number of groups!";


	//alpha_max
	if(task=="Fst"){
		tmp = "(3.0,0.1)" ;
	}else{
		tmp = params.getParameterString("alpha_max");
	}
	std::vector<std::string> alpha_maxVec;
	splitParameterStringIntoVector(tmp, alpha_maxVec, "alpha_max");
	if(alpha_maxVec.size() == 1) repeatFirstEntry(alpha_maxVec, numgroups);
	else if (alpha_maxVec.size() != numgroups) throw "The number of alpha_max values provided does not match the number of groups!";

	//lnkappa
	tmp = params.getParameterStringWithDefault("lnkappa", "-2.0");
	std::vector<std::string> lnkappaVec;
	splitParameterStringIntoVector(tmp, lnkappaVec, "lnkappa");
	if(lnkappaVec.size() == 1) repeatFirstEntry(lnkappaVec, numgroups);
	else if (lnkappaVec.size() != numgroups) throw "The number of lnkappa values provided does not match the number of groups!";

	//lnMu for groups
	tmp = params.getParameterStringWithDefault("lnMu_g", "-2.0");
	std::vector<std::string> lnMu_gVec;
	splitParameterStringIntoVector(tmp, lnMu_gVec, "lnMu_g");
	if(lnMu_gVec.size() == 1) repeatFirstEntry(lnMu_gVec, numgroups);
	else if (lnMu_gVec.size() != numgroups) throw "The number of lnMu_g values provided does not match the number of groups!";

	//lnNu for groups
	tmp = params.getParameterStringWithDefault("lnNu_g", "-1.0");
	std::vector<std::string> lnNu_gVec;
	splitParameterStringIntoVector(tmp, lnNu_gVec, "lnNu_g");
	if(lnNu_gVec.size() == 1) repeatFirstEntry(lnNu_gVec, numgroups);
	else if (lnNu_gVec.size() != numgroups) throw "The number of lnNu_g values provided does not match the number of groups!";

	//initialize groups and register parameters
	priorVec.addPrior(&log_a);
	priorVec.addPrior(&log_b);
	tmc->registerPriors(&priorVec);
	for(g=0; g<numgroups; ++g){
		if(numgroups>1){
			groups[g].initializeFromPrior(BVec[g], betaVec[g], alpha_maxVec[g], lnkappaVec[g], lnMu_gVec[g], lnNu_gVec[g], rangeProp_beta, rangeProp_alphaMax, rangeProp_kappa, rangeProp_mu, rangeProp_nu);
		}else{
			groups[g].initializeFromPrior(betaVec[g], alpha_maxVec[g], lnkappaVec[g], lnMu_gVec[g], lnNu_gVec[g], rangeProp_beta, rangeProp_alphaMax, rangeProp_kappa, rangeProp_mu, rangeProp_nu);
		}
		groups[g].registerPriors(&priorVec);
	}
	if(oneAlphaAllGroups==true)
		tmcgroups->registerPriors(&priorVec);

}


void TWorld::initializeFromPrior(TParameters & params){
	//read range prop
	double rangeProp_alphaMax = params.getParameterDoubleWithDefault("sigmaProp_alphaMax", 0.15);
	double rangeProp_beta = params.getParameterDoubleWithDefault("sigmaProp_beta", 0.05);
	double rangeProp_kappa = params.getParameterDoubleWithDefault("sigmaProp_kappa", 0.12);
	double rangeProp_mu = params.getParameterDoubleWithDefault("sigmaProp_mu", 0.4);
	double rangeProp_nu = params.getParameterDoubleWithDefault("sigmaProp_nu", 0.4);

	//estimate frequencies from data
	estimateFrequenciesFromData(params);

	initializeFromCommandLineArguments(params, rangeProp_alphaMax, rangeProp_beta, rangeProp_kappa, rangeProp_mu, rangeProp_nu);

	if(priorVec.hasPriors > 0){
		logfile->startIndent("The following hierarchical parameters will be estimated:");
		priorVec.listParams(logfile);
		logfile->endIndent();
	} else {
		logfile->list("All hierarchical parameters are fixed!");
		logfile->conclude("Will estimating per locus allele frequencies and selection coefficients only!");
	}

	//calc initial gamma terms to get started
	if(numgroups>1){
		for(g=0; g<numgroups; ++g){
			if(oneAlphaAllGroups==false){
				groups[g].calcInitialGammaTerms(P, tmc);
			}else{
				groups[g].calcInitialGammaTerms(P, tmc, tmcgroups);
			}
		}
	}else{
		groups[0].calcInitialGammaTerms();
	}
}


void TWorld::estimateFrequenciesFromData(TParameters & params){
	//first estimate in each group
	if(params.parameterExists("input_freq")){
		estimatep=false;
		std::string freq_p = params.getParameterString("input_freq");
		std::ifstream input_p(freq_p.c_str());

		if(!input_p) throw "File '" + freq_p + "' could not be opened!";
		logfile->write(" done!");
		std::string name_p;
		for(g=0; g<numgroups; ++g){
			for(int l=0; l< sites.numSites; l++){
				input_p >> name_p;
				input_p >> groups[g].p[l];
			}
		}
		input_p.close();
	} else if(params.parameterExists("p")) {
		estimatep=false;
		double initialp = params.getParameterDouble("p");
		logfile->list("p = " + toString(initialp));
		for(g=0; g<numgroups; g++){
			for(int l=0; l<sites.numSites; l++){
				groups[g].p[l] = initialp;
			}
		}
	} else {
		for(g=0; g<numgroups; ++g){
			groups[g].estimateFrequenciesFromData();
		}
	}

	//then estimate P
	if(params.parameterExists("input_P")){
		estimateP=false;
		std::string freq_P = params.getParameterString("input_P");
		std::ifstream input_P(freq_P.c_str());

		if(!input_P) throw "File '" + freq_P + "' could not be opened!";
		logfile->write(" done!");
		for(int l=0; l< sites.numSites; l++){
			input_P >> P[l];
		}
		input_P.close();
	}else if(params.parameterExists("P")){
		readAndSetP(params);
		estimateP=false;
	} else if(numgroups==1) {
		estimateP=false;
	} else {
		for(int l=0; l< sites.numSites; l++){
			P[l] = 0.0;
			for(g=0; g<numgroups; ++g){
				P[l] += groups[g].getP(l);
			}

			P[l] = P[l] / (double) numgroups;

			//make sure p is between 0 and 1
			if(P[l] < 0.001) P[l] = 0.001;
			else if(P[l] > 0.999) P[l] = 0.999;
		}
	}
}


void TWorld::InitializeS(TParameters & params){
	if(params.parameterExists("S_fromfile")){
		std::string file = params.getParameterString("S_fromfile");
		std::ifstream input_Sfile(file.c_str());
		if(!input_Sfile) throw "Missing file to initialize S";
		std::string name;
		for(int l=0; l< sites.numSites; l++){
			input_Sfile >> name;
			input_Sfile >> tmc->S[l];
            tmc->S[l] += s_max;
		}
		input_Sfile.close();
		if(params.parameterExists("no_S_estimation"))
			estimateS=false;
	}

	if(oneAlphaAllGroups==true){
		if(params.parameterExists("Sg_fromfile")){
			std::string file = params.getParameterString("Sg_fromfile");
			std::ifstream input_Sgfile(file.c_str());
			if(!input_Sgfile) throw "Missing file to initialize Sg";
			std::string name;
			for(int l=0; l< sites.numSites; l++){
				input_Sgfile >> name;
				input_Sgfile >> tmcgroups->S[l];
			}
			input_Sgfile.close();
			if(params.parameterExists("no_Sg_estimation"))
				estimateSg=false;
		}
		for(unsigned int g=0; g<numgroups; g++){
			groups[g].estimateSg=false;
		}
	}else{
		for(unsigned int g=0; g<numgroups; g++){
			if(params.parameterExists("Sg" + toString(g) + "_fromfile")){
				std::string file = params.getParameterString("Sg" + toString(g) + "_fromfile");
				groups[g].InitializeSg(file);
				if(params.parameterExists("no_Sg" + toString(g) + "_estimation"))
					groups[g].estimateSg=false;
			}
		}
	}
}


void TWorld::Restart(TParameters & params){
	if((log_a.isEstimated && !log_b.isEstimated) || (!log_a.isEstimated && log_b.isEstimated)){
		throw "Both the parameters of the beta distribution have to be set!";
	}else if(log_a.isEstimated && log_b.isEstimated){
		log_a.curValue=params.getParameterDouble("cur_log_a");
		log_b.curValue=params.getParameterDouble("cur_log_b");
	}

	if(tmc->lnNu.isEstimated){
		tmc->lnNu.curValue=params.getParameterDouble("cur_lnNu");
	}
	if(tmc->lnMu.isEstimated){
		tmc->lnMu.curValue=params.getParameterDouble("cur_lnMu");
	}
	if(tmc->lnkappa.isEstimated){
		tmc->lnkappa.curValue=params.getParameterDouble("cur_lnK");
	}
	for(unsigned int g=0; g<numgroups; g++){
		if(groups[g].B.isEstimated){
			groups[g].B.curValue=params.getParameterDouble("cur_B_" + toString(g));
		}
		for(unsigned int j=0; j<groups[g].numpop; j++){
			if(groups[g].pops[j].beta.isEstimated){
				groups[g].pops[j].beta.curValue=params.getParameterDouble("cur_beta_" + toString(g) + "_" + toString(j));
			}
		}
		if(oneAlphaAllGroups==false){
			if(groups[g].tmc->lnkappa.isEstimated){
				groups[g].tmc->lnkappa.curValue=params.getParameterDouble("cur_lnkappa_" + toString(g));
			}
			if(groups[g].tmc->lnMu.isEstimated){
				groups[g].tmc->lnMu.curValue=params.getParameterDouble("cur_lnMu_g" + toString(g));
			}
			if(groups[g].tmc->lnNu.isEstimated){
				groups[g].tmc->lnNu.curValue=params.getParameterDouble("cur_lnNu_g" + toString(g));
			}
			groups[g].tmc->fillAllQ();
		}else{
			if(tmcgroups->lnNu.isEstimated){
				tmcgroups->lnNu.curValue=params.getParameterDouble("cur_lnNu_g0");
			}
			if(tmcgroups->lnMu.isEstimated){
				tmcgroups->lnMu.curValue=params.getParameterDouble("cur_lnMu_g0");
			}
			if(tmcgroups->lnkappa.isEstimated){
				tmcgroups->lnkappa.curValue=params.getParameterDouble("cur_lnK_g0");
			}
			tmcgroups->fillAllQ();
		}
	}

	//calc initial gamma terms to get started
	if(numgroups>1){
		for(g=0; g<numgroups; ++g){
			if(oneAlphaAllGroups==false){
				groups[g].calcInitialGammaTerms(P, tmc);
			}else{
				groups[g].calcInitialGammaTerms(P, tmc, tmcgroups);
			}
		}
	}else{
		groups[0].calcInitialGammaTerms();
	}
}


//-------------------------------------------------------
// MCMC Inference
//-------------------------------------------------------
void TWorld::runMCMCInference(TParameters & params){
	int num_threads = params.getParameterIntWithDefault("numThreads", 1);

	//setting the number of cores
    #if defined(_OPENMP)
        omp_set_num_threads(num_threads);
    #endif

	//initialize populations and sites
	dataFile = params.getParameterString("data");
	readSitesGroupsAndPopulations(dataFile, params);

	//Initialize from prior
	initializeFromPrior(params);

	//read other parameters
	double sigma2_prior = params.getParameterDoubleWithDefault("sigma2_prior", 1.0);
	int numIterations = params.getParameterIntWithDefault("numIterations", 10000);
	int thinning = params.getParameterIntWithDefault("thinning", 10);
	int burnin = params.getParameterIntWithDefault("burnin", 0);
	int iteration_B = params.getParameterIntWithDefault("iteration_B", 1);
	int iteration_jumpallS = params.getParameterIntWithDefault("iteration_jumpallS", 1000);
	int lim_jumpallS = params.getParameterIntWithDefault("lim_jumpallS", numIterations);
	numIterations += burnin;
	int prog = 0;
	int oldProg = 0;

	if(params.parameterExists("printPosteriorP")){
		posteriorP = true;
		for(unsigned int g=0; g<numgroups; g++){
			groups[g].posterior_p = true;
		}
	}

	//open output file
	std::ofstream out;
	std::ofstream burninfile;
	if(priorVec.hasPriors || CalcTotLL==true){
		std::string outname = outNameTag + "_MCMCM_hierarchicalParameters.txt";
		std::string burniname = "burnin_MCMCM_hierarchicalParameters.txt";
		logfile->list("Will write MCMC of hierarchical parameters to '" + outname + "'");
		out.open(outname.c_str());
		burninfile.open(burniname.c_str());
		if(!out) throw "Failed to open file '" + outname + "' for writing!";
		out<<"iterations";
		burninfile<<"iterations";
		if(priorVec.hasPriors){
			out<<"\t";
			priorVec.writeHeader(out);
			burninfile<<"\t";
			priorVec.writeHeader(burninfile);
		}
		if(CalcTotLL==true){
			out<<"\t";
			out<<"totLL";
			burninfile<<"\t";
			burninfile<<"totLL";
		}
		out << std::endl;
		burninfile << std::endl;
	}

	if(params.parameterExists("S")){
		readAndSetS(params, tmc);
		estimateS=false;
	}else if(numgroups==1){
		estimateS=false;
	}
	if(params.parameterExists("Sg")){
		if(oneAlphaAllGroups==false){
			readAndSetSg(params);
			for(unsigned int g=0; g<numgroups; ++g)
				groups[g].estimateSg=false;
		}else{
			readAndSetS(params, tmcgroups);
			estimateSg=false;
		}
	}
	if(estimateS==false && tmc->lnkappa.isEstimated) throw "S cannot be fixed with lnK estimation!";
	for(unsigned int g=0; g<numgroups; ++g)
		if(groups[g].estimateSg==false && groups[g].tmc->lnkappa.isEstimated) throw "Sg cannot be fixed with Kg estimation in the group " + toString(g) + "!";
	if(oneAlphaAllGroups==true && estimateSg==false && tmcgroups->lnkappa.isEstimated) throw "Sg cannot be fixed with lnkappa_g estimation!";

	//reading S and Sg from files
	InitializeS(params);

	if(numgroups>1){
		for(unsigned int g=0; g<numgroups; ++g){
			groups[g].calcInitialGammaTerms(P, tmc);
		}
	}

	//find good starting values
	if( !restart && oneAlphaAllGroups==false){
		findGoodStartingValuesForMCMC(estimateS, sigma2_prior);
	}

	//now run MCMC
	logfile->listFlush("Running MCMC for " + toString(numIterations) + " iterations ...");
	int iterations=0;


	//set the number of threads (I had to add -fopenmp flag to the "Miscellaneous" property and to the linker section)
//	int numThreads = params.getParameterIntWithDefault("numThreads", 1);
//	omp_set_num_threads(numThreads);

	bool hierarchy = false;
	if(numgroups > 1)
		hierarchy = true;
	//starting MCMC
	if(numgroups>1){
		std::string anc_P = "Patbeginning.txt";
		std::ofstream freq_simP(anc_P.c_str());
		for(int l=0; l<sites.numSites; l++){
			freq_simP<<P[l]<<"\n";
		}
		freq_simP.close();

		//initialize Posterior alphas files for the higher hierarchy
		A_values = "Posterior_A.txt";
		A_file.open(A_values.c_str());
		A_file<<"chain";
		for(int l=0; l< sites.numSites; l++){
			A_file<<"\t"<<"A_"<<l;
		}
		A_file<<std::endl;

		if(posteriorP==true){
			P_values = "Posterior_P.txt";
			P_file.open(P_values.c_str());
			P_file<<"chain";
			for(int l=0; l< sites.numSites; l++){
				P_file<<"\t"<<"P_"<<l;
			}
			P_file<<std::endl;
		}
	}

	//initialize Posterior alphas files per group
	if(oneAlphaAllGroups==false){
		for(unsigned g=0; g<numgroups; ++g){
			alpha_values[g] = "Posterior_alphas_group_" + toString(g) + ".txt";
			alpha_file[g].open(alpha_values[g].c_str());
			alpha_file[g]<<"chain";
			for(int l=0; l< sites.numSites; l++){
				alpha_file[g]<<"\t"<<"alpha_"<<l;
			}
			alpha_file[g]<<std::endl;
		}
	}else{
		alpha_values[0] = "Posterior_alphas_group_" + toString(0) + ".txt";
		alpha_file[0].open(alpha_values[0].c_str());
		alpha_file[0]<<"chain";
		for(int l=0; l< sites.numSites; l++){
			alpha_file[0]<<"\t"<<"alpha_"<<l;
		}
		alpha_file[0]<<std::endl;
	}

	if(posteriorP==true){
		for(unsigned g=0; g<numgroups; ++g){
			p_values[g] = "Posterior_p_group_" + toString(g) + ".txt";
			p_file[g].open(p_values[g].c_str());
			p_file[g]<<"chain";
			for(int l=0; l< sites.numSites; l++){
				p_file[g]<<"\t"<<"p_"<<l;
			}
			p_file[g]<<std::endl;
		}
	}

	if(restart)
		Restart(params);

	std::string up = "upAllS_totLL.txt";
	std::ofstream upAllS(up.c_str());
	for(unsigned g=0; g<numgroups; ++g)
		if(groups[g].estimateSg==true)
			upAllS<<"iterations" << "\t" << "proposal_g" + toString(g) << "\t" << "accepted_g" + toString(g) << "\t" << "propLL_g" + toString(g) << "\t" << "newLL_g" + toString(g) << "\t" << "oldLL_g" + toString(g) << "\t";

	upAllS<<std::endl;

	std::string hastingsbeta = "hastings_beta.txt";
	std::ofstream hbeta(hastingsbeta.c_str());
	for(unsigned g=0; g<numgroups; ++g){
		for(unsigned int j=0;j<groups[g].numpop;j++){
			hbeta << "groups" + toString(g) + "_beta" + toString(j) << "\t" << "proposed beta" << "\t" << "log_h" << "\t";
	}}
	hbeta << std::endl;

	#ifdef GPERFTOOLS_PROF
		ProfilerStart("profile.prof");
	#endif

	if(burnin==0){
		writeInitialLL(out);
	}else{
		writeInitialLL(burninfile);
	}

	for(int i=1; i<=numIterations; i++){

		makeMCMCStep(hierarchy);

		if(i % iteration_B == 0){
			update_B(hbeta);
		}

		if(i % iteration_jumpallS == 0 && i <= lim_jumpallS){
			update_allStogether(upAllS, i);
		}

		//write values to file
		if(i < burnin && i % thinning == 0){
			writeParameters(burninfile, i);
		}
		else if(i > burnin && i % thinning == 0){
			iterations++;

			//write alphas values
			if(oneAlphaAllGroups==false){
				for(unsigned g=0; g<numgroups; ++g){
					alpha_file[g]<<i;
					for(int l=0; l< sites.numSites; l++){
						alpha_file[g]<<"\t"<<groups[g].tmc->alpha[groups[g].tmc->S[l]];
					}
					alpha_file[g]<<std::endl;
				}
			}else{
				alpha_file[0]<<i;
				for(int l=0; l< sites.numSites; l++){
					alpha_file[0]<<"\t"<<tmcgroups->alpha[tmcgroups->S[l]];
				}
				alpha_file[0]<<std::endl;
			}
			if(posteriorP==true){
				for(unsigned g=0; g<numgroups; ++g){
					p_file[g]<<i;
					for(int l=0; l< sites.numSites; l++){
						p_file[g]<<"\t"<<groups[g].p[l];
					}
					p_file[g]<<std::endl;
				}
			}
			if(numgroups>1){
				A_file<<i;
				for(int l=0; l< sites.numSites; l++){
					A_file<<"\t"<<tmc->alpha[tmc->S[l]];
				}
				A_file<<std::endl;
				if(posteriorP==true){
					P_file<<i;
					for(int l=0; l< sites.numSites; l++){
						P_file<<"\t"<<P[l];
					}
					P_file<<std::endl;
				}
			}

			writeParameters(out, i);

			if(estimateP==true){
				if(posteriorP==true){
					for(int l=0; l< sites.numSites; l++){
						meanP[l] += P[l];
					}
				}
			}
			if(estimatep==true){
				for(unsigned int g=0; g<numgroups; g++){
					if(groups[g].posterior_p==true){
						for(int l=0; l<groups[g].numSites; l++){
							groups[g].mean_p[l] += groups[g].p[l];
						}
					}
				}
			}

			if(iterations % 1000 == 0){
				print_S(estimateS,iterations);
				print_P(posteriorP, iterations);
			}
		}

		//print progress
		prog = i*(100.0/numIterations);
		if(prog > oldProg){
			logfile->listOverFlush("Running MCMC for " + toString(numIterations) + " iterations ... (" + toString(prog) + "%)");

			//write acceptance rates
			std::string acceptance_rates = "acceptance_rates.txt";
			std::ofstream accRates(acceptance_rates.c_str());
			accRates<<"acceptance rates"<<std::endl;

			if(log_a.isEstimated && log_b.isEstimated){
				accRates<<log_a.name<<" "<<(double) log_a.accepted/(log_a.accepted+log_a.rejected)<<std::endl;
				accRates<<log_b.name<<" "<<(double) log_b.accepted/(log_b.accepted+log_b.rejected)<<std::endl;
			}
			if(tmc->lnNu.isEstimated){
				accRates<<tmc->lnNu.name<<" "<<(double) tmc->lnNu.accepted/(tmc->lnNu.accepted+tmc->lnNu.rejected)<<std::endl;
			}
			if(tmc->lnMu.isEstimated){
				accRates<<tmc->lnMu.name<<" "<<(double) tmc->lnMu.accepted/(tmc->lnMu.accepted+tmc->lnMu.rejected)<<std::endl;
			}
			if(tmc->lnkappa.isEstimated){
				accRates<<tmc->lnkappa.name<<" "<<(double) tmc->lnkappa.accepted/(tmc->lnkappa.accepted+tmc->lnkappa.rejected)<<std::endl;
			}
			for(unsigned int g=0; g<numgroups; g++){
				if(groups[g].B.isEstimated){
					accRates<<groups[g].B.name<<" "<<(double) groups[g].B.accepted/(groups[g].B.accepted+groups[g].B.rejected)<<std::endl;
				}
				for(unsigned int j=0; j<groups[g].numpop; j++){
					if(groups[g].pops[j].beta.isEstimated){
						accRates<<groups[g].pops[j].beta.name<<" "<<(double) groups[g].pops[j].beta.accepted/(groups[g].pops[j].beta.accepted+groups[g].pops[j].beta.rejected)<<std::endl;
					}
				}
				if(oneAlphaAllGroups==false){
					if(groups[g].tmc->lnkappa.isEstimated){
						accRates<<groups[g].tmc->lnkappa.name<<" "<<(double) groups[g].tmc->lnkappa.accepted/(groups[g].tmc->lnkappa.accepted+groups[g].tmc->lnkappa.rejected)<<std::endl;
					}
					if(groups[g].tmc->lnMu.isEstimated){
						accRates<<groups[g].tmc->lnMu.name<<" "<<(double) groups[g].tmc->lnMu.accepted/(groups[g].tmc->lnMu.accepted+groups[g].tmc->lnMu.rejected)<<std::endl;
					}
					if(groups[g].tmc->lnNu.isEstimated){
						accRates<<groups[g].tmc->lnNu.name<<" "<<(double) groups[g].tmc->lnNu.accepted/(groups[g].tmc->lnNu.accepted+groups[g].tmc->lnNu.rejected)<<std::endl;
					}
				}
			}

			accRates.close();
		}

		/*auto end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end-start;
		std::cout << "Loop time " << elapsed_seconds.count() << " s!" << std::endl;*/
	}


	#ifdef GPERFTOOLS_PROF
		ProfilerStop();
	#endif

	print_S(estimateS,iterations);

	logfile->overList("Running MCMC for " + toString(numIterations) + " iterations ... done!");

	//close output file
	if(priorVec.hasPriors) out.close();

	//write estimate allele frequencies
	std::string freq_estimation = "freq_estimation.txt";
	std::ofstream f_est(freq_estimation.c_str());
	for(unsigned int g=0; g<numgroups; g++){
		for(int l=0; l<groups[g].numSites; l++){
			f_est<<"p["<<g<<"]["<<l<<"]"<<"\t"<<groups[g].p[l]<<std::endl;
		}
	}
	f_est.close();
	upAllS.close();
	A_file.close();
	if(posteriorP==true){
		P_file.close();
	}
}


void TWorld::writeInitialLL(std::ofstream& name){
	name<<"0";
	if(priorVec.hasPriors){
		name<<"\t";
		priorVec.writeCurrentParameters(name);
	}
	if(CalcTotLL==true){
		//print the total likelihood
		name<<"\t"<<give_totLL();
	}
	name << std::endl;
}

void TWorld::writeParameters(std::ofstream& name, int i){
	name<<i;
	if(priorVec.hasPriors){
		name<<"\t";
		priorVec.writeCurrentParameters(name);
	}
	if(CalcTotLL==true){
		//print the total likelihood
		name<<"\t"<<give_totLL();
	}
	name << std::endl;
}


double TWorld::give_totLL(){
	double totLL = 0.0;
	//mu nu kappas
	if(numgroups>1){
		for(int l=1; l<tmc->sites->numSites; l++){
			totLL += log( tmc->Q[tmc->sites->groupMembership[l]][tmc->S[l-1]][tmc->S[l]]);
		}
	}

	//B & beta
	for(g=0; g<numgroups; g++){
		if(numgroups>1){
			//B
			double* theta_h = new double[groups[g].numSites];
			for(int l=0; l< tmc->sites->numSites; l++){
				theta_h[l] = exp(-tmc->alpha[tmc->S[l]]-groups[g].B.curValue);
			}
			double gamma_theta = 0.0;
			double gamma_thetaP = 0.0;
			double gamma_thetaOneMinusP = 0.0;
			double ratioOfBinomials = 0.0;

			for(int l=0; l<groups[g].numSites; l++){
				gamma_theta += gammln(theta_h[l]);
				gamma_thetaP += gammln(theta_h[l]*P[l]);
				gamma_thetaOneMinusP += gammln(theta_h[l]*(1.0-P[l]));
				ratioOfBinomials += theta_h[l] * (P[l]*log(groups[g].p[l]) + (1.0-P[l])*log(1.0-groups[g].p[l]));
			}
			totLL += gamma_theta - gamma_thetaP - gamma_thetaOneMinusP + ratioOfBinomials;
			delete[] theta_h;
		}
		if(oneAlphaAllGroups==false){
			totLL += groups[g].give_totLLoneGroup();
		}else{
			totLL += groups[g].give_totLLoneGroup(tmcgroups);
		}

	}
	return totLL;
}



void TWorld::findGoodStartingValuesForMCMC(bool estimateS, double sigma2_prior){
	//find good starting values for MCMC inference
	//some variables
	int numSwitches = 6;

	std::cout<<"number of groups: "<<numgroups<<std::endl;
	for(g=0; g<numgroups; g++){
		std::cout<<"group number: "<<g<<std::endl;
		//B
		if(groups[g].B.isEstimated){
			groups[g].findInitialValuesOfB(numSwitches, logfile, P);
		}

		//beta
		groups[g].findInitialValuesOfBeta(numSwitches, logfile);

		if(oneAlphaAllGroups==false){
			//alpha
			if(groups[g].estimateSg==true){
				groups[g].findInitialValuesOfSAndKappa(numSwitches, logfile, groups[g].estimateSg, sigma2_prior);
			}

			//Mu, Nu_up and Nu_down
			if(groups[g].tmc->lnNu.isEstimated)
				groups[g].findInitialValues(&groups[g].tmc->lnNu, numSwitches, logfile);
			if(groups[g].tmc->lnMu.isEstimated)
				groups[g].findInitialValues(&groups[g].tmc->lnMu, numSwitches, logfile);
		}
	}

	if(tmc->lnNu.isEstimated)
		findInitialValues(&tmc->lnNu, numSwitches, logfile);
	if(tmc->lnMu.isEstimated)
		findInitialValues(&tmc->lnMu, numSwitches, logfile);

	//A_max
	findInitialValuesOfSAndKappa(numSwitches, logfile, estimateS, sigma2_prior);

	//calc initial gamma terms to get started
	if(numgroups>1){
		for(g=0; g<numgroups; ++g){
			groups[g].calcInitialGammaTerms(P, tmc);
		}
	}else{
		groups[0].calcInitialGammaTerms();
	}
}


double TWorld::calcSumlnLhdQ(){
	double sum = 0.0;
	sum += tmc->upsumlnLhdQ();
	return sum;
}


void TWorld::findInitialValues(TPrior* param, const int & numSwitches, TLog* logfile){
	std::cout<<"starting the function findInitialValues "<<std::endl;
	//start
	double min = -10.0;
	double max = 0.0;
	if(param->hasMinMax){
		min = param->min;
		max = param->max;
	}
	int i;
	int s = 0;
	double step = 0.2;
	double new_llQ = 0.0;
	double old_llQ = calcSumlnLhdQ();
	std::cout<<"find initial value: old_llQ "<<old_llQ<<std::endl;
	//now loop
	while(s < numSwitches){
		for(i=0; i<101; ++i){
			param->oldValue = param->curValue;
			param->curValue += step;
			if(param->curValue > max || param->curValue < min){
				param->curValue = param->oldValue;
				step = -step/2.71828;
				++s;
				break;
			}
			new_llQ = calcSumlnLhdQ();
			if(old_llQ > new_llQ){
				old_llQ = new_llQ;
				step = -step/2.71828;
				++s;
				break;
			}
			old_llQ = new_llQ;
		}
	}
	logfile->conclude("Estimated initial " + param->name + " = " + toString(param->curValue));
	std::cout<<"ending the function "<<std::endl;
}




void TWorld::findInitialValuesOfSAndKappa(const int & numSwitches, TLog* logfile, bool estimateS, double sigma2_prior){
	//to change
	if(tmc->alphaMax.isEstimated){
		//variables
		double derivative, oldDerivative;
		double this_alpha;
		double step;
		int s;
		double* alpha_estimates = new double[sites.numSites];
		double sign;
		int i;

		//estimate for each locus
		std::string progressString = "Estimating initial alpha for each locus ...";
		logfile->listFlush(progressString);
		int prog = 0; int oldProg = 0;

		std::string Avalues = "A_values.txt";
		std::ofstream Aval(Avalues.c_str());

		for(int l=0; l<sites.numSites; ++l){
			//start
			s = 0;
			this_alpha = 0.0;
			oldDerivative = 0.0;
			for(g=0;g<numgroups;g++){
				double this_theta = exp(-this_alpha-groups[g].B.curValue);
				oldDerivative += this_theta*groups[g].calcDerivativeOfTheta(this_theta, P) + this_alpha/sigma2_prior;
			}
			if(oldDerivative < 0.0) step = 1.0;
			else if(oldDerivative > 0.0) step = -1.0;
			else s = numSwitches;

			//now loop
			while(s < numSwitches){
				for(i=0; i<101; ++i){
					this_alpha += step;

					derivative = 0.0;
					for(g=0;g<numgroups;g++){
						double this_theta = exp(-this_alpha-groups[g].B.curValue);
						derivative += this_theta*groups[g].calcDerivativeOfTheta(this_theta, P) + this_alpha/sigma2_prior;
					}

//					std::cout<<"locus: "<<l<< "  step: "<<step << "  derivative: "<< derivative<<"  of alpha  "<<this_alpha<<"  at iteration "<<i <<std::endl;

					sign = derivative / oldDerivative;
					if(sign < 0.0){
						oldDerivative = derivative;
						step = -step/2.71828;
						++s;
						break;
					}/*else if((derivative < 0.0 && derivative < oldDerivative) || (derivative > 0.0 && derivative > oldDerivative)){
						this_alpha = 0.0;
						s = numSwitches;
						break;
					}*/
					oldDerivative = derivative;

					//checks
					if(i == 100){
						//set alpha = 0.0;
						this_alpha = 0.0;
						std::cout << "failed for this locus!" << std::endl;
						break;
					}
				}
				if(i==100) break;
			}

			Aval<<this_alpha<<std::endl;

			//now set alpha
			alpha_estimates[l] = this_alpha;
//			std::cout << "Locus " << l << " alpha = " << this_alpha << std::endl;

			//report progress
			prog = (double) l / (double) sites.numSites * 100.0;
			if(prog > oldProg){
				logfile->listOverFlush(progressString + " (" + toString(prog) + "%)");
				oldProg = prog;
			}
		}
		logfile->overList(progressString + " done! ");

		//set alpha_max
		double maxAlpha = 0.0;
		for(int l=0; l<sites.numSites; ++l){
			if(fabs(alpha_estimates[l]) > maxAlpha){
				maxAlpha = fabs(alpha_estimates[l]);
			}
		}
		tmc->setAlphaMax(maxAlpha);
		Aval.close();

		logfile->conclude("Estimated alpha_max = " + toString(tmc->alphaMax.curValue));

		//now set all S
		if(estimateS==true)
			tmc->setSAccordingToAlphaEstimates(alpha_estimates);

		//and estimate lnkappa
		if(tmc->lnkappa.isEstimated){
			tmc->estimateKappaFromS();
			if(tmc->lnkappa.curValue < tmc->lnkappa.min) tmc->setKappa(tmc->lnkappa.min+1.0);
			logfile->conclude("Estimated initial lnkappa = " + toString(tmc->lnkappa.curValue));
		}else{logfile->conclude("Fixed lnkappa = " + toString(tmc->lnkappa.curValue));}

		//clean up
		delete[] alpha_estimates;
	}
}


void TWorld::print_S(bool estimateS, int iterations){
	if(estimateS==true){
		std::string S_values = "S_values.txt";
		std::ofstream S_file(S_values.c_str());

		for(int l=0; l< sites.numSites; l++){
			S_file<<"S["<<l<<"]: "<<tmc->S[l] - s_max<<std::endl;
		}

		S_file.close();

	}

	if(oneAlphaAllGroups==false){
		for(unsigned g=0; g<numgroups; ++g){
			if(groups[g].estimateSg==true){
				std::string Sg_values = "Sg" + toString(g) + "_values.txt";
				std::ofstream Sg_file(Sg_values.c_str());
				for(int l=0; l< sites.numSites; l++){
					Sg_file<<"S["<<g<<"]["<<l<<"]: "<<groups[g].tmc->S[l] - s_max<<std::endl;
				}
				Sg_file.close();
			}
		}
	}else{
		if(estimateSg==true){
			std::string Sg_values = "Sg_values.txt";
			std::ofstream Sg_file(Sg_values.c_str());

			for(int l=0; l< sites.numSites; l++){
				Sg_file<<"S["<<l<<"]: "<<tmcgroups->S[l]<<std::endl;
			}

			Sg_file.close();
		}
	}
}

void TWorld::print_P(bool posteriorP, int iterations){
	double N=0.0;
	N = (double) iterations;
	if(posteriorP==true){
		if(numgroups>1){
			//print output of P mean and standard deviation
			std::string path_P = "Posterior_P.txt";
			std::ofstream path(path_P.c_str());
			path<<"-\tmean"<<std::endl;
			for(int l=0; l< sites.numSites; l++){
				path<<l<<"\t"<<meanP[l]/N<<std::endl;
			}
			path.close();
		}
		for(unsigned g=0; g<numgroups; ++g){
			if(groups[g].posterior_p==true){
				std::string pg_values = "Posterior_pg" + toString(g) + ".txt";
				std::ofstream pg_file(pg_values.c_str());
				pg_file<<"-\tmean"<<std::endl;
				for(int l=0; l< sites.numSites; l++){
					pg_file<<l<<"\t"<<groups[g].mean_p[l]/N<<std::endl;
				}
				pg_file.close();
			}
		}
	}
}


void TWorld::update_Amax(){
	//update A_max in world
	//propose new value of A_max and get prior density
	if(tmc->alphaMax.isEstimated){
		double log_h = tmc->proposeNewAlphaMax();

		for(g=0;g<numgroups;g++){
			log_h += groups[g].calcGammaFuncForAB(P, tmc);
		}

		//accept or reject
		if(!tmc->acceptOrRejectAlphamax(log_h)){
			//reject

			for(g=0;g<numgroups;g++){
				groups[g].reject_ThetasAndGammas();
			}
		}
	}

	//update alpha_max in each group
	if(oneAlphaAllGroups==false){
		for(g=0; g<numgroups; g++){
			groups[g].update_alphamax();
		}
	}else{
		if(tmcgroups->alphaMax.isEstimated){
			update_alphamaxAllGroups();
		}
	}
}

void TWorld::update_alphamaxAllGroups(){
	if(tmcgroups->alphaMax.isEstimated){
		//propose new value of alpha_max and get prior density
		double log_h = tmcgroups->proposeNewAlphaMax();

		//calc hastings
		for(g=0; g<numgroups; g++)
			log_h += groups[g].calculateHastingsRatio_alphaMax();

		//accept or reject
		if(!tmcgroups->acceptOrRejectAlphamax(log_h)){
			//reject
			for(g=0; g<numgroups; g++)
				groups[g].rejectAlphamax();
		}
	}
}

void TWorld::update_B(std::ostream & hbeta){
	//update B and beta in each group
	//#pragma omp parallel for
	if(oneAlphaAllGroups==false){
		for(g=0; g<numgroups; g++){
			groups[g].update_B(P, tmc,hbeta);
		}
		hbeta << std::endl;
	}else{
		for(g=0; g<numgroups; g++){
			groups[g].update_B(P, tmc, hbeta, tmcgroups);
		}
		hbeta << std::endl;
	}
}


void TWorld::update_allStogether(std::ofstream& upAllS, int iterations){
	double log_h = 0.0;
	double prop_LL = 0.0;
	double old_LL = 0.0;
	int cost = 1;

	//update all S together for the higher hierarchy
	if(numgroups>1){
		if(estimateS==true){
			log_h = 0.0;
			int* newS = new int[tmc->sites->numSites];
			int nStates = tmc->numStates-1;

			cost = 0;
			int min = *std::min_element(tmc->S, tmc->S + tmc->sites->numSites);
			int max = tmc->numStates - *std::max_element(tmc->S, tmc->S + tmc->sites->numSites);
			cost = randomGenerator->getRand(-min, max);

			if(cost==0){
				goto endupSw;
			}

			for(g=0; g<numgroups; g++){
				double proposedValue = groups[g].B.curValue - (double)cost * tmc->CalcDiff();
				groups[g].B.proposeNewValue(proposedValue);
				log_h += groups[g].B.logPriorRatio;
			}

			//shift all S
			for(int l=0; l<tmc->sites->numSites; l++){
				newS[l] = tmc->S[l] + cost;
				if(newS[l] < 0) newS[l] = 0;
				else if(newS[l] > nStates) newS[l] = nStates;
			}

			//add to hastings
			//Q term
			//first locus
			log_h += log( tmc->Q[tmc->sites->groupMembership[0]][0][newS[0]]);
			log_h -= log( tmc->Q[tmc->sites->groupMembership[0]][0][tmc->S[0]]);

			//all other loci
			for(int l=1; l<tmc->sites->numSites; l++){
				log_h += log( tmc->Q[tmc->sites->groupMembership[l]][newS[l-1]][newS[l]]);
				log_h -= log( tmc->Q[tmc->sites->groupMembership[l]][tmc->S[l-1]][tmc->S[l]]);
			}

			//EM term
			for(int l=0; l<tmc->sites->numSites; l++){
				if(newS[l] == tmc->S[l]){
					for(g=0; g<numgroups; g++)
						log_h += groups[g].calcHastingsRatioLocus(P, tmc, l);
				}
			}

			prop_LL = old_LL + log_h;

			if( log(randomGenerator->getRand()) < log_h ){
				//accept
				for(int l=0; l<tmc->sites->numSites; l++){
					if(newS[l] == tmc->S[l]){
						for(g=0; g<numgroups; g++){
							groups[g].changeGammas(l);
						}
					}
					tmc->changeS(l, newS[l]);
				}
			}else{
				//reject
				for(g=0; g<numgroups; g++)
					groups[g].B.RejectProposedValue();
				for(int l=1; l<tmc->sites->numSites; l++){
					if(newS[l] == tmc->S[l]){
						//reject
						tmc->S[l] = tmc->S_old[l];
						if(oneAlphaAllGroups==false){
							for(g=0;g<numgroups;g++)
								groups[g].rejectGammaOneSite(l);
						}
					}
				}
			}
			endupSw: ;
		}
	}

	//update all S together for each group
	if(oneAlphaAllGroups==false){
		for(unsigned int g=0; g<numgroups; g++){
			log_h = 0.0;
			prop_LL = 0.0;
			old_LL = groups[g].give_totLLoneGroup();
			bool accepted = false;
			int _min = 0;
			int _max = 0;
			int cost = 0;

			groups[g].update_allStogetherGroup(log_h,prop_LL,old_LL,accepted,_min,_max,cost);

			upAllS<<iterations<<"\t"<<cost<<"\t"<<_min<<"\t"<<_max<<"\t" << accepted << "\t" << log_h << "\t" << prop_LL << "\t" << groups[g].give_totLLoneGroup() << "\t" << old_LL << "\t";
		}
	}else{
		log_h = 0.0;
		prop_LL = 0.0;
		bool accepted = false;
		int _min = 0;
		int _max = 0;
		int cost = 0;
		for(unsigned int g=0; g<numgroups; g++){
			old_LL = groups[g].give_totLLoneGroup(tmcgroups);
		}
		update_allStogetherGroups(log_h,prop_LL,old_LL,accepted,_min,_max,cost);
		upAllS<<iterations<<"\t"<<cost<<"\t"<<_min<<"\t"<<_max<<"\t" << accepted << "\t" << log_h << "\t" << prop_LL << "\t" << old_LL << "\t";
	}

 	upAllS<<std::endl;
}


void TWorld::update_allStogetherGroups(double & log_h, double & prop_LL, double & old_LL, bool & accepted, int & _min, int & _max, int & cost){
	if(estimateSg==true){
		int* newS = new int[tmcgroups->sites->numSites];

		_min = *std::min_element(tmcgroups->S, tmcgroups->S + tmcgroups->sites->numSites);
		_max = tmcgroups->numStates - *std::max_element(tmcgroups->S, tmcgroups->S + tmcgroups->sites->numSites);
		cost = randomGenerator->getRand(-_min, _max);

		if(cost == 0) goto endupallSg;

		for(unsigned int g=0; g<numgroups; g++){
			for(unsigned int j=0;j<groups[g].numpop;j++){
				double proposedValue = groups[g].pops[j].beta.curValue - (double)cost * tmcgroups->CalcDiff();
				groups[g].pops[j].beta.proposeNewValue(proposedValue);
				log_h += groups[g].pops[j].beta.logPriorRatio;
			}
		}

		//shift all S
		for(int l=0; l<tmcgroups->sites->numSites; l++){
			newS[l] = tmcgroups->S[l] + cost;
		}

		//add to hastings
		//Q term
		//first locus
		log_h += log( tmcgroups->Q[tmcgroups->sites->groupMembership[0]][0][newS[0]]);
		log_h -= log( tmcgroups->Q[tmcgroups->sites->groupMembership[0]][0][tmcgroups->S[0]]);

		//all other loci
		for(int l=1; l<tmc->sites->numSites; l++){
			log_h += log( tmcgroups->Q[tmcgroups->sites->groupMembership[l]][newS[l-1]][newS[l]]);
			log_h -= log( tmcgroups->Q[tmcgroups->sites->groupMembership[l]][tmcgroups->S[l-1]][tmcgroups->S[l]]);
		}

		//EM term
		for(int l=0; l<tmcgroups->sites->numSites; l++){
			if(newS[l] == tmcgroups->S[l]){
				for(unsigned int g=0; g<numgroups; g++){
					for(unsigned int j=0;j<groups[g].numpop;j++)
						log_h += groups[g].pops[j].logHastingsTermS(l, groups[g].p[l], groups[g].logP[l], groups[g].logPMinus1[l], tmcgroups);
				}
			}
		}

		prop_LL = old_LL + log_h;

		if(log(randomGenerator->getRand()) <  log_h ){
			//accept
			accepted = true;
			for(int l=0; l<tmcgroups->sites->numSites; l++){
				if(newS[l] == tmcgroups->S[l]){
					for(unsigned int g=0; g<numgroups; g++){
						for(unsigned int j=0;j<groups[g].numpop;j++)
							groups[g].pops[j].acceptNewS(l);
					}
				}
				tmc->changeS(l, newS[l]);
			}
		} else {
			//reject
			for(unsigned int g=0; g<numgroups; g++){
				for(unsigned int j=0;j<groups[g].numpop;j++)
					groups[g].pops[j].beta.RejectProposedValue();
			}
			for(int l=1; l<tmcgroups->sites->numSites; l++){
				if(newS[l] == tmcgroups->S[l]){
					for(unsigned int g=0; g<numgroups; g++){
						for(unsigned int j=0;j<groups[g].numpop;j++)
							groups[g].pops[j].rejectNewS(l);
					}
				}
			}
		}
		endupallSg: ;
	}
}




void TWorld::update_P(){
	if(estimateP==true){
		//update P in world

		//update P for each locus independently
		for(int l=0;l<sites.numSites;l++){
			//propose new P and calc prior ratio
			P_old[l] = P[l];
			P[l] = randomGenerator->getNormalRandom(P_old[l], 0.01);
			if(P[l] < 0.0){
				P[l] = -P[l];
			} else if(P[l] > 1.0){
				P[l] = 2.0 - P[l];
			}

			//check we are within (0,1)
			if(P[l] < 0.0000000001){
				P[l] = 0.0000000001;
			} else if(P[l] > 0.9999999999){
				P[l] = 0.9999999999;
			}

			//calc hastings across all groups
			double log_h = (log_a.curExpVal - 1.0)*log(P[l]/P_old[l]) + (log_b.curExpVal - 1.0)*log((1.0-P[l])/(1.0-P_old[l]));
			for(g=0;g<numgroups;g++){
				log_h += groups[g].calcGammaFuncForP(P[l],P_old[l],l);
			}

			//accept or reject
			if(log_h < log(randomGenerator->getRand())){
				P[l] = P_old[l];
				for(g=0; g<numgroups; g++){
					groups[g].rejectPup(l);
				}
			} else {
				for(g=0; g<numgroups; g++){
					groups[g].acceptP(P[l], l);
				}
			}
		}
	}

	//update all p in each group
	if(estimatep==true){
		if(numgroups>1){
			for(g=0; g<numgroups; g++){
				groups[g].update_p(P);
			}
		}else{
			groups[0].update_p(log_a, log_b);
		}
	}
}


void TWorld::calcGammaFor_betapar(){    //to change
	lngamma_aplusb = gammln(log_a.curExpVal + log_b.curExpVal);
}


void TWorld::switchGammaFor_betapar(){
	old_lngamma_aplusb = lngamma_aplusb;
}


void TWorld::rejectGammaFor_betapar(){
	lngamma_aplusb = old_lngamma_aplusb;
}


double TWorld::logHastings_a(bool hierarchy){
	double log_h = 0.0;
	log_h = (double) sites.numSites * (lngamma_aplusb + gammln(log_a.oldExpVal) - old_lngamma_aplusb - gammln(log_a.curExpVal));
	double a_minus_old_a = log_a.curExpVal - log_a.oldExpVal;
	if(hierarchy){
		for(int l=0; l<sites.numSites; l++){
			log_h += a_minus_old_a * log(P[l]) ;
		}
	}else{
		for(int l=0; l<sites.numSites; l++){
			log_h += a_minus_old_a * log(groups[0].p[l]) ;
		}
	}
	return log_h;
}

double TWorld::logHastings_b(bool hierarchy){
	double log_h = 0.0;
	log_h = (double) sites.numSites * (lngamma_aplusb + gammln(log_b.oldExpVal) - old_lngamma_aplusb - gammln(log_b.curExpVal));
	double b_minus_old_b = log_b.curExpVal - log_b.oldExpVal;
	if(hierarchy){
		for(int l=0; l<sites.numSites; l++){
			log_h += b_minus_old_b * log(1.0-P[l]) ;
		}
	}else{
		for(int l=0; l<sites.numSites; l++){
			log_h += b_minus_old_b * log(1.0-groups[0].p[l]) ;
		}
	}
	return log_h;
}

void TWorld::update_beta_parameter(bool hierarchy, TNormalPriorWithExpVal & log_par){
	if(log_par.isEstimated){
		log_par.proposeNewValue();
		switchGammaFor_betapar();
		calcGammaFor_betapar();
		log_par.calcLogPriorRatio();
		double log_h = log_par.logPriorRatio;

		if(log_par.name == "log_a"){
			log_h += logHastings_a(hierarchy);
		}else if(log_par.name == "log_b"){
			log_h += logHastings_b(hierarchy);
		}

		//accept or reject
		if(log_h < log(randomGenerator->getRand())){
			//reject
			log_par.RejectProposedValue();
			rejectGammaFor_betapar();
 		}
 	}
 }


void TWorld::update_S(){
	if(estimateS==true){
		//update all S in world

		//do for each site individually (but not independently!)
//		#pragma omp parallel for
		for(int l=0; l<sites.numSites; l++){
			tmc->S_old[l] = tmc->S[l];
			//propose new value
			double log_h = tmc->proposeNewS(l);
			//calc hastings
			for(g=0;g<numgroups;g++){
				log_h += groups[g].calcGammaFuncOneSite(P[l], tmc, l);
			}
			//accept or reject
			double rand;
//            #pragma omp critical
//			{
				rand = randomGenerator->getRand(0.0,1.0);
//			}

			if(log_h < log(rand)){
				//reject
				tmc->S[l] = tmc->S_old[l];
				for(g=0;g<numgroups;g++){
					groups[g].rejectGammaOneSite(l);
				}
			}else{
				//accept
//				#pragma omp critical
//				{
					for(g=0;g<numgroups;g++){
						groups[g].acceptGammaOneSite(P[l], l);
//					}
				}
			}
		}
		if(oneAlphaAllGroups==true){
			update_SoneAlphaAllGroups();
		}
	}
	if(oneAlphaAllGroups==false){
		for(g=0; g<numgroups; g++){
			if(groups[g].estimateSg==true){
				//update S in each group
				groups[g].update_S();
			}
		}
	}
}

void TWorld::update_SoneAlphaAllGroups(){
	for(int l=0; l<sites.numSites; l++){
		tmcgroups->S_old[l] = tmcgroups->S[l];
		//propose new value
		double log_h = tmcgroups->proposeNewS(l);

		//sum over populations
		for(g=0; g<numgroups; g++){
			for(unsigned int j=0;j < groups[g].numpop;j++){
				log_h += groups[g].pops[j].logHastingsTermS(l, groups[g].p[l], groups[g].logP[l], groups[g].logPMinus1[l], tmcgroups);
			}
		}

		//accept or reject
		double rand;
//            #pragma omp critical
//			{
			rand = randomGenerator->getRand(0.0,1.0);
//			}

		if(log_h < log(rand)){
			//reject
			tmcgroups->S[l] = tmcgroups->S_old[l];
			for(g=0;g<numgroups;g++){
				groups[g].reject_Sg(l);;
			}
		}else{
			//accept
//			#pragma omp critical
//			{
			for(g=0;g<numgroups;g++){
				for (unsigned int j = 0; j < groups[g].numpop; j++){
					groups[g].pops[j].acceptNewS(l);
				}
			}
		}
	}
}


void TWorld::update_Kappa(){
	//update lnkappa in world
	tmc->updateKappa();

	//update lnkappa for each group
	if(oneAlphaAllGroups==false){
		for(unsigned int g=0; g<numgroups; g++){
			groups[g].update_kappa();
		}
	}else{
		tmcgroups->updateKappa();
	}
}

void TWorld::update_mu(){
	//update lnMu in world
	tmc->update_mu();

	//update lnMu for each group
	if(oneAlphaAllGroups==false){
		for(unsigned int g=0; g<numgroups; g++){
			groups[g].update_mu();
		}
	}else{
		tmcgroups->update_mu();
	}
}

void TWorld::update_nu(){
	//update nu_up and nu_down in world
	tmc->update_nu();
	//update nu_up and nu_down for each group
	if(oneAlphaAllGroups==false){
		for(unsigned int g=0; g<numgroups; g++){
			groups[g].update_nu();
		}
	}else{
		tmcgroups->update_nu();
	}
}


void TWorld::makeMCMCStep(bool hierarchy){
	update_Amax();
	update_P(); //Parallel
	update_S(); //Parallel

	update_Kappa();
	update_mu();
	update_nu();
	update_beta_parameter(hierarchy, log_a);
	update_beta_parameter(hierarchy, log_b);
}

void TWorld::writeData(std::string filename){
	logfile->listFlush("Writing data to file '" + filename + "' ...");
	//open file
	std::ofstream out(filename.c_str());
	if(!out) throw "File '" + filename + "' can not be opened for writing!";

	//write header
	out << "-\t-";
	for(g=0; g<numgroups; g++){
		groups[g].writeGroupHeader(out);
	}
	out << "\n-\t-";
	for(g=0; g<numgroups; g++){
		groups[g].writePopHeader(out);
	}
	out << "\n";

	//write data: parse through sites
	sites.begin();
	int l = 0;
	while(sites.writeNextSite(out)){
		for(g=0; g<numgroups; g++){
			groups[g].writeData(out, l);
		}
		++l;
		out << "\n";
	}

	//close the file
	out.close();
	logfile->write(" done!");
}


void TWorld::filterData(std::string filename, TParameters & params){
	logfile->listFlush("Writing data to file '" + filename + "' ...");
	//open file
	std::ofstream out(filename.c_str());
	if(!out) throw "File '" + filename + "' can not be opened for writing!";

	int allpop = 0;
	//write header
	out << "-\t-";
	for(g=0; g<numgroups; g++){
		groups[g].writeGroupHeader(out);
	}
	out << "\n-\t-";
	for(g=0; g<numgroups; g++){
		groups[g].writePopHeader(out);
		allpop += groups[g].numpop;
	}
	out << "\n";

	//write data: parse through sites
	sites.begin();

	//I ask the minor allele frequencies but I use the Major allele frequencies in the calculation
	if(params.parameterExists("missedPop"))
		filter_missedPop(params, out);
	else if(params.parameterExists("minVar"))
		filter_minVar(params, out);
	else if(params.parameterExists("minPop"))
		filter_minPop(params, out);
	else
		filter_countslimit(params, allpop, out);

	//close the file
	out.close();
	logfile->write(" done!");
}




void TWorld::filter_countslimit(TParameters & params, int allpop, std::ofstream& out){
	int countslim = 0;
	if(params.parameterExists("countslimit")){
		countslim = params.getParameterInt("countslimit");
	}
	double MAFlimit = 1.0 - params.getParameterDoubleWithDefault("maf", 0.0);
	int haploMin = params.getParameterIntWithDefault("haploMin", 0);
	bool check = true;
	for(int l=0; l<sites.numSites; l++){
		double sum = 0.0;
		int sumcounts = 0;
		int diff = 0;

		for(unsigned int g=0; g<numgroups; g++){
			for(unsigned int j=0; j<groups[g].numpop; j++){
				if(groups[g].pops[j].N[l]<haploMin) goto label;
				sum += (double)groups[g].pops[j].n[l]/groups[g].pops[j].N[l];
				sumcounts += groups[g].pops[j].n[l];
				if(groups[g].pops[j].N[l] != 0)
					diff += groups[g].pops[j].N[l] - groups[g].pops[j].n[l];
			}
		}
		if(params.parameterExists("countslimit")){
			if(sumcounts <= countslim || diff <= countslim){
				check = false;
			}
		}else{
			sum = sum/allpop;
			if(sum > MAFlimit || (1.0-sum) > MAFlimit){
				check = false;
			}
		}
		if(check == true){
			sites.writeNextSite(out);
			for(unsigned int g=0; g<numgroups; g++){
				groups[g].writeData(out, l);
			}
			out << "\n";
		}else{
			label:
			sites.goNextSite();
		}
		check = true;
	}
}

void TWorld::filter_missedPop(TParameters & params, std::ofstream& out){
	int missedPop = params.getParameterInt("missedPop");
	for(unsigned int g=0; g<numgroups; g++){
		numTotPops+=groups[g].numpop;
	}
	for(int l=0; l<sites.numSites; l++){
		if(params.parameterExists("missedPop")){
			int NumPop=0;
			for(unsigned int g=0; g<numgroups; g++){
				for(unsigned int j=0; j<groups[g].numpop; j++){
					if(groups[g].pops[j].N[l]==0){
						NumPop++;
					}
				}
			}
			if(missedPop>=numTotPops-NumPop)
				sites.goNextSite();
			else{
				sites.writeNextSite(out);
				for(g=0; g<numgroups; g++){
					groups[g].writeData(out, l);
				}
				out << "\n";
			}
		}
	}
}

void TWorld::filter_minVar(TParameters & params, std::ofstream& out){
	int minVar = params.getParameterInt("minVar");
	for(unsigned int g=0; g<numgroups; g++){
		numTotPops+=groups[g].numpop;
	}
	for(int l=0; l<sites.numSites; l++){
		int NumPop=0;
		for(g=0; g<numgroups; g++){
			for(unsigned int j=0; j<groups[g].numpop; j++){
				if(groups[g].pops[j].n[l]==0 || groups[g].pops[j].n[l]==groups[g].pops[j].N[l]){
					NumPop++;
				}
			}
		}
		if(minVar>=numTotPops-NumPop)
			sites.goNextSite();
		else{
			sites.writeNextSite(out);
			for(g=0; g<numgroups; g++){
				groups[g].writeData(out, l);
			}
			out << "\n";
		}
	}
}

void TWorld::filter_minPop(TParameters & params, std::ofstream& out){
	int minPop = params.getParameterInt("minPop");
	for(int l=0; l<sites.numSites; l++){
		int first=0;
		int second=0;
		for(unsigned int g=0; g<numgroups; g++){
			for(unsigned int j=0; j<groups[g].numpop; j++){
				if(groups[g].pops[j].N[l] > 0){
					if(groups[g].pops[j].n[l] > 0){
						first++;
					}
					if(groups[g].pops[j].n[l] < groups[g].pops[j].N[l]){
						second++;
					}
				}
			}
		}
		if(first>=minPop && second>=minPop){
			sites.writeNextSite(out);
			for(unsigned int g=0; g<numgroups; g++){
				groups[g].writeData(out, l);
			}
			out << "\n";
		}else
			sites.goNextSite();
	}
}



//-------------------------------------------------------
// Calc LL surfaces
//-------------------------------------------------------
void TWorld::calculateLLsurfaces(TParameters & params){
	//initialize populations and sites
	dataFile = params.getParameterString("data");
	readSitesGroupsAndPopulations(dataFile, params);
	//read and set parameters
	logfile->startIndent("Reading parameters to conduct simulation:");
	readAndSetParameters(params);
	//estimate frequencies from data
	estimateFrequenciesFromData(params);
	logfile->endIndent();

	//reading S and Sg from files
	InitializeS(params);
	//now calculate surfaces
	if(params.parameterExists("LLsurface_Amax"))
		calcLikelihoodSurfaceAmax(outNameTag + "_LLsurface_Amax.txt", params);

	if(params.parameterExists("LLsurface_B"))
		calcLikelihoodSurfaceB(outNameTag + "_LLsurface_B.txt", params);

	if(params.parameterExists("LLsurface_alphamax"))
		calcLikelihoodSurface_alphamax(outNameTag + "_LLsurface_alphamax.txt", params);

	if(params.parameterExists("LLsurface_beta"))
		calcLikelihoodSurface_beta(outNameTag + "_LLsurface_beta.txt", params);

	if(params.parameterExists("LLsurface_P"))
		calcLikelihoodSurface_P(outNameTag + "_LLsurface_P.txt");

	if(params.parameterExists("LLsurface_p"))
		calcLikelihoodSurface_p(outNameTag + "_LLsurface_p.txt", params);

	if(params.parameterExists("LLsurface_lnK"))
		calcLikelihoodSurfacelnK(outNameTag + "_LLsurface_lnK.txt", params);

	if(params.parameterExists("LLsurface_lnkappa"))
		calcLikelihoodSurfacelnkappa(outNameTag + "_LLsurface_lnkappa.txt", params);

	if(params.parameterExists("LLsurface_Sworld"))
		calcLikelihoodSurfaceSworld(outNameTag + "_LLsurface_Sworld.txt");

	if(params.parameterExists("LLsurface_Sgroups"))
		calcLikelihoodSurfaceSgroups(outNameTag + "_LLsurface_Sgroups.txt");

	if(params.parameterExists("LLsurface_a"))
		calcLikelihoodSurface_a(outNameTag + "_LLsurface_a.txt", params);
}


void TWorld::calcLikelihoodSurfaceAmax(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface for Amax ...");
	double limit = params.getParameterDoubleWithDefault("limit", 10.0);
	double step = 0.05;
	double LL = 0.0;
	std::ofstream outLL(filename.c_str());
	outLL << "A_max\tTheta\tLL"<<"\n";

	for(int i=0; i<(int)(limit/step); ++i){
		tmc->setAlphaMax(0.0 + i*step);

		//calc likelihood
		LL = 0.0;
		for(g=0; g<numgroups; g++){
			LL += groups[g].calcGammaFuncForAB(P, tmc);
		}

		//print
		outLL << tmc->alphaMax.curValue << "\t" << groups[0].Theta[0] << "\t" << LL << "\n";

		//reset
		tmc -> switch_alpha();
		for(g=0; g<numgroups; g++)
			groups[g].reject_ThetasAndGammas();
	}
	outLL.close();
	logfile->write(" done!");
}

void TWorld::calcLikelihoodSurfaceB(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface for B ...");
	double limit = params.getParameterDoubleWithDefault("limit", -10.0);
	int group = params.getParameterIntWithDefault("group", 0);
	double step = 0.005;
	double LL = 0.0;

	std::ofstream outLL_B(filename.c_str());
	outLL_B<<"B\tTheta\tLL"<<"\n";

	double probB;
	for(int i=0; i<(int)(-200*limit); ++i){
		probB = limit + i*step;

		//calc likelihood
		LL = 0.0;
		/*
		for(g=0; g<numgroups; g++){
			groups[g].set_B(probB);
			LL += groups[g].calcGammaFuncForAB(P, tmc);
		}
		*/

/*		for(g=0; g<numgroups; g++)
			groups[g].set_B(probB);
*/
		groups[group].set_B(probB);
		LL += groups[group].calcGammaFuncForAB(P, tmc);

		//print
		outLL_B << probB << "\t" << groups[group].Theta[0] << "\t" << LL << "\n";

		//reset
/*		for(g=0; g<numgroups; g++)
			groups[g].reject_ThetasAndGammas();
*/
		groups[group].reject_ThetasAndGammas();
	}
	outLL_B.close();
	logfile->write(" done!");
}

void TWorld::calcLikelihoodSurface_alphamax(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface for alphamax ...");
	double limit = params.getParameterDoubleWithDefault("limit", 10.0);
	int g = params.getParameterIntWithDefault("group", 0);
	double step = 0.05;
	double LL = 0.0;

	std::ofstream outLL(filename.c_str());
	outLL<<"alpha_max\tLL"<<"\n";

	double alphaMax;
	for(int i=0; i<(int)(limit/step); ++i){
		alphaMax = 0.0 + i*step;
		//calc likelihood
		LL = 0.0;
		groups[g].setAlphaMax(alphaMax);
		LL = groups[g].calculateHastingsRatio_alphaMax();

		//print
		outLL << groups[g].tmc->alphaMax.curValue << "\t" << LL << "\n";
		groups[g].rejectAlphamax();
	}
	outLL.close();
	logfile->write(" done!");
}


void TWorld::calcLikelihoodSurface_beta(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface for beta ...");
	double limit = params.getParameterDoubleWithDefault("limit", -10.0);
	int pop = params.getParameterIntWithDefault("pop", 0);
	int g = params.getParameterIntWithDefault("group", 0);
	double step = 0.1;
	double LL = 0.0;

	std::ofstream outLL_beta(filename.c_str());
	outLL_beta<<"beta\tTheta\tLL"<<"\n";

	double probB;
	for(int i=0; i<(int)(-10*limit); ++i){
		probB = limit + i*step;
		//calc likelihood
		LL = 0.0;
		groups[g].set_beta(probB);
//		groups[g].pops[pop].switchThetaAndEulerMacLaurinTerms();
//		groups[g].pops[pop].fillThetaAndEulerMacLaurinTerms(groups[g].p, groups[g].logP, groups[g].logPMinus1, groups[g].tmc);
//		LL=groups[g].give_totLLoneGroup();

//		LL = groups[g].calculateLL_beta(pop);
		LL=groups[g].pops[pop].calcThetaAndEulerMacLaurinTerms(groups[g].p, groups[g].logP, groups[g].logPMinus1, groups[g].tmc);

		//print
		outLL_beta << probB << "\t" << groups[g].pops[pop].theta[0] << "\t" << LL << "\n";
		groups[g].reject_beta();
	}
	outLL_beta.close();
	logfile->write(" done!");
}


void TWorld::calcLikelihoodSurface_P(std::string filename){
	logfile->listFlush("Calculating LL-surface ...");
	double start = 0.01;
	double step = 0.01;
	double LL = 0.0;
	std::ofstream outLL(filename.c_str());
	outLL<<"P\tLL"<<"\n";

	std::string print_sum = "LLsurface_sum.txt";
	std::ofstream LL_sum(print_sum.c_str());
	LL_sum << "P\tLL"<<"\n";

	double Pini = 0.001;
	for(int l=0;l<sites.numSites;l++){
		for(g=0;g<numgroups;g++){
			groups[g].calcGammaTermsForP(Pini, l);
		}
	}

	double P;
	int l;
	for(int i=0; i<99; ++i){
		l = 0;
		P = start + i*step;
		LL = 0.0;
		for(g=0;g<numgroups;g++){
			LL += groups[g].calcGammaFuncForP(P,Pini,l);
			groups[g].rejectPup(l);
		}

		//print
		outLL << P << "\t" << LL << "\n";
		double sum = LL;
		for(l=1;l<sites.numSites;l++){
			for(g=0;g<numgroups;g++){
				sum += groups[g].calcGammaFuncForP(P,Pini,l);
				groups[g].rejectPup(l);
			}
		}
		LL_sum << P << "\t" << sum << "\n";
	}
	LL_sum.close();
	outLL.close();
	logfile->write(" done!");
}


void TWorld::calcLikelihoodSurface_p(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface ...");
	double start = 0.01;
	double step = 0.01;
	double LL = 0.0;
	std::ofstream outLL(filename.c_str());
	outLL<<"p\tLL"<<"\n";
	int g = params.getParameterDoubleWithDefault("group", 0);
	double p;
	for(int i=0; i<99; ++i){
		int l=0;
		p = start + i*step;
		LL = 0.0;

		LL += groups[g].calc_hastingsratio_p(P[l],l,p, log(p), log(1.0 - p));
		groups[g].rejectNewP_pops(l);

		//print
		outLL << p << "\t" << LL << "\n";
	}
	outLL.close();
	logfile->write(" done!");
}

void TWorld::calcLikelihoodSurfacelnK(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface for lnK ...");
	double limit = params.getParameterDoubleWithDefault("limit", -10.0);
	double start = 0.01;
	double step = 0.05;
	std::ofstream outLL(filename.c_str());
	outLL << "lnK\tLL"<<"\n";
	double lnK = 0.0;

	for(int i=0; i<(int)(-20*limit); ++i){
		lnK = limit + i*step +start;
		tmc->setKappa(lnK);

		//calc hastings and print
		outLL << lnK << "\t" << tmc->logHastingsKappa() << "\n";
		tmc->rejectKappa();
	}
	outLL.close();
	logfile->write(" done!");
}

void TWorld::calcLikelihoodSurfacelnkappa(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface for lnkappa ...");
	double limit = params.getParameterDoubleWithDefault("limit", -10.0);
	int g = params.getParameterIntWithDefault("group", 0);
	double start = 0.01;
	double step = 0.05;
	double LL = 0.0;
	std::ofstream outLL(filename.c_str());
	outLL << "lnkappa\tLL"<<"\n";
	double lnkappa = 0.0;

	for(int i=0; i<(int)(-20*limit); ++i){
		lnkappa = limit + i*step +start;
		//calc likelihood
		LL = 0.0;
		LL += groups[g].calculateLL_kappa(lnkappa);
//		LL += tmc->calcLLForK(lnkappa);
		//print
		outLL << lnkappa << "\t" << LL << "\n";
	}
	outLL.close();
	logfile->write(" done!");
}

void TWorld::calcLikelihoodSurfaceSworld(std::string filename){
	logfile->listFlush("Calculating LL-surface for Sworld ...");
	double LL = 0.0;
	std::ofstream outLL(filename.c_str());
	outLL << "S\talpha\tLL"<<"\n";

	for(int i=0; i<tmc->numStates; ++i){

		tmc->fixS(i);
		LL = 0.0;
		for(g=0; g<numgroups; g++){
			LL += groups[g].calcGammaFuncForAB(P, tmc);
		}
		outLL << i << " \t" << tmc->alpha[i] << "\t" << LL << std::endl;

		for(g=0; g<numgroups; g++)
			groups[g].reject_ThetasAndGammas();
	}
	outLL.close();
	logfile->write(" done!");
}


void TWorld::calcLikelihoodSurfaceSgroups(std::string filename){
	logfile->listFlush("Calculating LL-surface for Sgroups ...");
	double LL = 0.0;
	std::ofstream outLL(filename.c_str());
	outLL << "S\talpha\tLL"<<"\n";
	int g = 0;

	for(int i=0; i<tmc->numStates; ++i){
		groups[g].tmc->fixS(i);
		LL = 0.0;
		int l = 0;
		//calculating Hastings ratio
		for(unsigned int j=0;j<groups[0].numpop;j++){
			LL += groups[0].pops[j].logHastingsTermS(l, groups[0].p[l],groups[0].logP[l],groups[0].logPMinus1[l], groups[0].tmc);
		}
		//printing parameters and likelihood
		outLL << i << " \t" << tmc->alpha[i] << "\t" << LL << std::endl;
		//rejecting proposed value
		groups[g].reject_Sg(l);
	}
	outLL.close();
	logfile->write(" done!");
}


void TWorld::calcLikelihoodSurface_a(std::string filename, TParameters & params){
	logfile->listFlush("Calculating LL-surface for a of the beta distribution...");
	double limit = params.getParameterDoubleWithDefault("limit", 5.0);
	double step = 0.1;
	double LL = 0.0;

	std::ofstream outLL(filename.c_str());
	outLL << "a\tLL"<<"\n";
	calcGammaFor_betapar();

	for(int i=0; i<(int)(limit/step); ++i){
		log_a.oldValue = log_a.curValue;
		log_a.curValue = (-20.0 + i*step);
		switchGammaFor_betapar();
		calcGammaFor_betapar();
//		a.calcLogPriorRatio();

		//calc likelihood
		LL=0.0;
//		LL = a.logPriorRatio;
		bool hierarchy = false;

		if(numgroups>1) hierarchy = true;
		LL += logHastings_a(hierarchy);

		//print
		outLL << log_a.curValue << "\t" << LL << "\n";

		//reset
		log_a.RejectProposedValue();
		rejectGammaFor_betapar();
	}
	outLL.close();
	logfile->write(" done!");
}

//-------------------------------------------------------
// Read and set S and Sg and P
//-------------------------------------------------------
void TWorld::readAndSetS(TParameters & params, TMC *tmc){
	std::string S = params.getParameterString("S");
	if(S == "neutral"){
		logfile->list("Setting all S to neutral state.");
		tmc->setAllSNeutral();
	} else if(S == "max"){
		logfile->list("Setting all S to maximum state.");
		tmc->setAllSMax();
	} else if(S == "min"){
		logfile->list("Setting all S to minimum state.");
		tmc->setAllSMin();
	} else {
		//is an integer value
		if(stringContainsOnly(S, "-0123456789")){
			int Svalue = stringToInt(S) + s_max;
			if(Svalue < 0 || Svalue >= tmc->numStates)
				throw "Proposed value of S = " + toString(Svalue) + " is outside the accepted range [-s_max,s_max]!";
			logfile->list("Setting all S to " + toString(Svalue));
			tmc->fixS(Svalue);
		} else throw "Unknown S '" + S + "'! Only accept 'neutral' or 'max' or 'min' or an integer";
	}
}

void TWorld::readAndSetSg(TParameters & params){
	std::string Sg = params.getParameterString("Sg");
	if(Sg == "neutral"){
		logfile->list("Setting all Sg to neutral state.");
		for(g=0; g<numgroups; ++g)
			groups[g].setAllSNeutral();
	} else if(Sg == "max"){
		logfile->list("Setting all Sg to maximum state.");
		for(g=0; g<numgroups; ++g)
			groups[g].setAllSMax();
	} else if(Sg == "min"){
		logfile->list("Setting all Sg to minimum state.");
		for(g=0; g<numgroups; ++g)
			groups[g].setAllSMin();
	} else {
		//is an integer value
		if(stringContainsOnly(Sg, "-0123456789")){
			int Svalue = stringToInt(Sg) + s_max;
			if(Svalue < 0 || Svalue >= tmc->numStates)
				throw "Proposed value of Sg = " + toString(Svalue) + " is outside the accepted range [-s_max,s_max]!";
			logfile->list("Setting all Sg to " + toString(Svalue));
			for(g=0; g<numgroups; ++g){
				groups[g].tmc->fixS(Svalue);
			}
		} else throw "Unknown Sg '" + Sg + "'! Only accept 'neutral' or 'max' or 'min' or an integer";
	}
}

void TWorld::readAndSetP(TParameters & params){
	logfile->startIndent("Hierarchical parameters:");

	//P
	if(params.parameterExists("P")){
		double initialP = params.getParameterDouble("P");
		logfile->list("P = " + toString(initialP));
		for(int l=0; l<sites.numSites; l++)
			P[l] = initialP;
	} else {
		logfile->list("All P are chosen randomly within U[0,1]");
		for(int l=0; l<sites.numSites; l++){
			if(log_a.curValue == 0.0 && log_b.curValue == 0.0){
				P[l] = randomGenerator->getRand();
			}else{
				P[l] = randomGenerator->getBetaRandom(log_a.curExpVal,log_b.curExpVal);
			}
			if(P[l]>=0.99998 || P[l]<=0.000001)
				P[l] = P[l] *  0.99998 + 0.000001;
		}
	}
}

//-------------------------------------------------------
// Simulate
//-------------------------------------------------------
void TWorld::readAndSetParameters(TParameters & params){
	//Read parameters of world
	//------------------------
	logfile->startIndent("Hierarchical parameters:");
	//I have removed the P from here cause a repetition when I was calculating the LLsurface. But I'm checking them before calling the function readAndSetParameters
	double rangeProp_beta=0.0;
	double rangeProp_alphaMax=0.0;
	double rangeProp_kappa=0.0;
	double rangeProp_mu=0.0;
	double rangeProp_nu=0.0;
	initializeFromCommandLineArguments(params, rangeProp_beta, rangeProp_alphaMax, rangeProp_kappa, rangeProp_mu, rangeProp_nu);
	logfile->endIndent();

	logfile->startIndent("States of the HMM S and Sg:");
	if(numgroups>1){
		//S
		if(params.parameterExists("S")){
			readAndSetS(params, tmc);
		} else if(params.parameterExists("S_fromfile")){
			std::string file = params.getParameterString("S_fromfile");
			std::ifstream input_Sfile(file.c_str());
			std::string name;
			for(int l=0; l< sites.numSites; l++){
				input_Sfile >> name;
				input_Sfile >> tmc->S[l];
                tmc->S[l] += s_max;
			}
			input_Sfile.close();
		} else { //use Kappa to simulate
			logfile->listFlush("Simulating S in World ...");
			tmc->simulateS();
			logfile->write(" done!");
		}
		std::string S_simulated = "S_simulated.txt";
		std::ofstream Ssim(S_simulated.c_str());
		for(int l=0; l< sites.numSites; l++){
			Ssim<<"S["<<l<<"]\t"<<tmc->S[l] - s_max<<std::endl;
		}
		Ssim.close();
	}

	//Sg
	if(params.parameterExists("Sg")){
		readAndSetSg(params);
	} else if(params.parameterExists("Sg_fromfile")){
		std::string file = params.getParameterString("Sg_fromfile");
		std::ifstream input_Sgfile(file.c_str());
		std::string name;
		for(unsigned int g=0; g<numgroups; ++g){
			for(int l=0; l< sites.numSites; l++){
				input_Sgfile >> name;
				input_Sgfile >> groups[g].tmc->S[l];
                groups[g].tmc->S[l] += s_max;
			}
		}
		input_Sgfile.close();
	} else {
		logfile->listFlush("Simulating Sg in all groups ...");
		for(g=0; g<numgroups; ++g){
			groups[g].simulateS();
		}
		logfile->write(" done!");
		for(g=0; g<numgroups; g++){
			std::string S_simulated = "S" + toString(g) + "_simulated.txt";
			std::ofstream Ssim(S_simulated.c_str());
			for(int l=0; l< sites.numSites; l++){
				Ssim<<"S["<<l<<"]\t"<<groups[g].tmc->S[l] - s_max<<std::endl;
			}
			Ssim.close();
		}
	}
	logfile->endIndent();

	if(numgroups>1){
		//------------------------
		//Calculate the theta	--
		//------------------------
		for(g=0; g<numgroups; ++g){
			groups[g].initialise_calcTheta(P, tmc);
		}
	}else{
		groups[0].initialise_calcTheta_pops_from_groups();
	}
}

void TWorld::simulateData(TParameters & params){
	double error;
	error = params.getParameterDoubleWithDefault("error", 0.0);
	if(error<0.0 || error>1.0) throw "error has be to in the interval [0.0,1.0]";
	//Construct populations and sites: read from file or simulate?
	if(params.parameterExists("data")){
		dataFile = params.getParameterString("data");
		readSitesGroupsAndPopulations(dataFile, params);
	} else createSitesGroupsAndPopulations(params);

	logfile->startIndent("Reading parameters to conduct simulation:");
	if(numgroups>1){
		readAndSetP(params);
	}

	readAndSetParameters(params);
	logfile->endIndent();

	//Now simulate data in each group
	//---------------------------------
	logfile->listFlush("Simulating data ...");
	//p
	if(params.parameterExists("p")){
		double initialp = params.getParameterDouble("p");
		logfile->list("p = " + toString(initialp));
		for(g=0; g<numgroups; g++){
			for(int l=0; l<sites.numSites; l++){
				groups[g].p[l] = initialp;
			}
			groups[g].simulateData_inpops(groups[g].p, error);
		}
	}else{
		if(numgroups>1){
			for(g=0; g<numgroups; g++){
				groups[g].simulateData(P, tmc, error);
			}
		}else{
			groups[0].simulateData(log_a, log_b, error);
		}
	}
	logfile->write(" done!");

	//write the output file
	std::string outname = outNameTag + "_simulations.txt";
	writeData(outname);

	std::string freq_p = "freq_p.txt";
	std::ofstream freq_sim(freq_p.c_str());
	for(g=0; g<numgroups; g++){
		for(int l=0; l<sites.numSites; l++){
			freq_sim<<"p["<<g<<"]["<<l<<"]"<<"\t"<<groups[g].p[l]<<"\n";
		}
	}
	freq_sim.close();

	if(numgroups>1){
		std::string freq_P = "freq_ancP.txt";
		std::ofstream freq_simP(freq_P.c_str());
		for(int l=0; l<sites.numSites; l++){
			freq_simP<<P[l]<<"\n";
		}
		freq_simP.close();
	}

	groups[0].tmc->printStatDis();
}

void TWorld::calculate_Fst(TParameters & params){
	double Hs=0.0;
	double Hs_1site=0.0;
	double Ht=0.0;
	double Fst=0.0;
	double p=0.0;
	double q=0.0;
	double Ntot=0.0;
	double Npop_mean = 0;
	int numpoptot = 0;

	//initialize populations and sites
	dataFile = params.getParameterString("data");
	readSitesGroupsAndPopulations(dataFile, params);

	//Initialize from prior
	initializeFromPrior(params);

	//calculate Fst
	std::string print_fst = "Fst.txt";
	std::ofstream fst_est(print_fst.c_str());

	for(int l=0; l<groups[0].numSites;l++){
		p = 0.0;
		Hs_1site = 0.0;
		Ntot = 0.0;
		Npop_mean = 0.0;
		numpoptot = 0;
		for(g=0;g<numgroups;g++){
			for(unsigned int j=0; j<groups[g].numpop; j++){
				Npop_mean += groups[g].pops[j].N[l];
				numpoptot += groups[g].numpop;
			}
		}
		Npop_mean = Npop_mean/(double)numpoptot;

		for(g=0;g<numgroups;g++){
			p = p + groups[g].p[l] * Npop_mean;
			Hs_1site = Hs_1site + (1.0-pow(groups[g].p[l],2.0)-pow(1.0-groups[g].p[l],2.0)) * Npop_mean;
			Ntot = Ntot + Npop_mean;
		}
		p = p / Ntot;
		q = 1.0 - p;
		Ht = Ht + 2.0 * p * q;
		Hs = Hs + Hs_1site / Ntot;
	}
	Ht = Ht / (double) groups[0].numSites;
	Hs = Hs / (double) groups[0].numSites;
	Fst = (Ht - Hs) / Ht;

	fst_est << "Ht" << "\t" << "Hs" << "\t" << "Fst" << std::endl;
	fst_est << Ht << "\t" << Hs << "\t" << Fst <<std::endl;
	fst_est.close();
}

