#ifndef _GAMAMFUNCTION_H_
#define _GAMAMFUNCTION_H_

#include <cmath>
#include <vector>
#include "stringFunctions.h"
#include "TRandomGenerator.h"

inline double euler_maclaurin(const int N, const double c, const double logC, TRandomGenerator & randomGenerator){ //when we use inline, we have to put the function directly into the header

    //TODO: Likely true, likely false
	if(N < 6){
		if(N <= 0) return 0.0;
		if(N == 1) return logC;
		if(N == 2) return logC + log(1 + c);

		//else calculate
		double sum = c;
		for(int i=1; i<N; ++i) {
            sum *= i + c;
        }

		return log(sum);
	}

	//do simple approximation for small c
	if(c < 0.1) {
        double temp = logC;

        temp += randomGenerator.factorialLn(N - 1);

        return temp;
    }

	//else do Euler-McLaurin
	double NplusC = (double) N + c;
	return (NplusC-0.5)*log(NplusC) - (c-0.5)*logC - (double) N + (1.0/12.0)*(1.0/NplusC - 1.0/c);// - (1.0/720.0)*(2.0/(pow(N+c,3.0)) - 2.0/(pow(c,3.0))) ;
}

double gammln_2ed(double xx);
double gammln(double xx);


void splitParameterStringIntoVector(std::string & paramString, std::vector<std::string> & paramVec, std::string name);
void splitParameterStringIntoVectorHierarchical(std::string & paramString, std::vector<std::string> & paramVec, std::string name);
void repeatFirstEntry(std::vector<std::string> & vec, int num);

long double digammal(long double x);

#endif /* _GAMAMFUNCTION_H_ */
