#ifndef OBJECT_TGROUP_H_
#define OBJECT_TGROUP_H_

#include "Object_TPop.h"
#include "TPrior.h"
#include "stringFunctions.h"
#include "helperFunctions.h"
#include "TLog.h"

class TGroup{
private:

	double chisq_1DF[145] = {0.00000000, 0.01579077, 0.06418475, 0.14847186, 0.27499590, 0.45493642, 0.70832630, 1.07419417, 1.64237442, 2.70554345, 2.87437340, 3.06490172, 3.28302029, 3.53738460, 3.84145882, 4.21788459, 4.70929225, 5.41189443, 6.63489660, 6.82282684, 7.03347427, 7.27296897, 7.55030254, 7.87943858, 8.28381500, 8.80746839, 9.54953571, 10.82756617, 11.02275934, 11.24123284, 11.48924675, 11.77597742, 12.11566515, 12.53219331, 13.07039414, 13.83108362, 15.13670523, 15.33569084, 15.55829202, 15.81084854, 16.10265061, 16.44811021, 16.87138911, 17.41782129, 18.18929348, 19.51142096, 19.71272662, 19.93786740, 20.19323638, 20.48820029, 20.83728702, 21.26484729, 21.81655780, 22.59504266, 23.92812698, 24.13099475, 24.35785181, 24.61512926, 24.91224854, 25.26382073, 25.69433267, 26.24970916, 27.03311129, 28.37398736, 28.57797700, 28.80606984, 29.06472539, 29.36340609, 29.71678549, 30.14945296, 30.70752084, 31.49455797, 32.84125335, 33.04608695, 33.27511121, 33.53480767, 33.83467059, 34.18942211, 34.62373144, 35.18385750, 35.97368894, 37.32489311, 37.53038421, 37.76013523, 38.02064522, 38.32143347, 38.67726154, 39.11286240, 39.67461253, 40.46665791, 41.82145620, 42.02747372, 42.25780726, 42.51897017, 42.82050279, 43.17719775, 43.61384066, 44.17690440, 44.97074640, 46.32847599, 46.53491954, 46.76572428, 47.02744599, 47.32958438, 47.68698487, 48.12447512, 48.68860480, 49.48400609, 50.84417133, 51.05105142, 51.28207860, 51.54430183, 51.84705969, 52.20477304, 52.64310208, 53.20834131, 54.00528396, 55.36641351, 55.57328236, 55.80452511, 56.06665929, 56.37286446, 56.73137415, 57.17020486, 57.73603707, 58.53368325, 59.89764960, 60.10503365, 60.33686835, 60.59971739, 60.90317599, 61.26212102, 61.70147751, 62.26797585, 63.06654055, 64.43203897, 64.66411330, 64.92723197, 65.23099918, 65.59030559, 65.59030559, 66.03009909, 66.59715246, 67.39648382, 68.76325221};
	double chisqPval[181] = {1.0, 9e-01, 8e-01, 7e-01, 6e-01, 5e-01, 4e-01, 3e-01, 2e-01, 1e-01, 9e-02, 8e-02, 7e-02, 6e-02, 5e-02, 4e-02, 3e-02, 2e-02, 1e-02, 9e-03, 8e-03, 7e-03, 6e-03, 5e-03, 4e-03, 3e-03, 2e-03, 1e-03, 9e-04, 8e-04, 7e-04, 6e-04, 5e-04, 4e-04, 3e-04, 2e-04, 1e-04, 9e-05, 8e-05, 7e-05, 6e-05, 5e-05, 4e-05, 3e-05, 2e-05, 1e-05, 9e-06, 8e-06, 7e-06, 6e-06, 5e-06, 4e-06, 3e-06, 2e-06, 1e-06, 9e-07, 8e-07, 7e-07, 6e-07, 5e-07, 4e-07, 3e-07, 2e-07, 1e-07, 9e-08, 8e-08, 7e-08, 6e-08, 5e-08, 4e-08, 3e-08, 2e-08, 1e-08, 9e-09, 8e-09, 7e-09, 6e-09, 5e-09, 4e-09, 3e-09, 2e-09, 1e-09, 9e-10, 8e-10, 7e-10, 6e-10, 5e-10, 4e-10, 3e-10, 2e-10, 1e-10, 9e-11, 8e-11, 7e-11, 6e-11, 5e-11, 4e-11, 3e-11, 2e-11, 1e-11, 9e-12, 8e-12, 7e-12, 6e-12, 5e-12, 4e-12, 3e-12, 2e-12, 1e-12, 9e-13, 8e-13, 7e-13, 6e-13, 5e-13, 4e-13, 3e-13, 2e-13, 1e-13, 9e-14, 8e-14, 7e-14, 6e-14, 5e-14, 4e-14, 3e-14, 2e-14, 1e-14, 9e-15, 8e-15, 7e-15, 6e-15, 5e-15, 4e-15, 3e-15, 2e-15, 1e-15, 9e-16, 8e-16, 7e-16, 6e-16, 5e-16, 4e-16, 3e-16, 2e-16, 1e-16, 9e-17, 8e-17, 7e-17, 6e-17, 5e-17, 4e-17, 3e-17, 2e-17, 1e-17, 9e-18, 8e-18, 7e-18, 6e-18, 5e-18, 4e-18, 3e-18, 2e-18, 1e-18, 9e-19, 8e-19, 7e-19, 6e-19, 5e-19, 4e-19, 3e-19, 2e-19, 1e-19, 9e-20, 8e-20, 7e-20, 6e-20, 5e-20, 4e-20, 3e-20, 2e-20, 1e-20};

	bool storageInitialized;

	std::string name;
	TRandomGenerator* randomGenerator;

	double* Theta_old;
	double* pointerToDouble;

	//Gamma terms
	double gamma_theta_old;
	double gamma_theta;
	double* gamma_theta_perLocus;
	double* gamma_theta_perLocus_old;
	double gamma_thetaP_old;
	double gamma_thetaP;
	double* gamma_thetaP_perLocus;
	double* gamma_thetaP_perLocus_old;
	double gamma_thetaOneMinusP_old;
	double gamma_thetaOneMinusP;
	double* gamma_thetaOneMinusP_perLocus;
	double* gamma_thetaOneMinusP_perLocus_old;
	double ratioOfBinomials;
	double* ratioOfBinomials_perLocus;
	double* ratioOfBinomials_perLocus_old;
	double ratioOfBinomials_old;

	//for Forward-Backward
	double LL_forwardBackward;
	double** alphaForward; //per site and state
	double** betaBackward; //old and new per state
	bool forwardBackwardStorageInitialized;

public:
	int numSites;
	TPop* pops;
	unsigned int numpop;
	TMC* tmc;
	double* p;
	double* logP;
	double* logPMinus1;

	TNormalPrior B;
	double* Theta;
	bool estimateSg;
	double* mean_p;
	bool posterior_p;

	TGroup();
	~TGroup(){
		if(storageInitialized){
			delete[] p;
			delete[] logP;
			delete[] logPMinus1;

			delete[] Theta;
			delete[] Theta_old;
			delete[] gamma_theta_perLocus;
			delete[] gamma_theta_perLocus_old;
			delete[] gamma_thetaP_perLocus;
			delete[] gamma_thetaP_perLocus_old;
			delete[] gamma_thetaOneMinusP_perLocus;
			delete[] gamma_thetaOneMinusP_perLocus_old;
			delete[] ratioOfBinomials_perLocus;
			delete[] ratioOfBinomials_perLocus_old;
			delete[] pops;
			delete tmc;
			delete[] mean_p;
		}
		clearForwardBackwardStorage();
	};


	void initialize(std::string Name, std::vector<std::string> populations, int S_max, TSites* Sites, TRandomGenerator* RandomGenerator);
	void initializeFromPrior(std::string & BString, std::string & betaString, std::string alphaString, std::string kappa, std::string Mu, std::string Nu, double & rangeProp_beta, double & rangeProp_alphaMax, double & rangeProp_kappa, double & rangeProp_mu,  double & rangeProp_nu);
	void initializeFromPrior(std::string & betaString, std::string alphaString, std::string kappa, std::string Mu, std::string Nu, double & rangeProp_beta, double & rangeProp_alphaMax, double & rangeProp_kappa, double & rangeProp_mu,  double & rangeProp_nu);
	void calcInitialGammaTerms(double* P, TMC* tmcWorld);
	void calcInitialGammaTerms(double* P, TMC* tmcWorld, TMC* tmcgroups);
	void calcInitialGammaTerms();
	void registerPriors(TPriorVector* priorVec);
	void estimateFrequenciesFromData();
	double getP(int & locus){ return p[locus]; };

	void addData(int & pop, int & locus, int & _n, int & _N){
		pops[pop].addData(locus, _n, _N);
	};
	void writeData(std::ostream & out, int & locus){
		for(unsigned int j=0;j<numpop;j++){
			out << "\t";
			pops[j].writeData(out, locus);
		}
	};
	void writeGroupHeader(std::ostream & out){
		for(unsigned int j=0;j<numpop;j++){
			out << "\t" << name;
		}
	};
	void writePopHeader(std::ostream & out){
		for(unsigned int j=0;j<numpop;j++){
			out << "\t" << pops[j].name;
		}
	};

	void calcThetaAndGammaTerms(double* P, TMC* tmcWorld);
	void calcThetaAndGammaTermsLocus(double* P, TMC* tmcWorld, const int & l);
	double calcHastingsRatioLocus(double* P, TMC* tmcWorld, const int & l);
	void StoreOldTerms(const int & l);
	void changeGammas(const int & l);
	void addPopulation(std::string & name);
	double calcGammaFuncForAB(double* P, TMC* tmcWorld);
	void reject_ThetasAndGammas();
	void initialise_calcTheta(double* P, TMC* tmcWorld);
	void initialise_calcTheta_pops_from_groups();
	void InitializeSg(std::string file);

	void set_B(double & _B);
	void findInitialValuesOfBeta(const int & numSwitches, TLog* logfile);
	void findInitialValuesOfB(const int & numSwitches, TLog* logfile, const double* P);
	double calcDerivativeOfTheta(const double & this_A, const double* P);
	double calcDerivativeOfTheta(const double & this_alpha, const int & lcous);
	double calcDerivativeOfTheta_Alpha(const double & this_alpha, const int & locus);
	void findInitialValuesOfSAndKappa(const int & numSwitches, TLog* logfile, bool estimateSg, double sigma2_prior);
	double calculateHastingsRatio_B(double* P, TMC* tmcWorld);
	double calculateLL_beta(int pop);
	void reject_beta();
	void update_B(double* P, TMC* tmcWorld,std::ostream & hbeta);
	void update_B(double* P, TMC* tmcWorld,std::ostream & hbeta, TMC* tmcgroups);
	void switchTheta();
	void switchGammas();
	void setAlphaMax(double & alpha_max);
	double calculateHastingsRatio_alphaMax();
	void rejectAlphamax();
	void update_alphamax();
	void update_kappa();
	void update_mu();
	void update_nu();
	void findInitialValues(TPrior* param, const int & numSwitches, TLog* logfile);
	double calcSumlnLhdQ();
	double calculateLL_kappa(double lnkappa);
	void LikelihoodRatioTest(double & this_alpha, const int & locus);
	void storeOldGammaTermsForP(const int & l);
	void storeOldGammaTerms(const int & l);
	void calcGammaTermsForP(const double & P, const int & l);
	void calcGammaTerms(const double & P, const int & l);
	double calc_hastingsratio_p(const double P, const int l, const double p, const double logP, const double logPMinus1);
	double calc_hastingsratio_p(const int l, const double p, const double logP, const double logPMinus1);
	double calc_freqratio_forp(double P, int l, double new_p);
	void rejectNewP_pops(int l);
	void update_p(double* P);
	void update_p(TNormalPriorWithExpVal & log_a, TNormalPriorWithExpVal & log_b);
	double calcGammaFuncForP(const double & P, const double & P_old, const int & l);
	void rejectP(const int & l);
	void rejectPup(const int & l);
	void acceptP(const double & P, const int & l);
	void acceptS(const double & P, const int & l);
	double calcGammaFuncOneSite(const double & P, TMC* tmcWorld, const int & l);
	void acceptGammaOneSite(const double & P, const int & l);
	void rejectGammaOneSite(const int & l);
	void calcRatioOfBinomials(const double & P, const int & l);

	void update_S();
	void reject_Sg(int l);

	void simulateS();
	void setAllSNeutral();
	void setAllSMax();
	void setAllSMin();
	void set_beta(double* betaVec);
	void set_beta(double beta);
	void simulateData(double* P, TMC* worldTmc, double error);
	void simulateData(TNormalPriorWithExpVal & log_a, TNormalPriorWithExpVal & log_b, double error);
	void simulateData_inpops(double* p, double error);
	void update_allStogetherGroup(double & log_h, double & prop_LL, double & old_LL, bool & accepted, int & _min, int & _max, int & cost);

	//forward backward
	void estimateSUsingForwardBackward();
	void initializeForwardBackwardStorage();
	void clearForwardBackwardStorage();
	void runForward();
	void runBackwardAndEstimateS();
	void fillEmissionProbabilities(double* em, const int & locus);
	double getLogEmissionProbability(const int & locus, const int & state);

	//calc tot LL
	double give_totLLoneGroup();
	double give_totLLoneGroup(TMC* tmcgroups);
	double give_LLfromEM(TMC* tmcgroups);
};

#endif /* OBJECT_TGROUP_H_ */
