#include "Object_TGroup.h"
#include "mathFunctions.h"

TGroup::TGroup(){
	std::string name;
	numSites = 0;
	randomGenerator = NULL;
	numpop = 0;
	pops = NULL;
	tmc = NULL;
	gamma_theta = 0.0;
	gamma_thetaP = 0.0;
	gamma_thetaOneMinusP = 0.0;
	ratioOfBinomials = 0.0;
	gamma_theta_old = 0.0;
	gamma_thetaP_old = 0.0;
	gamma_thetaOneMinusP_old = 0.0;
	ratioOfBinomials_old = 0.0;
	storageInitialized = false;
	estimateSg = true;

	//arrays
	p = NULL;
	Theta = NULL;
	Theta_old = NULL;
	pointerToDouble = NULL;
	gamma_theta_perLocus = NULL;
	gamma_theta_perLocus_old = NULL;
	gamma_thetaP_perLocus = NULL;
	gamma_thetaP_perLocus_old = NULL;
	gamma_thetaOneMinusP_perLocus = NULL;
	gamma_thetaOneMinusP_perLocus_old = NULL;
	ratioOfBinomials_perLocus = NULL;
	ratioOfBinomials_perLocus_old = NULL;
	mean_p = NULL;

	//forward backward
	LL_forwardBackward = 0.0;
	alphaForward = NULL;
	betaBackward = NULL;
	forwardBackwardStorageInitialized = false;
}

void TGroup::initialize(std::string Name, std::vector<std::string> populations, int S_max, TSites* Sites, TRandomGenerator* RandomGenerator){
	name = Name;
	randomGenerator = RandomGenerator;

	tmc = new TMC(S_max, Sites, randomGenerator);
	numSites = Sites->numSites;

	//create arrays
	p = new double[numSites];
	logP = new double[numSites];
	logPMinus1 = new double[numSites];

	Theta = new double[numSites];
	Theta_old = new double[numSites];
	gamma_theta_perLocus = new double[numSites];
	gamma_theta_perLocus_old = new double[numSites];
	gamma_thetaP_perLocus = new double[numSites];
	gamma_thetaP_perLocus_old = new double[numSites];
	gamma_thetaOneMinusP_perLocus = new double[numSites];
	gamma_thetaOneMinusP_perLocus_old = new double[numSites];
	ratioOfBinomials_perLocus = new double[numSites];
	ratioOfBinomials_perLocus_old = new double[numSites];

	storageInitialized = true;

	//now initialize populations
	numpop = populations.size();
	pops = new TPop[numpop];

	//initialize them
	for(unsigned int j=0;j<numpop;j++){
		pops[j].initializeStorage(populations[j], numSites, randomGenerator);
	}

	mean_p = new double[numSites];
	posterior_p = false;
}

void TGroup::initializeFromPrior(std::string & BString, std::string & betaString, std::string alphaString, std::string kappaString, std::string MuString, std::string Nu_String, double & rangeProp_beta, double & rangeProp_alphaMax, double & rangeProp_kappa, double & rangeProp_mu,  double & rangeProp_nu){
	B.initialize(name + "_B", BString, -15.0, 15.0, rangeProp_beta, randomGenerator);

	//initialize MarkovChain
	tmc->initializeFromPrior(name + "_alpha_max", alphaString, name + "_lnkappa", kappaString, name + "_lnMu", MuString, name + "_lnNu", Nu_String, rangeProp_alphaMax, rangeProp_kappa, rangeProp_mu, rangeProp_nu);

	//initialize populations
	std::vector<std::string> betaVec;
	splitParameterStringIntoVector(betaString, betaVec, "beta (" + name + ")");
	if(betaVec.size() == 1) repeatFirstEntry(betaVec, numpop);
	else if (betaVec.size() != numpop) throw "The number of beta values provided for group " + name + " does not match the number of populations!";

	for(unsigned int j=0;j<numpop;j++){
		pops[j].initializeFromPrior(betaVec[j], rangeProp_beta);
	}
}

void TGroup::initializeFromPrior(std::string & betaString, std::string alphaString, std::string kappaString, std::string MuString, std::string NuString, double & rangeProp_beta, double & rangeProp_alphaMax, double & rangeProp_kappa, double & rangeProp_mu,  double & rangeProp_nu){
	//initialize MarkovChain
	tmc->initializeFromPrior(name + "_alpha_max", alphaString, name + "_lnkappa", kappaString, name + "_lnMu", MuString, name + "_lnNu", NuString, rangeProp_alphaMax, rangeProp_kappa, rangeProp_mu, rangeProp_nu);

	//initialize populations
	std::vector<std::string> betaVec;
	splitParameterStringIntoVector(betaString, betaVec, "beta (" + name + ")");
	if(betaVec.size() == 1) repeatFirstEntry(betaVec, numpop);
	else if (betaVec.size() != numpop) throw "The number of beta values provided for group " + name + " does not match the number of populations!";

	for(unsigned int j=0;j<numpop;j++){
		pops[j].initializeFromPrior(betaVec[j], rangeProp_beta);
	}
}

void TGroup::calcInitialGammaTerms(double* P, TMC* tmcWorld){
	calcThetaAndGammaTerms(P, tmcWorld);
	for(unsigned int j=0;j<numpop;j++){
		pops[j].fillThetaAndEulerMacLaurinTerms(p, logP, logPMinus1, tmc);
	}
}

void TGroup::calcInitialGammaTerms(double* P, TMC* tmcWorld, TMC* tmcgroups){
	calcThetaAndGammaTerms(P, tmcWorld);
	for(unsigned int j=0;j<numpop;j++){
		pops[j].fillThetaAndEulerMacLaurinTerms(p, logP, logPMinus1, tmcgroups);
	}
}

void TGroup::calcInitialGammaTerms(){
	for(unsigned int j=0;j<numpop;j++){
		pops[j].fillThetaAndEulerMacLaurinTerms(p, logP, logPMinus1, tmc);
	}
}

void TGroup::registerPriors(TPriorVector* priorVec){
	priorVec->addPrior(&B);
	tmc->registerPriors(priorVec);
	for(unsigned int j=0;j<numpop;j++){
		pops[j].registerPriors(priorVec);
	}
}

void TGroup::estimateFrequenciesFromData(){
	//estimate p from data
	for(int l=0; l<numSites; l++){
		p[l] = 0.0;

		int numPosWithData = 0;
		for(unsigned int j=0;j<numpop;j++){
			//p[l] += pops[j].estimateP(l);
			pops[j].addToCrudePEstimate(l, p[l], numPosWithData);
		}
		if(numPosWithData > 0)
			p[l] = p[l] / numpop;
		else
			p[l] = 0.5;

		//make sure p is between 0 and 1
		if(p[l] < 0.001) p[l] = 0.001;
		else if(p[l] > 0.999) p[l] = 0.999;

		logP[l] = log(p[l]);
		logPMinus1[l] = log(1. - p[l]);
	}
}

void TGroup::initialise_calcTheta(double* P, TMC* tmcWorld){
	tmcWorld->calcTheta(Theta, B.curValue);
	initialise_calcTheta_pops_from_groups();
}

void TGroup::initialise_calcTheta_pops_from_groups(){
	for(unsigned int j=0;j<numpop;j++){
		pops[j].initialise_calcTheta_pops(tmc);
	}
}


void TGroup::InitializeSg(std::string file){
	std::ifstream input_Sgfile(file.c_str());
	if(!input_Sgfile) throw "Missing file to initialize S for the group";
	std::string name;
	for(int l=0; l< numSites; l++){
		input_Sgfile >> name;
		input_Sgfile >> tmc->S[l];
        tmc->S[l] += tmc->s_max;
	}
	input_Sgfile.close();
}


//----------------------------------------------
//Update A and alpha
//----------------------------------------------
void TGroup::calcThetaAndGammaTerms(double* P, TMC* tmcWorld){
	tmcWorld->calcTheta(Theta, B.curValue);

	gamma_theta = 0.0;
	gamma_thetaP = 0.0;
	gamma_thetaOneMinusP = 0.0;
	ratioOfBinomials = 0.0;

	for(int l=0; l<numSites; l++){
		gamma_theta_perLocus[l] = gammln(Theta[l]);
		gamma_theta += gamma_theta_perLocus[l];
		gamma_thetaP_perLocus[l] = gammln(Theta[l]*P[l]); //the argument cannot be 0
		gamma_thetaP += gamma_thetaP_perLocus[l];
		gamma_thetaOneMinusP_perLocus[l] = gammln(Theta[l]*(1.0-P[l]));
		gamma_thetaOneMinusP += gamma_thetaOneMinusP_perLocus[l];

		ratioOfBinomials_perLocus[l] = Theta[l] * (P[l]*log(p[l]) + (1.0-P[l])*log(1.0-p[l]));
		ratioOfBinomials += ratioOfBinomials_perLocus[l];
	}
}

void TGroup::StoreOldTerms(const int & l){
	//store old terms
	Theta_old[l] = Theta[l];
	gamma_theta_perLocus_old[l] = gamma_theta_perLocus[l];
	gamma_thetaP_perLocus_old[l] = gamma_thetaP_perLocus[l];
	gamma_thetaOneMinusP_perLocus_old[l] = gamma_thetaOneMinusP_perLocus[l] ;
	ratioOfBinomials_perLocus_old[l] = ratioOfBinomials_perLocus[l];
}

void TGroup::calcThetaAndGammaTermsLocus(double* P, TMC* tmcWorld, const int & l){
	tmcWorld->calcThetaLocus(Theta, B.curValue, l);
	gamma_theta_perLocus[l] = gammln(Theta[l]);
	gamma_thetaP_perLocus[l] = gammln(Theta[l]*P[l]);
	gamma_thetaOneMinusP_perLocus[l] = gammln(Theta[l]*(1.0-P[l]));
	ratioOfBinomials_perLocus[l] = Theta[l] * (P[l]*log(p[l]) + (1.0-P[l])*log(1.0-p[l]));
}

void TGroup::changeGammas(const int & l){
	gamma_theta += gamma_theta_perLocus[l] - gamma_theta_perLocus_old[l];
	gamma_thetaP += gamma_thetaP_perLocus[l] - gamma_thetaP_perLocus_old[l];
	gamma_thetaOneMinusP += gamma_thetaOneMinusP_perLocus[l] - gamma_thetaOneMinusP_perLocus_old[l];
	ratioOfBinomials += ratioOfBinomials_perLocus[l] - ratioOfBinomials_perLocus_old[l];
}

double TGroup::calcHastingsRatioLocus(double* P, TMC* tmcWorld, const int & l){
	StoreOldTerms(l);
	calcThetaAndGammaTermsLocus(P, tmcWorld, l);

	return gamma_theta_perLocus[l] - gamma_thetaP_perLocus[l] -gamma_thetaOneMinusP_perLocus[l] + ratioOfBinomials_perLocus[l] - gamma_theta_perLocus_old[l] + gamma_thetaP_perLocus_old[l] + gamma_thetaOneMinusP_perLocus_old[l] - ratioOfBinomials_perLocus_old[l];
}




double TGroup::calcGammaFuncForAB(double* P, TMC* tmcWorld){
	switchTheta();
	switchGammas();
	gamma_theta_old = gamma_theta;
	gamma_thetaP_old = gamma_thetaP;
	gamma_thetaOneMinusP_old = gamma_thetaOneMinusP;
	ratioOfBinomials_old = ratioOfBinomials;

	calcThetaAndGammaTerms(P, tmcWorld);
	double log_h = 0.0;
	log_h += gamma_theta - gamma_thetaP - gamma_thetaOneMinusP - gamma_theta_old + gamma_thetaP_old + gamma_thetaOneMinusP_old + ratioOfBinomials - ratioOfBinomials_old;
	return log_h;
}


void TGroup::reject_ThetasAndGammas(){
	switchTheta();
	switchGammas();
	gamma_theta = gamma_theta_old;
	gamma_thetaP = gamma_thetaP_old;
	gamma_thetaOneMinusP = gamma_thetaOneMinusP_old;
	ratioOfBinomials = ratioOfBinomials_old;
}

void TGroup::setAlphaMax(double & alpha_max){
	tmc->setAlphaMax(alpha_max);
}

double TGroup::calculateHastingsRatio_alphaMax(){
	double log_h = 0.0;

	for(unsigned int j=0;j<numpop;j++){
		log_h += pops[j].calcThetaAndEulerMacLaurinTerms(p, logP, logPMinus1, tmc);
	}

	return log_h;
}

void TGroup::rejectAlphamax(){
	for(unsigned int j=0; j<numpop; j++){
		pops[j].rejectThetaAndEulerMacLaurinTerms();
	}
}

void TGroup::update_alphamax(){
	if(tmc->alphaMax.isEstimated){
		//propose new value of alpha_max and get prior density
		double log_h = tmc->proposeNewAlphaMax();

		//calc hastings
		log_h += calculateHastingsRatio_alphaMax();

		//accept or reject
		if(!tmc->acceptOrRejectAlphamax(log_h)){
			//reject
			rejectAlphamax();
		}
	}
}


//----------------------------------------------
//Update B and beta
//----------------------------------------------
void TGroup::set_B(double & _B){
	B.setNewValue(_B);
}

void TGroup::set_beta(double* betaVec){
	for(unsigned int j=0; j<numpop; ++j){
		pops[j].set_beta(betaVec[j]);
	}
}

void TGroup::set_beta(double beta){
	for(unsigned int j=0; j<numpop; ++j){
		pops[j].set_beta(beta);
	}
}

void TGroup::findInitialValuesOfBeta(const int & numSwitches, TLog* logfile){
	logfile->startIndent("Group " + name + ":");
	for(unsigned int j=0;j<numpop;j++){
		pops[j].findInitialValueOfBeta(numSwitches, p, logfile);
	}
	logfile->endIndent();
}

void TGroup::findInitialValuesOfB(const int & numSwitches, TLog* logfile, const double* P){
	logfile->startIndent("Group " + name + ":");
	//initialize
	int s = 0;
	double log_theta = 3.0;
	double this_theta = exp(log_theta);
	double oldDerivative = calcDerivativeOfTheta(this_theta, P);
	double step;
	if(oldDerivative < 0.0)
		step = -0.5;
	else if(oldDerivative > 0.0)
		step = 0.5;
	else s = numSwitches;
	double derivative;
	double sign;

	//now run algorithm
	while(s < numSwitches){
		for(int i=0; i<101; ++i){
			log_theta += step;
			this_theta = exp(log_theta);
			derivative = calcDerivativeOfTheta(this_theta, P);

			//std::cout << "Theta = " << this_theta << ", deriv = " << derivative << std::endl;

			sign = derivative / oldDerivative;
			oldDerivative = derivative;
			if(sign < 0.0){
				step = -step/2.71828;
				++s;
				//std::cout << "NEW STEP = " << step << std::endl;
				break;
			}

			//checks
			if(i == 100)
				throw "Issue initializing B values for population " + name + "!";
		}
	}

	double new_B = -log(this_theta);
	if(new_B<-10.0) new_B=-9.0;
	set_B(new_B);
	logfile->list(name + ": initial B = " + toString(B.curValue));
	logfile->endIndent();
}

double TGroup::calcDerivativeOfTheta(const double & this_theta, const double* P){
	double sum = 0.0;

	for(int i = 0; i < numSites; ++i){
		sum += digammal(this_theta) + P[i]*(log(p[i])-digammal(this_theta*P[i])) + (1.0-P[i])*(log(1.0-p[i])-digammal(this_theta*(1.0-P[i])));
	}

	return sum;
}


double TGroup::calcDerivativeOfTheta(const double & this_alpha, const int & locus){
	double this_theta;
	double sum = 0.0;
	for(unsigned int j=0;j<numpop;j++){
		this_theta = exp(-this_alpha - pops[j].beta.curValue);
		sum += pops[j].calcDerivativeOfThetaOneLocus(this_theta, locus, p[locus]);
	}
	return sum;
}

double TGroup::calcDerivativeOfTheta_Alpha(const double & this_alpha, const int & locus){
	double this_theta;
	double sum = 0.0;
	for(unsigned int j=0;j<numpop;j++){
		this_theta = exp(-this_alpha - pops[j].beta.curValue);
		sum += this_theta * pops[j].calcDerivativeOfThetaOneLocus(this_theta, locus, p[locus]);
	}
	return sum;
}

void TGroup::LikelihoodRatioTest(double & this_alpha, const int & locus){
	double PValue = 0.0;
	double D = 0.0;
	double this_theta;
	// calc alternative model
	for(unsigned int j=0;j<numpop;j++){
		this_theta = exp(-this_alpha - pops[j].beta.curValue);
		D += pops[j].calcLLofThetaOneLocus(this_theta, locus, p[locus]);
	}

	// calc H0 (alpha = 0.0)
	for(unsigned int j=0;j<numpop;j++){
		this_theta = exp(-pops[j].beta.curValue);
		D -= pops[j].calcLLofThetaOneLocus(this_theta, locus, p[locus]);
	}
	D = 2.0 * D;
	//calc p value
	double* thisCisqDist;
	thisCisqDist = chisq_1DF;

	if(D > thisCisqDist[144]){
		PValue = 1e-20;
	} else {
		int x = 0;
		while(thisCisqDist[x] < D) ++x;
		PValue = chisqPval[x-1] + (D - thisCisqDist[x-1])/(thisCisqDist[x]-thisCisqDist[x-1])*(chisqPval[x]-chisqPval[x-1]);
	}
	//small values of the ratio support the new model
	if(PValue >= 0.05){
		this_alpha = 0.0;
	}
}

void TGroup::findInitialValuesOfSAndKappa(const int & numSwitches, TLog* logfile, bool estimateSg, double sigma2_prior){
	//first estimate S
	estimateSUsingForwardBackward();

	//and estimate lnkappa
	if(tmc->lnkappa.isEstimated){
		tmc->estimateKappaFromS();
		if(tmc->lnkappa.curValue < tmc->lnkappa.min){
			tmc->setKappa(tmc->lnkappa.min+1.0);
		}else if(tmc->lnkappa.curValue > tmc->lnkappa.max){
			tmc->setKappa(tmc->lnkappa.max-1.0);
		}
		logfile->conclude("Estimated initial lnkappa = " + toString(tmc->lnkappa.curValue));
	} else {
		logfile->conclude("Fixed lnkappa = " + toString(tmc->lnkappa.curValue));
	}
}


double TGroup::calculateHastingsRatio_B(double* P, TMC* tmcWorld){
	return B.logPriorRatio + calcGammaFuncForAB(P, tmcWorld);
}

double TGroup::calculateLL_beta(int pop){
	double log_h = 0.0;
	log_h += pops[pop].calculateHastingsRatio_beta(p, logP, logPMinus1, tmc);
	return log_h;
}

void TGroup::reject_beta(){
	for(unsigned int j=0;j<numpop;j++){
		pops[j].reject_beta();
	}
}

void TGroup::update_B(double* P, TMC* tmcWorld,std::ostream & hbeta){
	if(B.isEstimated){
		//propose new B
		B.proposeNewValue();

		//calculate hastings
		double log_h = calculateHastingsRatio_B(P, tmcWorld);

		//accept or reject
		if(!B.acceptOrReject(log_h))
			reject_ThetasAndGammas();
	}

	//update beta in each population
	for(unsigned int j=0;j<numpop;j++){
		pops[j].update_beta(p, logP, logPMinus1, tmc, hbeta);
	}
}

void TGroup::update_B(double* P, TMC* tmcWorld,std::ostream & hbeta, TMC* tmcgroups){
	if(B.isEstimated){
		//propose new B
		B.proposeNewValue();

		//calculate hastings
		double log_h = calculateHastingsRatio_B(P, tmcWorld);

		//accept or reject
		if(!B.acceptOrReject(log_h))
			reject_ThetasAndGammas();
	}

	//update beta in each population
	for(unsigned int j=0;j<numpop;j++){
		pops[j].update_beta(p, logP, logPMinus1, tmcgroups, hbeta);
	}
}

inline void TGroup::switchTheta(){
	pointerToDouble = Theta_old;
	Theta_old = Theta;
	Theta = pointerToDouble;
}

inline void TGroup::switchGammas(){
	pointerToDouble = gamma_theta_perLocus;
	gamma_theta_perLocus = gamma_theta_perLocus_old;
	gamma_theta_perLocus_old = pointerToDouble;

	pointerToDouble = gamma_thetaP_perLocus;
	gamma_thetaP_perLocus = gamma_thetaP_perLocus_old;
	gamma_thetaP_perLocus_old = pointerToDouble;

	pointerToDouble = gamma_thetaOneMinusP_perLocus;
	gamma_thetaOneMinusP_perLocus = gamma_thetaOneMinusP_perLocus_old;
	gamma_thetaOneMinusP_perLocus_old = pointerToDouble;

	pointerToDouble = ratioOfBinomials_perLocus;
	ratioOfBinomials_perLocus = ratioOfBinomials_perLocus_old;
	ratioOfBinomials_perLocus_old = pointerToDouble;
}

//---------------------------------------------------//
//                    Update lnkappa					 //
//---------------------------------------------------//

void TGroup::update_kappa(){
	tmc->updateKappa();
}

double TGroup::calculateLL_kappa(double lnkappa){
	tmc->setKappa(lnkappa);
	double LL = tmc->logHastingsKappa();
	tmc->lnkappa.RejectProposedValue();
	tmc->switchQ();
	return LL;
}

//---------------------------------------------------//
//         Update mu, nu_up annd nu_down			 //
//---------------------------------------------------//

void TGroup::update_mu(){
	tmc->update_mu();
}

void TGroup::update_nu(){
	tmc->update_nu();
}

void TGroup::findInitialValues(TPrior* param, const int & numSwitches, TLog* logfile){
	//start
	double min = -10.0;
	double max = 0.0;
	if(param->hasMinMax){
		min = param->min;
		max = param->max;
	}
	int i;
	int s = 0;
	double step = 0.2;
	double new_llQ = 0.0;
	double old_llQ = calcSumlnLhdQ();
	//now loop
	while(s < numSwitches){
		for(i=0; i<101; ++i){
			param->oldValue = param->curValue;
			param->curValue += step;
			if(param->curValue > max || param->curValue < min){
				param->curValue = param->oldValue;
				step = -step/2.71828;
				++s;
				break;
			}
			new_llQ = calcSumlnLhdQ();
			if(old_llQ > new_llQ){
				old_llQ = new_llQ;
				step = -step/2.71828;
				++s;
				break;
			}
			old_llQ = new_llQ;
		}
	}
	logfile->conclude("Estimated initial " + param->name + " = " + toString(param->curValue));
}

double TGroup::calcSumlnLhdQ(){
	double sum = 0.0;
	sum += tmc->upsumlnLhdQ();
	return sum;
}

//---------------------------------------------------//
//                     Update P	and p				 //
//---------------------------------------------------//

void TGroup::storeOldGammaTerms(const int & l){
	storeOldGammaTermsForP(l);
	ratioOfBinomials_perLocus_old[l] = ratioOfBinomials_perLocus[l];
}

void TGroup::storeOldGammaTermsForP(const int & l){
	gamma_thetaP_perLocus_old[l] = gamma_thetaP_perLocus[l];
	gamma_thetaOneMinusP_perLocus_old[l] = gamma_thetaOneMinusP_perLocus[l];
}

void TGroup::calcGammaTerms(const double & P, const int & l){
	calcGammaTermsForP(P, l);
	calcRatioOfBinomials(P, l);
}

void TGroup::calcGammaTermsForP(const double & P, const int & l){
	gamma_thetaP_perLocus[l] = gammln(Theta[l]*P);
	gamma_thetaOneMinusP_perLocus[l] = gammln(Theta[l]*(1.0-P));
}

void TGroup::calcRatioOfBinomials(const double & P, const int & l){
	ratioOfBinomials_perLocus[l] = Theta[l]*P* log(p[l]) + Theta[l]*(1.0-P)*log(1.0-p[l]);
}

double TGroup::calcGammaFuncForP(const double & P, const double & P_old, const int & l){
	storeOldGammaTermsForP(l);
	calcGammaTermsForP(P, l);

	return gamma_thetaP_perLocus_old[l] - gamma_thetaP_perLocus[l] + gamma_thetaOneMinusP_perLocus_old[l] - gamma_thetaOneMinusP_perLocus[l] + Theta[l]*(P-P_old)*log(p[l]/(1.0-p[l]));
}


void TGroup::rejectP(const int & l){
	gamma_thetaP_perLocus[l] = gamma_thetaP_perLocus_old[l];
	gamma_thetaOneMinusP_perLocus[l] = gamma_thetaOneMinusP_perLocus_old[l];
	ratioOfBinomials_perLocus[l] = ratioOfBinomials_perLocus_old[l];
}

void TGroup::rejectPup(const int & l){
	gamma_thetaP_perLocus[l] = gamma_thetaP_perLocus_old[l];
	gamma_thetaOneMinusP_perLocus[l] = gamma_thetaOneMinusP_perLocus_old[l];
}


void TGroup::acceptP(const double & P, const int & l){
	gamma_thetaP += gamma_thetaP_perLocus[l] - gamma_thetaP_perLocus_old[l];
	gamma_thetaOneMinusP += gamma_thetaOneMinusP_perLocus[l] - gamma_thetaOneMinusP_perLocus_old[l];

	//now ratio of bionmials: need to be calculated with new P!
	ratioOfBinomials_perLocus_old[l] = ratioOfBinomials_perLocus[l];
	calcRatioOfBinomials(P, l);
	ratioOfBinomials += ratioOfBinomials_perLocus[l] - ratioOfBinomials_perLocus_old[l];
}


double TGroup::calc_hastingsratio_p(const double P, const int l, const double p, const double logP, const double logPMinus1){
	double LL=0.0;
	LL += calc_freqratio_forp(P, l, p);

	for(unsigned int j=0;j<numpop;j++){
		LL+=pops[j].logHastingsTermP(l,p, logP, logPMinus1);
	}
	return LL;
}

double TGroup::calc_hastingsratio_p(const int l, const double p, const double logP, const double logPMinus1){
	double LL=0.0;

	for(unsigned int j=0;j<numpop;j++){
		LL+=pops[j].logHastingsTermP(l,p, logP, logPMinus1);
	}
	return LL;
}

void TGroup::rejectNewP_pops(int l){
	for(unsigned int j=0;j<numpop;j++) pops[j].rejectNewP(l);
}

double TGroup::calc_freqratio_forp(double P, int l, double new_p){
	return (Theta[l]*P-1.0)*log(new_p/p[l]) + (Theta[l]*(1.0-P)-1.0)*log( (1.0-new_p)/(1.0-p[l]));
}

void TGroup::update_p(double* P){
	double new_p;
	double log_h;
	for(int l=0;l<numSites;l++){
		new_p = randomGenerator->getNormalRandom(p[l], 0.01);
		if(new_p < 0.0){
			new_p = -new_p;
		} else if(new_p > 1.0){
			new_p = 2.0 - new_p;
		}
		double new_logP = log(new_p);
		double new_logPMinus1 = log(1.0 - new_p);

		log_h =  calc_hastingsratio_p(P[l], l, new_p, new_logP, new_logPMinus1);

		//accept or reject
		if(log_h > log(randomGenerator->getRand(0.0,1.0))){
			//accept
			p[l] = new_p;
			logP[l] = new_logP;
			logPMinus1[l] = new_logPMinus1;

			ratioOfBinomials_perLocus_old[l] = ratioOfBinomials_perLocus[l];
			ratioOfBinomials_perLocus[l] = Theta[l]*P[l]* log(p[l]) + Theta[l]*(1.0-P[l])*log(1.0-p[l]);
			ratioOfBinomials += ratioOfBinomials_perLocus[l] - ratioOfBinomials_perLocus_old[l];
			for(unsigned int j=0;j<numpop;j++) pops[j].acceptNewP(l);
		} else {
			rejectNewP_pops(l);
		}
	}
}

void TGroup::update_p(TNormalPriorWithExpVal & log_a, TNormalPriorWithExpVal & log_b){

    #pragma omp parallel for
	for(int l=0;l<numSites;l++){
        double new_p;
        double rand;
        #pragma omp critical
        {
            new_p = randomGenerator->getNormalRandom(p[l], 0.01);
            rand = randomGenerator->getRand(0.0,1.0);
        };

		if(new_p < 0.0){
			new_p = -new_p;
		} else if(new_p > 1.0){
			new_p = 2.0 - new_p;
		}

        const double new_logP = log(new_p);
        const double new_logPMinus1 = log(1.0 - new_p);

        double log_h =  calc_hastingsratio_p(l, new_p, new_logP, new_logPMinus1);
        log_h += (log_a.curExpVal-1.0)*(new_logP - logP[l] ) + (log_b.curExpVal-1.0)*(new_logPMinus1 - logPMinus1[l]) ;

		//accept or reject
        if (log_h > log(rand)) {
            //accept
            p[l] = new_p;
			logP[l] = new_logP;
			logPMinus1[l] = new_logPMinus1;

            #pragma omp critical
            {
                for (unsigned int j = 0; j < numpop; j++){
                    pops[j].acceptNewP(l);
                }
            }

        } else {
            rejectNewP_pops(l);
        }
	}
}


//---------------------------------------------------//
//                     Update S_l					 //
//---------------------------------------------------//


double TGroup::calcGammaFuncOneSite(const double & P, TMC* tmcWorld, const int & l){
	//store old
	Theta_old[l] = Theta[l];
	gamma_theta_perLocus_old[l] = gamma_theta_perLocus[l];
	ratioOfBinomials_perLocus_old[l] = ratioOfBinomials_perLocus[l];
	storeOldGammaTermsForP(l);

	//calc new
	Theta[l] = tmcWorld->calcTheta(l, B.curValue);
	gamma_theta_perLocus[l] = gammln(Theta[l]);
	calcGammaTermsForP(P, l);
	calcRatioOfBinomials(P, l);

	return gamma_theta_perLocus[l] - gamma_theta_perLocus_old[l] + gamma_thetaP_perLocus_old[l] - gamma_thetaP_perLocus[l] - gamma_thetaOneMinusP_perLocus[l] + gamma_thetaOneMinusP_perLocus_old[l] + ratioOfBinomials_perLocus[l] - ratioOfBinomials_perLocus_old[l];
}


void TGroup::acceptGammaOneSite(const double & P, const int & l){
	gamma_theta += gamma_theta_perLocus[l] - gamma_theta_perLocus_old[l];
	acceptS(P, l);
}

void TGroup::acceptS(const double & P, const int & l){
	gamma_thetaP += gamma_thetaP_perLocus[l] - gamma_thetaP_perLocus_old[l];
	gamma_thetaOneMinusP += gamma_thetaOneMinusP_perLocus[l] - gamma_thetaOneMinusP_perLocus_old[l];
	ratioOfBinomials += ratioOfBinomials_perLocus[l] - ratioOfBinomials_perLocus_old[l];
}

void TGroup::rejectGammaOneSite(const int & l){
	Theta[l] = Theta_old[l];
	gamma_theta_perLocus[l] = gamma_theta_perLocus_old[l];
	rejectP(l);
}


//---------------------------------------------------//
//                     Update S_l for groups		 //
//---------------------------------------------------//

void TGroup::update_S(){
	#pragma omp parallel for
	for(int l=0; l<numSites; l++){
		tmc->S_old[l] = tmc->S[l];

        double log_h = tmc->proposeNewS(l);

		//sum over populations
		for(unsigned int j=0;j < numpop;j++){
			log_h += pops[j].logHastingsTermS(l, p[l], logP[l], logPMinus1[l], tmc);
		}

        double rand;

		#pragma omp critical
        {
            rand = randomGenerator->getRand(0.0, 1.0);
        }

		//accept or reject
        if (log_h > log(rand)) {
            //accept
			#pragma omp critical
            {
                for (unsigned int j = 0; j < numpop; j++){
                    pops[j].acceptNewS(l);
                }
            }
        } else {
            reject_Sg(l);
        }
	}

}

void TGroup::reject_Sg(int l){
	tmc->S[l] = tmc->S_old[l];
	for(unsigned int j=0;j<numpop;j++){
		pops[j].rejectNewS(l);
	}
}


//----------------------------------------------------//
// Calc tot LL
//----------------------------------------------------//

double TGroup::give_totLLoneGroup(){
	double totLL = 0.0;

	totLL += tmc->give_LLfromQ();
	totLL += give_LLfromEM(tmc);

	return totLL;
}

double TGroup::give_totLLoneGroup(TMC* tmcgroups){
	double totLL = 0.0;

	totLL += tmcgroups->give_LLfromQ();
	totLL += give_LLfromEM(tmcgroups);

	return totLL;
}


double TGroup::give_LLfromEM(TMC* tmc){
	double LL = 0.0;
	double* theta_h = new double[numSites];

	for(unsigned int j=0;j<numpop;j++){
		double EM_thetaP = 0.0;
		double EM_thetaOneMinusP = 0.0;
		double EM_theta = 0.0;
		double bincoef = 0.0;
		for(int l=0; l<numSites; l++){
			theta_h[l] = exp(-tmc->alpha[tmc->S[l]]-pops[j].beta.curValue);
			EM_thetaP += euler_maclaurin(pops[j].n[l], p[l] * theta_h[l], log(p[l] * theta_h[l]), *randomGenerator);
			EM_thetaOneMinusP += euler_maclaurin(pops[j].N[l] - pops[j].n[l], theta_h[l] * (1.0-p[l]), log(theta_h[l] * (1.0-p[l])), *randomGenerator);
			EM_theta += euler_maclaurin(pops[j].N[l], theta_h[l], log(theta_h[l]), *randomGenerator);
			bincoef += chooseLog(pops[j].N[l], pops[j].n[l]);
		}
		LL += EM_thetaP + EM_thetaOneMinusP - EM_theta + bincoef;
	}

	delete[] theta_h;
	return LL;
}


//----------------------------------------------------//
// Simulate data
//----------------------------------------------------//
void TGroup::simulateS(){
	tmc->simulateS();
}

void TGroup::setAllSNeutral(){
	tmc->setAllSNeutral();
}

void TGroup::setAllSMax(){
	tmc->setAllSMax();
}

void TGroup::setAllSMin(){
	tmc->setAllSMin();
}

void TGroup::simulateData(double* P, TMC* worldTmc, double error){
	double tmpTheta;
	for(int l=0; l<numSites; l++){
		tmpTheta = worldTmc->calcTheta(l, B.curValue);
		p[l] = randomGenerator->getBetaRandom(tmpTheta*P[l], tmpTheta*(1.0-P[l]));
		if(p[l]>=0.99998 || p[l]<=0.000001)
			p[l] = p[l] * 0.99998 + 0.000001;

		logP[l] = log(p[l]);
		logPMinus1[l] = log(1. - p[l]);
	}
	simulateData_inpops(p, error);
}

void TGroup::simulateData(TNormalPriorWithExpVal & log_a, TNormalPriorWithExpVal & log_b, double error){
	for(int l=0; l<numSites; l++){
		if(log_a.curValue == 0.0){
			p[l] = randomGenerator->getRand();
		} else {
			p[l] = randomGenerator->getBetaRandom(log_a.curExpVal, log_b.curExpVal);
		}
		if(p[l]>=0.99998 || p[l]<=0.000001) {
			p[l] = p[l] * 0.99998 + 0.000001;
		}

		logP[l] = log(p[l]);
		logPMinus1[l] = log(1.0 - p[l]);
	}
	simulateData_inpops(p, error);
}

void TGroup::simulateData_inpops(double* p, double error){
	//now simulate data in populations
	for(unsigned int j=0; j<numpop; ++j){
		pops[j].simulateData(p, tmc, error);
	}
}


//----------------------------------------------------//
// Run forward-backward
//----------------------------------------------------//
void TGroup::estimateSUsingForwardBackward(){
	initializeForwardBackwardStorage();
	runForward();
	runBackwardAndEstimateS();
	clearForwardBackwardStorage();
}

void TGroup::initializeForwardBackwardStorage(){
	if(!forwardBackwardStorageInitialized){
		//alpha
		alphaForward = new double*[numSites];
		for(int l=0; l<numSites; ++l)
			alphaForward[l] = new double[tmc->numStates];

		forwardBackwardStorageInitialized = true;

		//beta
		betaBackward = new double*[2];
		betaBackward[0] = new double[tmc->numStates];
		betaBackward[1] = new double[tmc->numStates];

	}
}

void TGroup::clearForwardBackwardStorage(){
	if(forwardBackwardStorageInitialized){
		//alpha
		for(int l=0; l<numSites; ++l)
			delete[] alphaForward[l];
		delete[] alphaForward;

		//backward
		delete[] betaBackward[0];
		delete[] betaBackward[1];
		delete[] betaBackward;

		forwardBackwardStorageInitialized = false;
	}
}

void TGroup::runForward(){
	//variables
	int s, s_old;
	int l=0;
	double sum;
	double* emission = new double[tmc->numStates];

//	std::ofstream out("alphas.txt");

	//calculate alpha for first locus
	sum = 0.0;
	for(s=0; s<tmc->numStates; ++s){
		alphaForward[0][s] = getLogEmissionProbability(l, s) * tmc->getInitialProb(s);
		sum += alphaForward[0][s];
	}

//	out << "0";
	for(s=0; s<tmc->numStates; ++s){
		alphaForward[0][s] /= sum;
//		out << "\t" << alphaForward[0][s];
	}
//	out << "\n";

	//now calculate all other alpha
	for(l=1; l<numSites; ++l){
		sum = 0.0;

		//calculate  emission probs
		fillEmissionProbabilities(emission, l);

		for(s=0; s<tmc->numStates; ++s){
			alphaForward[l][s] = 0.0;
			for(s_old=0; s_old<tmc->numStates; ++s_old){
				alphaForward[l][s] += tmc->getTransitionProbability(l, s_old, s) * alphaForward[l-1][s_old];
			}
			alphaForward[l][s] *= emission[s];
			sum += alphaForward[l][s];
		}

		//normalize
//		out << l << "\t" << sum;
		for(s=0; s<tmc->numStates; ++s){
			alphaForward[l][s] /= sum;
//			out << "\t" << alphaForward[l][s];
		}
//		out << "\n";
		LL_forwardBackward += log(sum);
	}

//	out.close();
	delete[] emission;
}


void TGroup::runBackwardAndEstimateS(){
	//variables
	int s, s_next;
	int curBeta = 0;
	int nextBeta = 1;
	int l, l_next;
	double sum;
	int best;
	double* nextEmission = new double[tmc->numStates];
	double* posterior = new double[tmc->numStates];

	//set initial beta
	for(s=0; s<tmc->numStates; ++s)
		betaBackward[curBeta][s] = 1.0;

	//set last locus to best S
	best = 0;
	l = numSites - 1;
	for(s=1; s<tmc->numStates; ++s){
		if(alphaForward[l][s] > alphaForward[l][best])
			best = s;
	}
	tmc->setS(l, best);

	//open file for posterior probs
	std::ofstream out("posterior_S.txt");


	//now calculate all beta
	for(l=numSites-2; l>=0; --l){
		//move indexes
		l_next = l+1;
		curBeta = 1 - curBeta;
		nextBeta = 1 - nextBeta;

		//calculate all next emission probs
		fillEmissionProbabilities(nextEmission, l_next);

		//calculate beta
		sum = 0.0;
		for(s=0; s<tmc->numStates; ++s){
			betaBackward[curBeta][s] = 0.0;
			for(s_next=0; s_next<tmc->numStates; ++s_next){
				betaBackward[curBeta][s] += nextEmission[s_next] * tmc->getTransitionProbability(l_next, s, s_next) * betaBackward[nextBeta][s_next];
			}
			sum += betaBackward[curBeta][s];
		}

		//normalize
		for(s=0; s<tmc->numStates; ++s)
			betaBackward[curBeta][s] /= sum;

		//calculate posterior probabilities on the S
		sum = 0.0;
		for(s=0; s<tmc->numStates; ++s){
			posterior[s] = alphaForward[l][s] * betaBackward[curBeta][s];
			sum += posterior[s];
		}
		out << l;
		for(s=0; s<tmc->numStates; ++s){
			posterior[s] /= sum;
			out << "\t" << posterior[s];
		}
		out << "\n";

		//set to best S
		best = 0;
		for(s=1; s<tmc->numStates; ++s){
			if(posterior[s] > posterior[best])
				best = s;
		}

		tmc->setS(l, best);
	}

	out.close();
	delete[] nextEmission;
	delete[] posterior;
}

void TGroup::fillEmissionProbabilities(double* em, const int & locus){
	//cal log emission probs
	for(int s=0; s<tmc->numStates; ++s)
		em[s] = getLogEmissionProbability(locus, s);

	//find maximum
	double max = em[0];
	for(int s=1; s<tmc->numStates; ++s){
		if(em[s] > max)
			max = em[s];
	}

	//now add max to each value and de-log
	for(int s=0; s<tmc->numStates; ++s){
		em[s] = exp(em[s] - max);
	}
}

double TGroup::getLogEmissionProbability(const int & locus, const int & state){
	static double thisTheta;
	double LL = 0.0;
	for(unsigned int j=0;j<numpop;j++){
		thisTheta = exp(-tmc->alpha[state] - pops[j].beta.curValue);
		LL += pops[j].calcLLofThetaOneLocus(thisTheta, locus, p[locus]);
	}

	return LL;
}


void TGroup::update_allStogetherGroup(double & log_h, double & prop_LL, double & old_LL, bool & accepted, int & _min, int & _max, int & cost){
	if(estimateSg==true){
		int* newS = new int[tmc->sites->numSites];
//		int nStates = tmc->numStates-1;

		_min = *std::min_element(tmc->S, tmc->S + tmc->sites->numSites);
		_max = tmc->numStates - *std::max_element(tmc->S, tmc->S + tmc->sites->numSites);
		cost = randomGenerator->getRand(-_min, _max);

		if(cost == 0) goto endupS1g;

		for(unsigned int j=0;j<numpop;j++){
			double proposedValue = pops[j].beta.curValue - (double)cost * tmc->CalcDiff();
			pops[j].beta.proposeNewValue(proposedValue);
			log_h += pops[j].beta.logPriorRatio;
		}

		//shift all S
		for(int l=0; l<tmc->sites->numSites; l++){
			newS[l] = tmc->S[l] + cost;
		}

		//add to hastings
		//Q term
		//first locus
		log_h += log( tmc->Q[tmc->sites->groupMembership[0]][0][newS[0]]);
		log_h -= log( tmc->Q[tmc->sites->groupMembership[0]][0][tmc->S[0]]);

		//all other loci
		for(int l=1; l<tmc->sites->numSites; l++){
			log_h += log( tmc->Q[tmc->sites->groupMembership[l]][newS[l-1]][newS[l]]);
			log_h -= log( tmc->Q[tmc->sites->groupMembership[l]][tmc->S[l-1]][tmc->S[l]]);
		}

		//EM term
		for(int l=0; l<tmc->sites->numSites; l++){
			if(newS[l] == tmc->S[l]){
				for(unsigned int j=0;j<numpop;j++)
					log_h += pops[j].logHastingsTermS(l, p[l], logP[l], logPMinus1[l], tmc);
			}
		}

		prop_LL = old_LL + log_h;

		if(log(randomGenerator->getRand()) <  log_h ){
			//accept
			accepted = true;
			for(int l=0; l<tmc->sites->numSites; l++){
				if(newS[l] == tmc->S[l]){
					for(unsigned int j=0;j<numpop;j++)
						pops[j].acceptNewS(l);
				}
				tmc->changeS(l, newS[l]);
			}
		} else {
			//reject
			for(unsigned int j=0;j<numpop;j++)
				pops[j].beta.RejectProposedValue();
			for(int l=1; l<tmc->sites->numSites; l++){
				if(newS[l] == tmc->S[l]){
					for(unsigned int j=0;j<numpop;j++)
						pops[j].rejectNewS(l);
				}
			}
		}
		endupS1g: ;
	}
}


