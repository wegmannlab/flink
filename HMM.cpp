// to compile: g++ -Wall -O3 -o HMM HMM.cpp TRandomGenerator.cpp  HMM_obj.cpp (old version)
//now go to /data/Code/FmodelHMM/Debug   to compile it: control+b

//#include "Object_TWorld.h"

using namespace std;

#include "TMain.h"
#include "allTasks.h"

//---------------------------------------------------------------------------
// Function to add existing tasks
// Use main.addRegularTask to add a regular task (shown in list of available tasks)
// Use main.adddebugTask to add a debug task (not shown in list of available tasks)
//
//---------------------------------------------------------------------------
void addTasks(TMain & main){
    main.addRegularTask("simulate", new TTask_simulate());
    main.addRegularTask("estimate", new TTask_estimate());
    main.addRegularTask("filter", new TTask_filter());
    main.addRegularTask("LLsurface", new TTask_LLsurface());
    main.addRegularTask("Fst", new TTask_Fst());
};

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
    TMain main("Flink", "1.0", "https://bitbucket.org/wegmannlab/Flink");

    //add existing tasks
    addTasks(main);

    //now run program
    return main.run(argc, argv);
};


