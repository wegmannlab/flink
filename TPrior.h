#ifndef _TPRIOR_H_
#define _TPRIOR_H_

#include <cmath>
#include "TParameters.h"
#include "TRandomGenerator.h"

//--------------------------------------------
class TPrior{ //considered uniform prior
public:
	bool isEstimated;
	double curValue;
	double oldValue;
	double sigmaProposal;
	double min, max;
	bool hasMinMax;
	TRandomGenerator* randomGenerator;
	std::string name;
	double randomForHastings;
	double logPriorRatio;
	int accepted;
	int rejected;

	TPrior(){
		name = "";
		randomGenerator = NULL;
		curValue = 0.0;
		oldValue = curValue;
		sigmaProposal = 0.0;
		isEstimated = false;
		randomForHastings = 0.0;
		min = 0.0;
		max = 0.0;
		hasMinMax = false;
		logPriorRatio = 1.0;
		accepted = 0.0;
		rejected = 0.0;
	};

	virtual ~TPrior(){};

	void initialize(std::string & Name, double & Min, double & Max, TRandomGenerator* RandomGenerator){
		name = Name;
		randomGenerator = RandomGenerator;
		setMinMax(Min, Max);
	};

	void setMinMax(double & Min, double & Max){
		min = Min;
		max = Max;
		if(min >= max) throw "Minimum > maximum for parameter '" + name + "'!";
		hasMinMax = true;
		if(hasMinMax && sigmaProposal > (max-min)/4.0) sigmaProposal = (max-min)/4.0;
	};

	bool isWithinRange(double & val){
		if(val >= min && val <= max) return true;
		return false;
	};


	bool acceptOrReject(double & log_h){
		if(log(randomForHastings) < log_h){
			accepted++;
			return true;
		}
		else RejectProposedValue();
		return false;
	};

	virtual void RejectProposedValue(){
		curValue = oldValue;
		rejected++;
	};

	virtual void proposeNewValue(){
		randomForHastings = randomGenerator->getRand();
		oldValue = curValue;
		curValue = randomGenerator->getNormalRandom(curValue, sigmaProposal);

		if(hasMinMax){
			if(curValue > max)
//				curValue = max - 0.001;
				curValue = 2.0*max - curValue;
			else if(curValue < min)
//				curValue = min + 0.001;
				curValue = 2.0*min - curValue;
			if(curValue > max || curValue < min)
				throw("error proposing a new parameter!!!");
		}
		calcLogPriorRatio();
	};

	virtual void proposeNewValue(double proposedValue){
		oldValue = curValue;
		curValue = proposedValue;

		if(hasMinMax){
			if(curValue > max)
				curValue = max - 0.001;
			else if(curValue < min)
				curValue = min + 0.001;
			if(curValue > max || curValue < min)
				throw("error proposing a new parameter!!!");
		}
		calcLogPriorRatio();
	};

	void setNewValue(double val){
		randomForHastings = randomGenerator->getRand();
		oldValue = curValue;
		curValue = val;
		calcLogPriorRatio();
	};

	virtual void calcLogPriorRatio(){ logPriorRatio = 0.0; };
};

class TNormalPrior:public TPrior{
public:
	double mean;
	double sigma;
	double minusOneOverTwosigmaSquared;

	TNormalPrior():TPrior(){
		mean = 0.0;
		sigma = 0.0;
		minusOneOverTwosigmaSquared = 0.0;
	};

	virtual void initialize(std::string Name, std::string paramString, double Min, double Max, double rangeProportion, TRandomGenerator* RandomGenerator){
		TPrior::initialize(Name, Min, Max, RandomGenerator);

		//parse string
		trimString(paramString);
		std::size_t pos = paramString.find_first_of(',');
		if(pos == std::string::npos){
			mean = stringToDoubleCheck(paramString);
		} else {
			mean = stringToDoubleCheck(paramString.substr(0, pos));
			sigma = stringToDoubleCheck(paramString.substr(pos+1));
			isEstimated = true;
		}

		//check
		if(mean < min || mean > max) throw "The mean (initial value) = " + toString(mean) + " of parameter '" + name + "' is outside the range [" + toString(min) + "," + toString(max) + "]!";
		if(sigma < 0.0) throw "The sigma = " + toString(mean) + " of parameter '" + name + "' can not be negative!";

		//set stuff
		curValue = mean;
		oldValue = curValue;
		if(isEstimated){
			minusOneOverTwosigmaSquared = -1.0 / (2.0 * sigma * sigma);
			sigmaProposal = rangeProportion * sigma;
		}
	};

	void calcLogPriorRatio(){
		logPriorRatio = minusOneOverTwosigmaSquared * (curValue*curValue - oldValue*oldValue + 2.0 * mean * (oldValue - curValue));
//		logPriorRatio = 0.0; //test
	};
};

class TNormalPriorWithExpVal:public TNormalPrior{
public:
	double curExpVal, oldExpVal;

	void initialize(std::string Name, std::string paramString, double Min, double Max, double rangeProportion, TRandomGenerator* RandomGenerator){
		TNormalPrior::initialize(Name, paramString, Min, Max, rangeProportion, RandomGenerator);
		curExpVal = exp(curValue);
		oldExpVal = curExpVal;
	};

	virtual void RejectProposedValue(){
		curValue = oldValue;
		curExpVal = oldExpVal;
		rejected++;
	};

	virtual void proposeNewValue(){
		TPrior::proposeNewValue();
		oldExpVal = curExpVal;
		curExpVal = exp(curValue);
	};

};

class TExponentialPrior:public TPrior{
public:
	double lambda;

	TExponentialPrior():TPrior(){
		lambda = 0.0;
	};

	void initialize(std::string Name, std::string paramString, double Min, double Max, double rangeProportion, TRandomGenerator* RandomGenerator){
		TPrior::initialize(Name, Min, Max, RandomGenerator);

		//parse string
		trimString(paramString);

		std::size_t pos = paramString.find_first_of(',');
		if(pos == std::string::npos){
			curValue = stringToDoubleCheck(paramString);
		} else {
			curValue = stringToDoubleCheck(paramString.substr(0, pos));
			lambda = stringToDoubleCheck(paramString.substr(pos+1));
			isEstimated = true;
		}

		//check
		if(min < 0.0) throw "The minimum of parameter '" + name + "' can not be negative!";
		if(curValue < min || curValue > max) throw "The mean (initial value) = " + toString(curValue) + " of parameter '" + name + "' is outside the range [" + toString(min) + "," + toString(max) + "]!";
		if(lambda < 0.0) throw "The lambda = " + toString(lambda) + " of parameter '" + name + "' can not be negative!";

		//set stuff
		oldValue = curValue;
		if(isEstimated)
			sigmaProposal = rangeProportion / (lambda * lambda);
	};

	void calcLogPriorRatio(){
		logPriorRatio = lambda * (oldValue - curValue);
//		logPriorRatio = 0.0; //test
	};

	void proposeNewGM(){
		// proposing a new mu or nu for the generating matrix
		randomForHastings = randomGenerator->getRand();
		oldValue = curValue;
		curValue = randomGenerator->getNormalRandom(curValue, sigmaProposal);
		if(hasMinMax){
			if(curValue > max)
				curValue = 2.0*max - curValue;
			else if(curValue < min)
				curValue = 2.0*min - curValue;
			if(curValue > max || curValue < min){
				throw "error proposing a new value in the generating matrix " + toString(curValue);
			}
		}
		calcLogPriorRatio();
	};
};


class TLogNormalPrior:public TPrior{
public:
	double mean;
	double sigma;
	double minusOneOverTwosigmaSquared;

	TLogNormalPrior():TPrior(){
		mean = 0.0;
		sigma = 1.0;
		minusOneOverTwosigmaSquared = -1.0;
	};

	void initialize(std::string Name, std::string paramString, double Min, double Max, double rangeProportion, TRandomGenerator* RandomGenerator){
		TPrior::initialize(Name, Min, Max, RandomGenerator);
		curValue = 0.5;
		oldValue = curValue;

		//parse string
		trimString(paramString);
		std::size_t pos = paramString.find_first_of(',');

		if(pos == std::string::npos){
			curValue = stringToDoubleCheck(paramString);
		} else {
			mean = stringToDoubleCheck(paramString.substr(0, pos));
			sigma = stringToDoubleCheck(paramString.substr(pos+1));
			isEstimated = true;
		}

		//check
		if(curValue < min || curValue > max) throw "The initial value = " + toString(mean) + " of parameter '" + name + "' is outside the range [" + toString(min) + "," + toString(max) + "]!";
		if(sigma < 0.0) throw "The sigma = " + toString(mean) + " of parameter '" + name + "' can not be negative!";

		//set stuff
		if(isEstimated){
			Min=0.0001;
			Max=0.9999;
			if(curValue<Min)
				curValue=Min;
			if(curValue>Max)
				curValue=Max;
			minusOneOverTwosigmaSquared = -1.0 / (2.0 * sigma * sigma);
			sigmaProposal = rangeProportion * sigma;
		}
	};

	void calcLogPriorRatio(){
		logPriorRatio = log(oldValue/curValue) + minusOneOverTwosigmaSquared * (log(curValue)*log(curValue) - log(oldValue)*log(oldValue) + 2.0 * mean * (log(oldValue) - log(curValue)));
//		logPriorRatio = 0.0; //test
	};

	void proposeNewValue(){
		oldValue = curValue;
		curValue = randomGenerator->getNormalRandom(curValue, sigmaProposal);
		if(hasMinMax){
			if(curValue > max)
				curValue = 2.0*max - curValue;
			else if(curValue < min)
				curValue = 2.0*min - curValue;
			if(curValue > max || curValue < min){
				throw "error proposing a new value in the generating matrix " + toString(curValue);
			}
		}
	};
};


class TUniformPrior:public TPrior{
public:

	void initialize(std::string Name, std::string paramString, double Min, double Max, double rangeProportion, TRandomGenerator* RandomGenerator){
		TPrior::initialize(Name, Min, Max, RandomGenerator);

		//parse string
		trimString(paramString);

		std::size_t pos = paramString.find_first_of(',');
		if(pos == std::string::npos){
			curValue = stringToDoubleCheck(paramString);
		} else {
			Min = stringToDoubleCheck(paramString.substr(0, pos));
			Max = stringToDoubleCheck(paramString.substr(pos+1));
			isEstimated = true;
			curValue = randomGenerator->getRand(Min, Max);
		}

		min = Min;
		max = Max;

		//set stuff
		oldValue = curValue;
		if(isEstimated)
			sigmaProposal = rangeProportion;
	};

	void calcLogPriorRatio(){
		logPriorRatio = 0.0; //test
	};

};


class TReflExponentialPrior:public TPrior{
public:
	double lambda;

	TReflExponentialPrior():TPrior(){
		lambda = 0.0;
	};

	void initialize(std::string Name, std::string paramString, double Min, double Max, double rangeProportion, TRandomGenerator* RandomGenerator){
		TPrior::initialize(Name, Min, Max, RandomGenerator);

		//parse string
		trimString(paramString);

		std::size_t pos = paramString.find_first_of(',');
		if(pos == std::string::npos){
			curValue = stringToDoubleCheck(paramString);
		} else {
			curValue = stringToDoubleCheck(paramString.substr(0, pos));
			lambda = stringToDoubleCheck(paramString.substr(pos+1));
			isEstimated = true;
		}

		//check
		if(min < 0.0) throw "The minimum of parameter '" + name + "' can not be negative!";
		if(curValue < min || curValue > max) throw "The mean (initial value) = " + toString(curValue) + " of parameter '" + name + "' is outside the range [" + toString(min) + "," + toString(max) + "]!";
		if(lambda < 0.0) throw "The lambda = " + toString(lambda) + " of parameter '" + name + "' can not be negative!";

		//set stuff
		oldValue = curValue;
		if(isEstimated)
			sigmaProposal = rangeProportion / (lambda * lambda);
	};

	void calcLogPriorRatio(){
		logPriorRatio = log( (1.0-lambda*(exp(-lambda*curValue))) / (1.0-lambda*(exp(-lambda*oldValue))) );
//		logPriorRatio = 0.0; //test
	};

};



//-------------------------------------------------------------
//TPriorVector
//-------------------------------------------------------------
class TPriorVector{
public:
	std::vector<TPrior*> paramVec;
	std::vector<TPrior*> paramVecAll;
	std::vector<TPrior*>::iterator it;
	bool hasPriors;

	TPriorVector(){
		hasPriors = false;
	};
	~TPriorVector(){};

	void addPrior(TPrior* p){
		if(p->isEstimated){
			paramVec.push_back(p);
			hasPriors = true;
		}
		paramVecAll.push_back(p);
	};
	void writeHeader(std::ostream & out){
		it = paramVec.begin();
		out << (*it)->name; ++it;
		for(; it!=paramVec.end(); ++it){
			out << "\t" << (*it)->name;
		}
	};
	void writeCurrentParameters(std::ostream & out){
		it=paramVec.begin();
		out << (*it)->curValue; ++it;
		for(; it!=paramVec.end(); ++it){
			out << "\t" << (*it)->curValue;
		}
	};
	void writeHeaderAll(std::ostream & out){
		it=paramVecAll.begin();
		out << (*it)->name; ++it;
		for(; it!=paramVecAll.end(); ++it){
			out << "\t" << (*it)->name;
		}
	};
	void writeCurrentParametersAll(std::ostream & out){
		it=paramVecAll.begin();
		out << (*it)->curValue; ++it;
		for(; it!=paramVecAll.end(); ++it){
			out << "\t" << (*it)->curValue;
		}
	};
	int numParams(){
		return paramVec.size();
	};
	void listParams(TLog* logfile){
		for(it=paramVec.begin(); it!=paramVec.end(); ++it){
			logfile->list((*it)->name);
		}
	};
};


#endif /* _TPRIOR_H_ */
