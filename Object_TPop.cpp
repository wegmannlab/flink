#include "Object_TPop.h"

#include "TLog.h"

TPop::TPop(){
	randomGenerator = NULL;
	numSites = 0;
	storageInitialized = false;

	//initialize beta from prior
	EM_thetaP = 0.0;
	EM_thetaOneMinusP = 0.0;
	EM_theta = 0.0;
	EM_thetaP_old = 0.0;
	EM_thetaOneMinusP_old = 0.0;
	EM_theta_old = 0.0;

	//create all arrays
	theta = NULL;
	theta_old = NULL;
	tmpPointerToDouble = NULL;
	EM_thetaP_perLocus = NULL;
	EM_thetaP_perLocus_old = NULL;
	EM_thetaOneMinusP_perLocus = NULL;
	EM_thetaOneMinusP_perLocus_old = NULL;
	EM_theta_perLocus = NULL;
	EM_theta_perLocus_old = NULL;
	n = NULL;
	N = NULL;
}

void TPop::initializeStorage(std::string & Name, int NumSites, TRandomGenerator* RandomGenerator){
	name = Name;
	randomGenerator = RandomGenerator;
	numSites = NumSites;

	//create all arrays
	theta = new double[numSites];
	theta_old = new double[numSites];
	EM_thetaP_perLocus = new double[numSites];
	EM_thetaP_perLocus_old = new double[numSites];
	EM_thetaOneMinusP_perLocus = new double[numSites];
	EM_thetaOneMinusP_perLocus_old = new double[numSites];
	EM_theta_perLocus = new double[numSites];
	EM_theta_perLocus_old = new double[numSites];
	n = new int[numSites];
	N = new int[numSites];

	storageInitialized = true;

    //TODO: Use memset
	for(int l=0; l<numSites; l++){
		EM_thetaP_perLocus_old[l] = 0.0;
		EM_thetaP_perLocus[l] = 0.0;
		EM_thetaOneMinusP_perLocus_old[l] = 0.0;
		EM_thetaOneMinusP_perLocus[l] = 0.0;
		EM_theta_perLocus_old[l] = 0.0;
		EM_theta_perLocus[l] = 0.0;
		theta[l]=0.0;
		theta_old[l]=0.0;
	}
}


void TPop::initializeFromPrior(std::string & betaString, double & rangeProp){
	//initialize beta from prior
	beta.initialize(name + "_beta", betaString, -15.0, 15.0, rangeProp, randomGenerator);

	//initialize theta and Euler Mac Laurin Terms
	EM_thetaP = 0.0;
	EM_thetaOneMinusP = 0.0;
	EM_theta = 0.0;
}

void TPop::registerPriors(TPriorVector* priorVec){
	priorVec->addPrior(&beta);
}

void TPop::fillThetaAndEulerMacLaurinTerms(const double* p, const double *logP, const double *logPMinus1, TMC* tmc){
    double EM_thetaP_l = 0.0;
    double EM_thetaOneMinusP_l = 0.0;
    double EM_theta_l = 0.0;

    double const currentBeta = beta.curValue;

	//calc EM terms
    #pragma omp parallel for reduction(+:EM_thetaP_l,EM_thetaOneMinusP_l,EM_theta_l)
	for(int l=0; l<numSites; l++){
        //calc new theta
        double thetaL = tmc->calcTheta(l, currentBeta);
        theta[l] = thetaL;
        const double alphaBeta = - tmc->alpha[tmc->S[l]] - beta.curValue; //this equals log(theta[l])

		//calc new Euler MacLaurin terms
		EM_thetaP_perLocus[l] = euler_maclaurin(n[l], p[l] * thetaL, alphaBeta + logP[l], *randomGenerator);
		EM_thetaP_l += EM_thetaP_perLocus[l];

		EM_thetaOneMinusP_perLocus[l] = euler_maclaurin(N[l] - n[l], thetaL * (1.0-p[l]), alphaBeta + logPMinus1[l], *randomGenerator);
		EM_thetaOneMinusP_l += EM_thetaOneMinusP_perLocus[l];

		if(theta_old[l] == thetaL){
            EM_theta_perLocus[l] = EM_theta_perLocus_old[l];
		}else{
            EM_theta_perLocus[l] = euler_maclaurin(N[l], thetaL, alphaBeta, *randomGenerator);
		}

		EM_theta_l += EM_theta_perLocus[l];
	}


	EM_thetaP = EM_thetaP_l;
	EM_thetaOneMinusP = EM_thetaOneMinusP_l;
	EM_theta = EM_theta_l;

/*
	//calc new Euler MacLaurin terms
	#pragma omp parallel for
	for(int l=0; l<numSites; l++){
		EM_thetaP_perLocus[l] = euler_maclaurin(n[l], p[l] * theta[l]);
		EM_thetaOneMinusP_perLocus[l] = euler_maclaurin(N[l] - n[l], theta[l] * (1.0-p[l]));
		EM_theta_perLocus[l] = euler_maclaurin(N[l], theta[l]);
	}

	for(int l=0; l<numSites; l++){
		EM_thetaP += EM_thetaP_perLocus[l];
		EM_thetaOneMinusP += EM_thetaOneMinusP_perLocus[l];
		EM_theta += EM_theta_perLocus[l];
	}
*/
}


void TPop::initialise_calcTheta_pops(TMC* tmc){
	tmc->calcTheta(theta, beta.curValue);
}


void TPop::switchArrays(){
	//theta
	tmpPointerToDouble = theta_old;
	theta_old = theta;
	theta = tmpPointerToDouble;

	//EM_thetaP
	tmpPointerToDouble = EM_thetaP_perLocus_old;
	EM_thetaP_perLocus_old = EM_thetaP_perLocus;
	EM_thetaP_perLocus = tmpPointerToDouble;

	//EM_thetaOneMinusP
	tmpPointerToDouble = EM_thetaOneMinusP_perLocus_old;
	EM_thetaOneMinusP_perLocus_old = EM_thetaOneMinusP_perLocus;
	EM_thetaOneMinusP_perLocus = tmpPointerToDouble;

	//EM_thetaP
	tmpPointerToDouble = EM_theta_perLocus_old;
	EM_theta_perLocus_old = EM_theta_perLocus;
	EM_theta_perLocus = tmpPointerToDouble;
}

void TPop::switchThetaAndEulerMacLaurinTerms(){
	//switch arrays
	switchArrays();

	//switch values
	EM_thetaP_old = EM_thetaP;
	EM_thetaOneMinusP_old = EM_thetaOneMinusP;
	EM_theta_old = EM_theta;

	EM_thetaP = 0.0;
	EM_thetaOneMinusP = 0.0;
	EM_theta = 0.0;
}

void TPop::rejectThetaAndEulerMacLaurinTerms(){
	//reject new theta
	switchArrays();

	//reject new Euler MacLaurin terms
	EM_thetaP = EM_thetaP_old;
	EM_thetaOneMinusP = EM_thetaOneMinusP_old;
	EM_theta = EM_theta_old;
}

double TPop::calcDerivativeOfThetaOneLocus(const double & this_theta, const int & locus, const double & p){
	double fact1 = 0.0;
	double fact2 = 0.0;
	double fact3 = 0.0;

	for(int i=0; i<n[locus]; i++)
		fact1 += 1.0/(this_theta * p + i);
	for(int i=0; i<(N[locus]-n[locus]); i++)
		fact2 += 1.0/(this_theta * (1.0-p) + i);
	for(int i=0; i<N[locus]; i++)
		fact3 += 1.0/(this_theta + i);
	return(p*fact1 + (1.0-p)*fact2 - fact3);
}

double TPop::calcLLofThetaOneLocus(const double & this_theta, const int & locus, const double & p){
	double fact1 = 0.0;
	double fact2 = 0.0;
	double fact3 = 0.0;

	for(int i=0; i<n[locus]; i++)
		fact1 += log(this_theta * p + i);
	for(int i=0; i<(N[locus]-n[locus]); i++)
		fact2 += log(this_theta * (1.0-p) + i);
	for(int i=0; i<N[locus]; i++)
		fact3 += log(this_theta + i);

	return(fact1 + fact2 - fact3);
}

double TPop::calcDerivativeOfTheta(const double & this_theta, double* & p){
	double fact1;
	double fact2;
	double fact3;
	double sum = 0.0;

	for(int l=0; l< numSites; l++){
		fact1 = 0.0;
		fact2 = 0.0;
		fact3 = 0.0;

		for(int i=0; i<n[l]; i++)
			fact1 += 1.0/(this_theta * p[l] + i);
		for(int i=0; i<(N[l]-n[l]); i++)
			fact2 += 1.0/(this_theta * (1.0-p[l]) + i);
		for(int i=0; i<N[l]; i++)
			fact3 += 1.0/(this_theta + i);
		sum += p[l]*fact1 + (1.0-p[l])*fact2 - fact3;
	}
	return sum;
}

void TPop::findInitialValueOfBeta(const int & numSwitches, double* p, TLog* logfile){
//working on
	if(beta.isEstimated){
		//initialize
		int s = 0;
		double log_theta = 3.0;
		double this_theta = exp(log_theta);
		double oldDerivative = calcDerivativeOfTheta(this_theta, p);
		double step;
		if(oldDerivative < 0.0)
			step = -0.5;
		else if(oldDerivative > 0.0)
			step = 0.5;
		else s = numSwitches;
		double derivative;
		double sign;

		//std::cout << "Initial theta = " << this_theta << ", initial deriv = " << oldDerivative << std::endl;

		//now run algorithm
		while(s < numSwitches){
			for(int i=0; i<101; ++i){
				log_theta += step;
				this_theta = exp(log_theta);
				derivative = calcDerivativeOfTheta(this_theta, p);

				//std::cout << "Theta = " << this_theta << ", deriv = " << derivative << std::endl;

				sign = derivative / oldDerivative;
				oldDerivative = derivative;
				if(sign < 0.0){
					step = -step/2.71828;
					++s;
					//std::cout << "NEW STEP = " << step << std::endl;
					break;
				}

				//checks
				if(i == 100)
					throw "Issue initializing beta values for population " + name + "!";
			}
		}

		double new_beta = -log(this_theta);
		if(new_beta>0.0) new_beta=-1.0;
		if(new_beta<-10.0) new_beta=-9.0;
		set_beta(new_beta);
		logfile->list(name + ": initial beta = " + toString(beta.curValue));
	} else logfile->list(name + ": beta is fixed at " + toString(beta.curValue));
}


double TPop::calcThetaAndEulerMacLaurinTerms(const double* p, const double *logP, const double *logPMinus1, TMC* tmc){
	switchThetaAndEulerMacLaurinTerms();
	fillThetaAndEulerMacLaurinTerms(p, logP, logPMinus1, tmc);

	return EM_thetaP - EM_thetaP_old + EM_thetaOneMinusP - EM_thetaOneMinusP_old - EM_theta + EM_theta_old;
}


double TPop::calculateHastingsRatio_beta(const double* p, const double* logP, const double* logPMinus1, TMC* tmc){
	return beta.logPriorRatio + calcThetaAndEulerMacLaurinTerms(p, logP, logPMinus1, tmc);
}

void TPop::reject_beta(){
	beta.RejectProposedValue();
	rejectThetaAndEulerMacLaurinTerms();
}

void TPop::update_beta(const double* p, const double* logP, const double* logPMinus1, TMC* tmc, std::ostream & out){
	if(beta.isEstimated){
		//propose new beta
		beta.proposeNewValue();

		//calc hastings ratio
		double log_h = calculateHastingsRatio_beta(p, logP, logPMinus1, tmc);

		out << beta.oldValue << "\t" << beta.curValue << "\t" << log_h << "\t";

		//accept or reject
		if(!beta.acceptOrReject(log_h)){
			rejectThetaAndEulerMacLaurinTerms();
			out << "rejected" << "\t";
		}else{
			out << "accepted" << "\t";
		}
	}
}



//----------------------------------------------
//Update p
//----------------------------------------------
double TPop::logHastingsTermP(const int l, const double new_p, const double logP, const double logPMinus1){
	//store old terms
	EM_thetaP_perLocus_old[l] = EM_thetaP_perLocus[l];
	EM_thetaOneMinusP_perLocus_old[l] = EM_thetaOneMinusP_perLocus[l];

	//calc new Euler MacLaurin terms
	double alphaBeta = log(theta[l]);

	EM_thetaP_perLocus[l] = euler_maclaurin(n[l], new_p * theta[l], alphaBeta + logP, *randomGenerator);
	EM_thetaOneMinusP_perLocus[l] = euler_maclaurin(N[l] - n[l], theta[l] * (1.0 - new_p), alphaBeta + logPMinus1, *randomGenerator);

	//return hastings
	return EM_thetaP_perLocus[l] + EM_thetaOneMinusP_perLocus[l] - EM_thetaP_perLocus_old[l] - EM_thetaOneMinusP_perLocus_old[l] ;
}


void TPop::acceptNewP(const int & l){
	EM_thetaP = EM_thetaP - EM_thetaP_perLocus_old[l] + EM_thetaP_perLocus[l];
	EM_thetaOneMinusP = EM_thetaOneMinusP - EM_thetaOneMinusP_perLocus_old[l] + EM_thetaOneMinusP_perLocus[l];
}

void TPop::rejectNewP(const int & l){
	EM_thetaP_perLocus[l] = EM_thetaP_perLocus_old[l];
	EM_thetaOneMinusP_perLocus[l] = EM_thetaOneMinusP_perLocus_old[l];
}


//----------------------------------------------
//Update S
//----------------------------------------------
double TPop::logHastingsTermS(const int l, const double new_p, const double newLogP, const double newLogPMinus1, TMC* tmc){
	StoreOldTerms(l);

	CalcNewtheta_and_EMterms(l, new_p, newLogP, newLogPMinus1, tmc);

	//return hastings
	return EM_thetaP_perLocus[l] - EM_thetaP_perLocus_old[l] + EM_thetaOneMinusP_perLocus[l] - EM_thetaOneMinusP_perLocus_old[l] + EM_theta_perLocus_old[l] - EM_theta_perLocus[l];
}

void TPop::StoreOldTerms(const int & l){
	//store old terms
	theta_old[l] = theta[l];
	EM_thetaP_perLocus_old[l] = EM_thetaP_perLocus[l];
	EM_thetaOneMinusP_perLocus_old[l] = EM_thetaOneMinusP_perLocus[l];
	EM_theta_perLocus_old[l] = EM_theta_perLocus[l];
}


void TPop::CalcNewtheta_and_EMterms(const int l, const double new_p, const double newLogP, const double newLogPMinus1, TMC* tmc){
	//calc new theta
	const double thetaL = tmc->calcTheta(l, beta.curValue);
	theta[l] = thetaL;
	double alphaBeta = - tmc->alpha[tmc->S[l]] - beta.curValue;

	//calc new Euler MacLaurin terms
	EM_thetaP_perLocus[l] = euler_maclaurin(n[l], new_p * thetaL, alphaBeta + newLogP, *randomGenerator);
	EM_thetaOneMinusP_perLocus[l] = euler_maclaurin(N[l] - n[l], thetaL * (1.0 - new_p), alphaBeta + newLogPMinus1, *randomGenerator);
	EM_theta_perLocus[l] = euler_maclaurin(N[l], thetaL, alphaBeta, *randomGenerator);
}


void TPop::acceptNewS(const int & l){
	EM_thetaP = EM_thetaP - EM_thetaP_perLocus_old[l] + EM_thetaP_perLocus[l];
	EM_thetaOneMinusP = EM_thetaOneMinusP - EM_thetaOneMinusP_perLocus_old[l] + EM_thetaOneMinusP_perLocus[l];
	EM_theta = EM_theta - EM_theta_perLocus_old[l] + EM_theta_perLocus[l];
}

void TPop::rejectNewS(const int & l){
	theta[l] = theta_old[l];
	EM_thetaP_perLocus[l] = EM_thetaP_perLocus_old[l];
	EM_thetaOneMinusP_perLocus[l] = EM_thetaOneMinusP_perLocus_old[l];
	EM_theta_perLocus[l] = EM_theta_perLocus_old[l];
}

void TPop::simulateData(double* p, TMC* tmc, double error){
	double p_pop;
	double tmpTheta;
	for(int l=0; l<numSites; l++){
		//check if site is monomorphic
		if(p[l] <= 0.0) n[l] = 0;
		else if (p[l] >= 1.0) n[l] = N[l];
		else {
			//now draw from beta-binomial
			tmpTheta = tmc->calcTheta(l, beta.curValue);
			p_pop = randomGenerator->getBetaRandom(tmpTheta*p[l], tmpTheta*(1.0-p[l]));
			n[l] = randomGenerator->getBiomialRand(p_pop*(1.0-error)+(1.0-p_pop)*error, N[l]);
		}
	}
}

