#ifndef OBJECT_TPOP_H_
#define OBJECT_TPOP_H_

#include "helperFunctions.h"
#include "TRandomGenerator.h"
#include "Object_TMC.h"
#include "TLog.h"

class TPop{
public:
	std::string name;
	TRandomGenerator* randomGenerator;
	int numSites;
	bool storageInitialized;

	//beta
	TNormalPrior beta;

	//data
	int* n;
    int* N;

	//store theta
	double* theta;
	double* theta_old;
	double* tmpPointerToDouble;

	//store Euler MacLaurin terms
	double EM_thetaP;
	double EM_thetaP_old;
	double* EM_thetaP_perLocus;
	double* EM_thetaP_perLocus_old;
	double EM_thetaOneMinusP;
	double EM_thetaOneMinusP_old;
	double* EM_thetaOneMinusP_perLocus;
	double* EM_thetaOneMinusP_perLocus_old;
	double EM_theta;
	double EM_theta_old;
	double* EM_theta_perLocus;
	double* EM_theta_perLocus_old;

	TPop();
	~TPop(){
		if(storageInitialized){
			delete[] theta;
			delete[] theta_old;
			delete[] EM_thetaP_perLocus;
			delete[] EM_thetaP_perLocus_old;
			delete[] EM_thetaOneMinusP_perLocus;
			delete[] EM_thetaOneMinusP_perLocus_old;
			delete[] EM_theta_perLocus;
			delete[] EM_theta_perLocus_old;
			delete[] n;
			delete[] N;
		}
	};

	void initializeStorage(std::string & Name, int NumSites, TRandomGenerator* RandomGenerator);
	void initializeFromPrior(std::string & betaString, double & rangeProp);
	void registerPriors(TPriorVector* priorVec);

	void addData(int & locus, int & _n, int & _N){
		n[locus] = _n;
		N[locus] = _N;
	};

	void writeData(std::ostream & out, int & locus){
		out << n[locus] << "/" << N[locus];
	};
	double estimateP(int & l){
		return (double)n[l] / N[l];
	};
	void addToCrudePEstimate(int & l, double & p, int & numPops){
		if(N[l] > 0){
			p += (double)n[l] / N[l];
			++numPops;
		}
	};

	void set_beta(double & _beta){
		beta.setNewValue(_beta);
	};

	void fillThetaAndEulerMacLaurinTerms(const double* p, const double *logP, const double *logPMinus1, TMC* tmc);
	void initialise_calcTheta_pops(TMC* tmc);

	void switchArrays();
	void switchThetaAndEulerMacLaurinTerms();
	void rejectThetaAndEulerMacLaurinTerms();

	double calcDerivativeOfThetaOneLocus(const double & this_theta, const int & locus, const double & p);
	double calcLLofThetaOneLocus(const double & this_theta, const int & locus, const double & p);
	double calcDerivativeOfTheta(const double & this_theta, double* & p);
	void findInitialValueOfBeta(const int & numSwitches, double* p, TLog* logfile);

	double calcThetaAndEulerMacLaurinTerms(const double* p, const double *logP, const double *logPMinus1, TMC* tmc);
	double calculateHastingsRatio_beta(const double* p, const double* logP, const double* logPMinus1, TMC* tmc);
	double calculateLL_beta(double* p, TMC* tmc);
	void reject_beta();
	void update_beta(const double* p, const double* logP, const double* logPMinus1, TMC* tmc,std::ostream & out);

	double logHastingsTermP(const int l, const double new_p, const double logP, const double logPMinus1);
	void acceptNewP(const int & l);
	void rejectNewP(const int & l);

	double logHastingsTermS(const int l, const double new_p, const double newLogP, const double newLogPMinus1, TMC* tmc);
	void StoreOldTerms(const int & l);
	void CalcNewtheta_and_EMterms(const int l, const double new_p, const double newLogP, const double newLogPMinus1, TMC* tmc);
	void acceptNewS(const int & l);
	void rejectNewS(const int & l);

	void simulateData(double* P, TMC* tmc, double error);

};

#endif /* OBJECT_TPOP_H_ */
