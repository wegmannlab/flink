#make file for atlas

SRC = $(wildcard *.cpp) $(wildcard *.C) $(wildcard commonutilities/*.cpp)
GIT_HEADER = gitversion.cpp

OBJ = $(SRC:%.cpp=%.o)
BIN = flink

.PHONY : all
all : $(BIN)

BINFLAG = -lz
OBJFLAG = -std=c++11

$(BIN): $(OBJ)
	$(CXX) -O3 -fopenmp -o $(BIN) $(OBJ) $(BINFLAG)

$(GIT_HEADER): .git/HEAD .git/COMMIT_EDITMSG
	echo "#include \"gitversion.h\"" > $@
	echo "std::string getGitVersion(){" >> $@
	echo "return \"$(shell git rev-parse HEAD)\";" >> $@
	echo "}" >> $@

.git/COMMIT_EDITMSG :
	touch $@

%.o: %.cpp
	$(CXX) -O3 -fopenmp -c -Icommonutilities $(OBJFLAG) $< -o $@


.PHONY : clean
clean:
	rm -rf $(BIN) $(OBJ)
