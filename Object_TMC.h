#ifndef OBJECT_TMC_H_
#define OBJECT_TMC_H_

#include <vector>
#include <iostream>
#include "stringFunctions.h"
#include "TMatrix.h"
#include "TPrior.h"

//Group 0 is the group of the first sites of each chromosome!

struct TDistanceGroup{
	int min;
	int maxPlusOne;
	double distance;
};

class TSites{
public:
	int numChromosomes, curChromosomeIndex;
	int numSites;
	std::vector<std::string> chromosomes;
	std::vector< std::pair<int, long> > sites; //int is index for chromosomes, long is position
	std::vector< std::pair<int, long> >::iterator curSite;
	int oldPosition; //used during construction

	//distances
	bool distancesInitialized;
	int numDistanceGroups;
	TDistanceGroup* distanceGroups;
	int* groupMembership; //for each site, to which group does it belong?
	bool* groupHasSites;

	TSites(){
		numChromosomes = 0;
		curChromosomeIndex = -1;
		numSites = 0;
		oldPosition = -1;
		numDistanceGroups = 0;
		distanceGroups = NULL;
		groupMembership = NULL;
		distancesInitialized = false;
		groupHasSites = NULL;
	};

	~TSites(){
		if(distancesInitialized){
			delete[] groupMembership;
			delete[] distanceGroups;
			delete[] groupHasSites;
		}
	};

	void addChromosome(std::string & Name){
		//check if name already exists
		for(std::vector<std::string>::iterator it=chromosomes.begin(); it!=chromosomes.end(); ++it){
			if(*it == Name) throw "Chromosome '" + Name + "' appears multiple times in the input file! Is the file correctly ordered?";
		}

		//now add
		chromosomes.push_back(Name);
		++numChromosomes;
		curChromosomeIndex = numChromosomes - 1;
		oldPosition = -1;
	};

	void addSite(std::string & chr, long position){
		if(curChromosomeIndex < 0 || chr != chromosomes[curChromosomeIndex])
			addChromosome(chr);

		//check if positions are ordered
		if(position <= oldPosition) throw "Positions are not ordered in input file!";
		sites.push_back( std::pair<int, long>(curChromosomeIndex, position));
		oldPosition = position;
	};

	void createChromosomesAndSites(int _numChr, int _numSites, int _distance){
		std::string chrName;
		for(int c=0; c<_numChr; ++c){
			chrName = "Chr" + toString(c+1);
			for(int s=0; s <_numSites; ++s)
				addSite(chrName, s * _distance + 1);
		}
	}

	void begin(){
		curSite = sites.begin();
	}

	bool writeNextSite(std::ostream & out){
		if(curSite == sites.end()) return false;
		out << chromosomes[curSite->first] << "\t" << curSite->second;
		++curSite;
		return true;
	};

	void goNextSite(){
		++curSite;
	};

	void calcAndGroupDistances(int maxDist){
		//create distance groups
		numDistanceGroups = ceil(log(maxDist)/log(2)) + 1; //one extra for beginning of chromosome (and last in genome)
		distanceGroups = new TDistanceGroup[numDistanceGroups];
		groupHasSites = new bool[numDistanceGroups];
		for(int d=1; d<numDistanceGroups;d++){
			groupHasSites[d]=false;
		}

		//group 0: first sites of chromosomes
		distanceGroups[0].min = -1;
		distanceGroups[0].maxPlusOne = -1;
		distanceGroups[0].distance = -1.0;

		//all other groups
		distanceGroups[1].min = 1;
		int g;
		for(g=2; g<numDistanceGroups; ++g){
			distanceGroups[g].min = distanceGroups[g-1].min * 2;
			distanceGroups[g-1].maxPlusOne = distanceGroups[g].min;
			distanceGroups[g-1].distance = distanceGroups[g-1].maxPlusOne - distanceGroups[g-1].min;
		}
		distanceGroups[numDistanceGroups - 1].maxPlusOne = distanceGroups[numDistanceGroups-1].min * 2.0 + 1.0;
		//calculate distance for each site and assign sites to groups
		numSites = sites.size();
		groupMembership = new int[numSites + 1]; //artificial Q at end
		int curChr = -1;
		long oldPos = 0;
		int l = 0;
		double dist;
		for(std::vector< std::pair<int, long> >::iterator it = sites.begin(); it != sites.end(); ++it, ++l){
			//check if it is a new chromosome
			if(it->first != curChr){
				groupMembership[l] = 0;
				curChr = it->first;
			} else {
				dist = it->second - oldPos;
				for(g=1; g<numDistanceGroups; ++g){
					if(g == numDistanceGroups - 1){
						groupMembership[l] = g;
						break;
					} else if(dist < distanceGroups[g].maxPlusOne){
						groupMembership[l] = g;
						break;
					}
				}
				groupHasSites[g] = true;
			}
			oldPos = it->second;
		}
		//create artificial extra site at very end of the genome to simplify calculations
		groupMembership[numSites] = 0;
		distancesInitialized = true;
	};

	double getTotalLength(){
		double length = 0.0;
		int oldChr = -1;
		long oldPos = 0;
		for(std::vector< std::pair<int, long> >::iterator it = sites.begin(); it != sites.end(); ++it){
			if(oldChr != it->first) {
                oldChr = it->first;
            }else {
                length += it->second - oldPos;
            }
			oldPos = it->second;
		}
		return length;
	};
};

class TMC{
public:
	int s_max;
	double s_max_asDouble;
	int numStates; //2*s_max + 1

	TExponentialPrior alphaMax;
	double* alpha; //vector to store S -> alpha translation
	double* alpha_old;

	//for HMM: S and transition matrix Q
	int* S;
	int* S_old;
	int* minS;
	int* maxS;
	TSquareMatrixStorage* Q;
	TSquareMatrixStorage* Q_old;
	TUniformPrior lnkappa;
	TUniformPrior lnMu;
	TUniformPrior lnNu;
//	TUniformPrior lnNu_down;
	double* stationaryDistribution;

	double* pointerToDouble;
	TSquareMatrixStorage* pointerToQ;
	TSquareMatrixStorage* curQ;
	TSquareMatrixStorage* nextQ;

	TRandomGenerator* randomGenerator;
	TSites* sites;

	TMC(int S_max, TSites* Sites, TRandomGenerator* RandomGenerator){
		randomGenerator = RandomGenerator;

		sites = Sites;

		s_max = S_max;
		s_max_asDouble = (double) s_max;
		numStates = 2*s_max + 1;
		alpha = new double[numStates];
		alpha_old = new double[numStates];

		//S
		S = new int[sites->numSites];
		S_old = new int[sites->numSites];
		pointerToDouble = NULL;

		minS = new int[sites->numSites];
		maxS = new int[sites->numSites];

		//transition Q
		Q = new TSquareMatrixStorage[sites->numDistanceGroups];
		Q_old = new TSquareMatrixStorage[sites->numDistanceGroups];
		pointerToQ = NULL;
		stationaryDistribution = new double[numStates];
		curQ = 0;
		nextQ = 0;
	};
	~TMC(){
		delete[] alpha;
		delete[] alpha_old;
		delete[] S;
		delete[] S_old;
		delete[] Q;
		delete[] Q_old;
		delete[] minS;
		delete[] maxS;
		delete[] stationaryDistribution;
	};

	void initializeFromPrior(std::string alphaName, std::string & alphaString, std::string kappaName, std::string & kappaString, std::string MuName, std::string & MuString, std::string Nu_Name, std::string & Nu_String, double & rangeProp_alphaMax, double & rangeProp_kappa, double & rangeProp_mu, double & rangeProp_nu){
		alphaMax.initialize(alphaName, alphaString, 0.0, 15.0, rangeProp_alphaMax, randomGenerator);
		lnkappa.initialize(kappaName, kappaString, -20.0, -0.1, rangeProp_kappa, randomGenerator);
		lnMu.initialize(MuName, MuString, -10.0, -0.01, rangeProp_mu, randomGenerator);
		lnNu.initialize(Nu_Name, Nu_String, -10.0, -0.01, rangeProp_nu, randomGenerator);

		fillStationaryDistribution();

		//fill alpha vector
		fillAlphaVector(alphaMax.curValue, alpha);
		fillAllQ();

		//choose S at neutrality
		setAllSNeutral();

		for(int i = 0; i < sites->numSites; ++i){
			minS[i]=0;
			maxS[i]=0;
		}
	};

	void registerPriors(TPriorVector* priorVec){
		priorVec->addPrior(&alphaMax);
		priorVec->addPrior(&lnkappa);
		priorVec->addPrior(&lnMu);
		priorVec->addPrior(&lnNu);
	};


	double CalcDiff(){
		double diff = 0.0;
		diff = alpha[int(numStates / 2.0) + 1];
		return diff;
	};

	//---------------------------------------------------//
	//             Stuff for Q             		         //
	//---------------------------------------------------//

	void fillStationaryDistribution(){
		double nu = exp(lnNu.curValue);
		double mu = exp(lnMu.curValue);
		double cost;
		cost = pow(mu,double (s_max)) * (mu -1.0) * nu / (2.0 * (pow(mu,double (s_max))-1.0) *mu *nu + mu - 1.0 );
		for(int i = 0; i < s_max; ++i){
			stationaryDistribution[i] = cost * 1.0/pow(mu,(double)i);
		}
		stationaryDistribution[s_max] = cost * 1.0/(pow(mu,double (s_max))*nu);
		for(int i = s_max-1; i >= 0; --i){
			stationaryDistribution[numStates-i-1] = cost * 1.0/pow(mu,(double)i);
		}
	}

	void printStatDis(){
		std::string StatDis = "stationaryDistrib.txt";
		std::ofstream PI(StatDis.c_str());
		PI<<"state"<<"\t"<<"value"<<std::endl;
		for(int i = -s_max; i <= s_max; ++i){
			PI<<i<<"\t"<<stationaryDistribution[s_max+i]<<std::endl;
		}
		PI.close();
	}

	void fillAllQ(){
		int k;

		//first fill Q for first sites of chromosomes
		//-------------------------------------------
		Q[0].resize(numStates);
		fillStationaryDistribution();
		for(int i=0; i<numStates; i++){
			for(int j=0; j<numStates; j++){
				Q[0][i][j] = stationaryDistribution[j];
			}
		}

//		std::cout<<"first Q element  "<<Q[0][0][0]<<std::endl;
//		throw("stop");

		//	Q[0].set((1.0/(double)numStates));

		//fill Q for 1 site (Q_0)
		//-----------------------
		TBandMatrixStorage lambda(numStates,3);
		lambda.set(0.0);

		double K;
		// this was the previous good transition matrix
		double mu, nu;

		K = exp(lnkappa.curValue);
		mu = exp(lnMu.curValue);
		nu = exp(lnNu.curValue);


		/////////////////////////////////////////////////////////////////////////////////////////////////
		//  matrix NuTimesMu
		//negative cas-1.2171e-46e
		double kappaMinusOneMinusMu = K * (-1.0 - mu);
		double kappaMu = K * mu;
		lambda(0,0) = -K; lambda(0,1) = K;
		for(k=1; k<s_max; k++){
			lambda(k,k-1) = kappaMu; lambda(k,k) = kappaMinusOneMinusMu; lambda(k,k+1) = K;
		}

		//neutral case
//		lambda(s_max,s_max-1) = K*(-1.0+mu); lambda(s_max,s_max) = K*(2.0-2.0*mu); lambda(s_max,s_max+1) = K*(-1.0+mu);
		lambda(s_max,s_max-1) = kappaMu*nu; lambda(s_max,s_max) = K*(-2.0*mu)*nu; lambda(s_max,s_max+1) = kappaMu*nu;

		//positive case
		for(k=s_max+1; k<numStates-1; k++){
			lambda(k,k-1) = K; lambda(k,k) = kappaMinusOneMinusMu; lambda(k,k+1) = kappaMu;
		}
		lambda(numStates-1,numStates-1) = -K; lambda(numStates-1,numStates-2) = K;

		//now get exponential of lambda
		Q[1].fillAsExponential(lambda);

		//now fill all other Q
		//-------------------------------------------
		for(k=2; k<sites->numDistanceGroups; ++k){
			Q[k].fillFromSquare(Q[k-1]);
		}
	};

	void switchQ(){
		pointerToQ = Q_old;
		Q_old = Q;
		Q = pointerToQ;
	};

	double getInitialProb(int state){ //make sure Q[0] has identical rows!
		return Q[0](0,state);
	};

	double getTransitionProbability(const int & locus, const int & oldState, const int & newState){
		return Q[sites->groupMembership[locus]][oldState][newState];
	};

	double give_LLfromQ(){
		double LL = log(Q[0][0][S[0]]);

		for(int l=1; l < sites->numSites; l++){
			LL += log( Q[ sites->groupMembership[l] ][ S[l-1] ][ S[l] ] );
		}
		return LL;
	}

	//---------------------------------------------------//
	//             Update alpha max (or A max)			 //
	//---------------------------------------------------//

	void fillAlphaVector(double & alphaMax, double* vec){
		double s;

		for(int i=0; i<numStates; ++i){
			s = (double)i - s_max_asDouble;
			vec[i] = alphaMax * (s / s_max_asDouble);
		}
	};


	double proposeNewAlphaMax(){
		switch_alpha();

		//propose new alpha_max value
		alphaMax.proposeNewValue();

		//fill alpha vector
		fillAlphaVector(alphaMax.curValue, alpha);

		//return prior ratio
		return alphaMax.logPriorRatio;
	};

	void switch_alpha(){
		//store old values
		pointerToDouble = alpha;
		alpha = alpha_old;
		alpha_old = pointerToDouble;
	}

	void setAlphaMax(double newAlphaMax){
		switch_alpha();

		//propose new alpha_max value
		alphaMax.setNewValue(newAlphaMax);

		//fill alpha vector
		fillAlphaVector(alphaMax.curValue, alpha);
	};

	bool acceptOrRejectAlphamax(double & log_h){
		if(alphaMax.acceptOrReject(log_h)) return true;
		//else reject
		switch_alpha();
		return false;
	};


	//---------------------------------------------------//
	//             calc theta               			 //
	//---------------------------------------------------//

	double calcTheta(const int & locus, const double & beta){
		return exp(-alpha[S[locus]] - beta);
	};

	void calcTheta(double* theta, const double & beta){
		for(int l=0; l< sites->numSites; l++){
			theta[l] = exp(-alpha[S[l]]-beta);
		}
	};

	void calcThetaLocus(double* theta, const double & beta, const int & l){
		theta[l] = exp(-alpha[S[l]]-beta);
	};

	//---------------------------------------------------//
	//                    Update lnkappa				 //
	//---------------------------------------------------//
	void updateKappa(){
		if(lnkappa.isEstimated){
			//propose new value
			lnkappa.proposeNewValue();
			switchQ();
			fillAllQ();

			//accept or reject
			double log_h = logHastingsKappa();
			if(lnkappa.acceptOrReject(log_h)==false)
				switchQ();
		}
	};

	double logHastingsKappa(){
		double log_h = 0.0;
		for(int l=1; l<sites->numSites; l++){ //was until L-1, but I think that is wrong...
			log_h += log( Q[sites->groupMembership[l]][S[l-1]][S[l]] / Q_old[sites->groupMembership[l]][S[l-1]][S[l]] );
		}
		return log_h;
	}


	double sumlnLhdQ(){
		double ll_sumQ = 0.0;
		switchQ();
		fillAllQ();
		for(int l=1; l<sites->numSites; l++){ //was until L-1, but I think that is wrong...
			ll_sumQ += log( Q[sites->groupMembership[l]][S[l-1]][S[l]] );
		}
		return ll_sumQ;
	}

	void setKappa(double Kappa){
		lnkappa.setNewValue(Kappa);
		switchQ();
		fillAllQ();
	};

	void rejectKappa(){
		lnkappa.RejectProposedValue();
		switchQ();
	};

	void estimateKappaFromS(){
		//simply get rate of change as number of changes / base pair
		int numChanges = 0;
		for(int l=1; l<sites->numSites; l++){
			if(S[l-1] != S[l])
				++numChanges;
		}

		double rate = (double) numChanges / sites->getTotalLength();

		setKappa(log(-log(1.0 - rate)/(2.0)));
	};


	//---------------------------------------------------//
	//                    Update nu & mu				 //
	//---------------------------------------------------//

	void updateNuAndMu(){
		switchQ();
		fillAllQ();
	};

	void rejectNuAndMu(){
		switchQ();
	};

	double upsumlnLhdQ(){
		updateNuAndMu();
		return sumlnLhdQ();
	};

	void update_mu(){
		//update mu in world
		if(lnMu.isEstimated){
			//propose new value
			lnMu.proposeNewValue();
			double log_h = lnMu.logPriorRatio;

			updateNuAndMu();
			log_h += logHastingsKappa();

			//accept or reject
			if(lnMu.acceptOrReject(log_h)==false){
				rejectNuAndMu();
			}
		}
	};

	void update_nu(){
		//update nu in world
		if(lnNu.isEstimated){
			//propose new value
			lnNu.proposeNewValue();
			double log_h = lnNu.logPriorRatio;
			updateNuAndMu();
			log_h += logHastingsKappa();

			//accept or reject
			if(lnNu.acceptOrReject(log_h)==false){
				rejectNuAndMu();
			}
		}
	}

	//---------------------------------------------------//
	//                    S         					 //
	//---------------------------------------------------//

	void changeS(const int l, const int newS){
		S_old[l] = S[l];
		S[l] = newS;
	}

	double proposeNewS(const int l){
		double log_h = 0.0;
		if(S[l] == 0){
			S[l] = 1;
			log_h = -log(2.0);
		} else if(S[l] == numStates-1){
			S[l] = numStates - 2;
			log_h = -log(2.0);
		} else {
			double rand;

			#pragma omp critical
			rand = randomGenerator->getRand();

			if(rand < 0.5){
				++S[l];
				if(S[l] == numStates - 1){
					log_h = log(2.0);
				}
			}
			else {
				--S[l];
				if(S[l] == 0){
					log_h = log(2.0);
				}
			}
		}
		if(l==0){
			nextQ = &Q[sites->groupMembership[l+1]];
			return log( nextQ->at(S[l],S[l+1]) / (nextQ->at(S_old[l],S[l+1]) ) ) + log_h;
		}
		if(l==sites->numSites-1) {
			curQ = &Q[sites->groupMembership[l]];
			return log( curQ->at(S[l-1],S[l]) / (curQ->at(S[l-1],S_old[l]) ) ) + log_h;
		}
		log_h += logHastingsS(l);
		return log_h;
	};

	double logHastingsS(const int l){
		curQ = &Q[sites->groupMembership[l]];
		nextQ = &Q[sites->groupMembership[l+1]];
		return log( curQ->at(S[l-1],S[l]) * nextQ->at(S[l],S[l+1]) / (curQ->at(S[l-1],S_old[l]) * nextQ->at(S_old[l],S[l+1]) ) );
	};


	void simulateS(){
		//simulate S
		//pick first S at random
		S[0] = randomGenerator->getRand(0, numStates);
//		S[0] = numStates - 1;//test

		//simulate other S
		double r;
		int index;
		double cumul;
		fillAllQ();
		for(int l=1; l<sites->numSites; l++){
			r = randomGenerator->getRand();
			index = 0;
			cumul = Q[sites->groupMembership[l]](S[l-1],index);
			while(r > cumul){
				++index;
				cumul += Q[sites->groupMembership[l]](S[l-1],index);
			}
			S[l] = index;
		}
	};

	void setS(const int & locus, const int & state){
		S[locus] = state;
	};

	void fixS(int & Svalue){
		for(int l=0; l<sites->numSites; l++){
			S[l] = Svalue;
		}
	};

	void setAllSNeutral(){
		int neutralS = floor(numStates / 2.0);
		for(int l=0; l<sites->numSites; l++){
			S[l] = neutralS;//test
		}
	};

	void setAllSMax(){
		int maxS = floor(numStates - 1);
		for(int l=0; l<sites->numSites; l++){
			S[l] = maxS;//test
		}
	};

	void setAllSMin(){
		int minS = 0;
		for(int l=0; l<sites->numSites; l++){
			S[l] = minS;//test
		}
	};

	void setSAccordingToAlphaEstimates(double* alpha_estimates){
		for(int l=0; l<sites->numSites; ++l){
			S[l] = round(alpha_estimates[l] / alphaMax.curValue * s_max) + s_max;
			if(S[l] > 2*s_max){
				S[l] = 2*s_max;
			} else if (S[l] < 0){
				S[l] = 0;
			}
		}
	};

};


#endif /* OBJECT_TMC_H_ */
