/*
 * TTask.h
 *
 *  Created on: Mar 31, 2019
 *      Author: phaentu
 */

#ifndef ALLTASKS_H_
#define ALLTASKS_H_

#include "TTask.h"

//---------------------------------------------------------------------------
// all includes necessary for the application
//---------------------------------------------------------------------------
//#include "HMM.cpp"
#include "Object_TWorld.h"
// #include "gperftools/profiler.h"

//---------------------------------------------------------------------------
// TTask class specific to this application (optional)
//---------------------------------------------------------------------------
class TTask_flink:public TTask{
public:
    TTask_flink(){ _citations.push_back("Flink"); };
};

//---------------------------------------------------------------------------
// Function to create map of tasks (adjust in allTasks.cpp file)
//---------------------------------------------------------------------------
void fillTaskMaps(std::map< std::string, TTask* > & taskMap_regular, std::map< std::string, TTask* > & taskMap_debug);

//---------------------------------------------------------------------------
// Create a class for each task, as shown in the example below
//---------------------------------------------------------------------------
//    class TTask_NAME:public TTask{ //or derive from class specific task
//        public:
//        TTask_NAME(){ _explanation = "SAY WHAT THIS TASK IS DOING"; };

//        void run(TParameters & parameters, TLog* logfile){
//            SPECIFY HOW TO EXECUTE THIS TASK
//        };
//    };
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Read groups
//---------------------------------------------------------------------------
class TTask_simulate:public TTask_flink{
public:
    TTask_simulate(){ _explanation = "Simulating under Flink model"; };

    void run(TParameters & myParameters, TLog* logfile){
		TWorld world(myParameters, logfile, randomGenerator);
		world.simulateData(myParameters);
    };
};

class TTask_estimate:public TTask_flink{
public:
    TTask_estimate(){ _explanation = "Estimation"; };

    void run(TParameters & myParameters, TLog* logfile){
		TWorld world(myParameters, logfile, randomGenerator);
		world.runMCMCInference(myParameters);
    };
};

class TTask_filter:public TTask_flink{
public:
    TTask_filter(){ _explanation = "Filtering data"; };

    void run(TParameters & myParameters, TLog* logfile){
		TWorld world(myParameters, logfile, randomGenerator);
    	std::string data = myParameters.getParameterString("data");
    	world.readSitesGroupsAndPopulations(data, myParameters);
    	world.filterData("filtered_data.txt", myParameters);
    };
};

class TTask_LLsurface:public TTask_flink{
public:
    TTask_LLsurface(){ _explanation = "LLsurface"; };

    void run(TParameters & myParameters, TLog* logfile){
		TWorld world(myParameters, logfile, randomGenerator);
		world.calculateLLsurfaces(myParameters);
    };
};

class TTask_Fst:public TTask_flink{
public:
    TTask_Fst(){ _explanation = "calculating Fst"; };

    void run(TParameters & myParameters, TLog* logfile){
		TWorld world(myParameters, logfile, randomGenerator);
		world.calculate_Fst(myParameters);
    };
};

#endif /* ALLTASKS_H_ */
